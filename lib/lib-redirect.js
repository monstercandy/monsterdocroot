const libBase = require("lib-redirect-base.js")
var lib = module.exports = function(app) {
   return libBase(app, "nginx_redirect_directory")
}
