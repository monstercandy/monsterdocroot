module.exports = function(app) {

    var router = app.ExpressPromiseRouter()

    var docroot = require("lib-docroot.js")(app)

    var common_docroot = require("common-docroot.js")(app, docroot)


  router.get("/complete/distinct", function(req,res,next){

      return docroot.GetDistinctCompletePathes()
        .then((re)=>{
            req.sendResponse(re)
        })

  })

  router.get("/complete/distinct-logdirs", function(req,res,next){

      return docroot.GetDistinctCompleteLogdirs()
        .then((re)=>{
            req.sendResponse(re)
        })

  })


  router.route("/webapps/upgrade")
    .post(function(req,res,next){
        return req.sendOk(docroot.UpgradeWebappContainerReferences(req.body.json))
    })

  router.route("/webapps/detach")
    .post(function(req,res,next){
        return req.sendOk(docroot.DetachWebappContainerReferences(req.body.json))
    })

  router.route("/webapps/:webhosting_id/upgrade")
    .post(function(req,res,next){
        return req.sendOk(docroot.UpgradeWebappContainerReferencesForWebstore(req.params.webhosting_id, req.body.json))
    })

  router.route("/webapps/:webhosting_id")
      .get(common_docroot.getWebhostingInfoExpress(function(wh, req,res,next){

          return docroot.GetWebAppsOfWebhosting(wh)

      }, true))
      .post(common_docroot.getWebhostingInfoExpress(function(wh, req,res,next){

          return docroot.SetDefaultWebAppOfWebhosting(wh, req.body.json)

      }))
  router.post("/webapps/:webhosting_id/:domain", common_docroot.getWebhostingInfoDomainExpress(function(wh, d, req,res,next){

          return d.SetWebAppOfWebhostingDomain(req.body.json)

      }))

  router.get("/hostentries/:webhosting_id", common_docroot.getWebhostingInfoExpress(function(wh, req,res,next){

          return docroot.GetHostEntriesOfWebhosting(wh)

      }, true))
  router.get("/hostmap/:webhosting_id", common_docroot.getWebhostingInfoExpress(function(wh, req,res,next){

          return docroot.GetHostDocrootMappingOfWebhosting(wh)

      }, true))
  router.get("/distinct/:webhosting_id", common_docroot.getWebhostingInfoExpress(function(wh, req,res,next){

          return docroot.GetDistinctDocrootsOfWebhosting(wh)

      }, true))
  router.get("/certificates/:webhosting_id", common_docroot.getWebhostingInfoExpress(function(wh, req,res,next){
          return docroot.GetDocrootsMappedByCertificate(wh)

      }, true))

  router.route("/:webhosting_id/:domain/move")
    .post(common_docroot.getWebhostingInfoDomainExpress(function(wh, d, req,res,next){

         return d.Move(req.body.json);

    }))


  router.route("/:webhosting_id/:domain/entries/:entry")
    .post(common_docroot.getWebhostingInfoDomainExpress(function(wh, d, req,res,next){
    	  // changes a subdomain docroot entry (like www.example.com for example.com)

       // adds a new domain docroot entry
       return d.AddDocroot(req.params.entry, req.body.json)
         .then(()=>{
            return d.Save()
         })
         .then(()=>{
              app.InsertEvent(req, {e_event_type: "docroot-create"})
         })


	  }))
    .delete(common_docroot.getWebhostingInfoDomainExpress(function(wh, d, req,res,next){
    	  // delete a subdomain docroot entry (like www.example.com for example.com)

        return d.RemoveDocroot(req.params.entry, req.body.json)
         .then(()=>{
            return d.Save()
         })
         .then(()=>{
              app.InsertEvent(req, {e_event_type: "docroot-remove"})
         })

	  }))


  router.route("/:webhosting_id/:domain/entries")
    .get(common_docroot.getWebhostingInfoDomainExpress(function(wh,d,  req,res,next){
        // get entries
        req.result = d.GetEntries()
        next()
    }))
    .put(common_docroot.getWebhostingInfoDomainExpress(function(wh,d,req,res,next){
       // adds a new domain docroot entry
       return d.AddDocroot(req.body.json.host, req.body.json)
         .then(()=>{
            return d.Save()
         })
         .then(()=>{
              app.InsertEvent(req, {e_event_type: "docroot-create", e_other: true})
         })

    }))

    router.get("/:webhosting_id/:domain/raw",common_docroot.getWebhostingInfoDomainExpress(function(wh,d,  req,res,next){
        // get entries
        return d.GetRaw()
          .then(c=>{
              req.result = c
              next()

          })
    }))


  //!!! if you change anything here, dont forget to adjust relayer-docrootapi.js to grant permissions for endusers!
  Array("uwsgi", "postdata", "awstats_language", "php", 
        "certificate", "suspend", "bandwidthexceeded", 
        "primary", "expiredroot", "bandwidthexceededroot", 
        "timeout", "httpsonly", "hstsheaders", "anticlickjackingheaders", 
        "skipnginxstaticfiles", "nginxgzip", "forceredirect",
        "djangostatic", "dontblockxmlrpc"
    ).forEach(q=>{

    router.post("/:webhosting_id/:domain/"+q, common_docroot.getWebhostingInfoDomainExpress(function(wh,d, req,res,next){

         var m = "Set"+common_docroot.capitalize(q)
         return d[m](req.body.json)
           .then(()=>{
              return d.Save()
           })
           .then(()=>{
                app.InsertEvent(req, {e_event_type: "docroot-tweak", e_other: extend({}, req.params, {c:q}, req.body.json)})
           })

      }))

  })


  router.route("/:webhosting_id/:domain")
    .get(common_docroot.getWebhostingInfoDomainExpress(function(wh,d,  req,res,next){
       // get complete hash
       req.result = d.GetAll()
       next()

    }))

    .delete(common_docroot.getWebhostingInfoDomainExpress(function(wh,d,req,res,next){
	      // 1. delete each docroot entry
	      // 2. delete domain config file

        return d.Delete()
           .then(()=>{
                app.InsertEvent(req, {e_event_type: "docroot-remove"})
           })

	  }))



  // return domains configured under this webhosting
  router.route("/:webhosting_id")
      .get(common_docroot.getWebhostingInfoExpress(function(wh, req,res,next){

          return docroot.GetDomainsOfWebhosting(wh)
            .catch(ex=>{
               if(ex.code == "ENOENT")
                  return [];
               throw ex;
            })
            .then((domains)=>{
                req.result = domains
                next()
            })

      }))


  // return webhostings or webhostings+domains
  router.route("/")
     .get(function(req,res,next){
         if(req.result) return

         return docroot.GetWebhostings()
           .then(d=>{
              req.result = d
              next()
           })
      })
     .search(function(req,res,next){
         if(req.result) return
         return docroot.GetWebhostingsDomains()
           .then(d=>{
              req.result = d
              next()
           })
    })

  return router



}
