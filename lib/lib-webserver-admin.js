var lib = module.exports = function(app) {

   const passwordLengthInBytes = app.config.get("webserver_admin_password_length_in_bytes");

   const adminCredentialValidity = app.config.get("webserver_admin_expose_credential_max_interval_minute");

   const adminUsername = app.config.get("webserver_admin_username");
   const adminAuthFilePath =  app.config.get("webserver_admin_auth_file_path");
   if((!adminUsername)||(!adminAuthFilePath)) {
      console.error("webserver_admin_username/webserver_admin_auth_file_path not configured");
      return;
   }

   var vali = require("MonsterValidators").ValidateJs()

   var clearCredentialTimeout

   var re = {};

   re.GetAdminUrl = function(url_prefix, in_data){
       var d;
       return vali.async(in_data, {validity_min: {default: 60, isInteger: {lessTheanOrEqualTo: adminCredentialValidity}}})
         .then(ad=>{
            d = ad;

            return re.GetAuthenticationObject(true);
         })
         .then(x=>{

            const cred = adminUsername +":"+x.adminPassword+"@";

            var url = url_prefix;
            Array("https://", "http://").forEach(a=>{
               url = url.replace(a, a+cred);
            })

            clearCredentialTimeout = setTimeout(function(){
              return re.ReprotectAuthentication();
            }, d.validity_min * 60 * 1000)


            return Promise.resolve({url: url});

         })

   }

   re.ReprotectAuthentication = function(){
      console.log("ReprotectAuthentication");
      const token = require("Token");
      var x;
      return re.GetAuthenticationObject()
        .then((ax)=>{
           x = ax;
           return token.GetTokenAsync(passwordLengthInBytes, "hex");
        })
        .then((newPassword)=>{
           return x.AddAccount({username:adminUsername, password:newPassword}, true);
        })
        .then(()=>{
           return x.Save();
        })
        .then(()=>{
           return x;
        })
   }

   re.GetAuthenticationObject=function(regenerateWhenNeeded){
      const auth = require("lib-auth.js")(app);
      return auth.OpenByFilename(adminAuthFilePath, true)
        .then(x=>{
            if(!regenerateWhenNeeded) return x;

            var a = x.GetAccounts();
            var p;
            if((!a.length)||(a[0].username != adminUsername))
              p = re.ReprotectAuthentication();
            else
              p = Promise.resolve(x)

            return p.then(x=>{
                x.adminUsername = adminUsername;
                x.adminPassword = x.GetAccounts()[0].password;
                return x;
            })
        })
   }


   return re


}
