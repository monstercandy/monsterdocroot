
exports.up = function(knex, Promise) {

    return Promise.all([

        knex.schema.createTable('transferlog', function(table) {
            table.integer('tl_webhosting_id').notNullable();
            table.string('tl_description').notNullable();
            table.bigInteger('tl_bytes_in').notNullable();
            table.bigInteger('tl_bytes_out').notNullable();
            table.bigInteger('tl_bytes_sum').notNullable();
            table.integer('tl_hits').notNullable();
            table.timestamp('tl_timestamp').notNullable().defaultTo(knex.fn.now());
        }),

    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('transferlog'),
    ])

};
