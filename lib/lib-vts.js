var lib = module.exports = function(app) {

   var re = {}
   const dotq = require("MonsterDotq");
   const moment = require("MonsterMoment");

   const webserverAdmin = require("lib-webserver-admin.js")(app);

   const vts_url_prefix = app.config.get("vts_url_prefix");
   const mrtg_url_prefix = app.config.get("mrtg_url_prefix");

   var vali = require("MonsterValidators").ValidateJs()

   var clearCredentialTimeout

   const useResponseMsec = app.config.get("vts_use_response_now");

   function sendVtsReq(uri){
      return webserverAdmin.GetAuthenticationObject(true)
      .then((x)=>{

          return app.PostForm(vts_url_prefix+uri,  null, {
            'auth': {
            'user': x.adminUsername,
            'pass': x.adminPassword,
            'sendImmediately': true
          }})

      })
      .then(data=>{
         return JSON.parse(data);
      })
   }

   re.Fetch = function() {
      return sendVtsReq("format/json");

   }

   re.GetVtsUrl = function(in_data){
       return webserverAdmin.GetAdminUrl(vts_url_prefix, in_data);
   }
   re.GetMrtgUrl = function(in_data){
       return webserverAdmin.GetAdminUrl(mrtg_url_prefix, in_data);
   }

   re.FetchAndReset = function() {
      return re.Fetch()
        .then(r=>{
           sendVtsReq("status/control?cmd=reset&group=*");

           return r;
        })
   }

   re.FetchWebstoresWithBandwidthLimit = function(){
      return app.GetRelayerMapiPool().getAsync("/s/[this]/info/webhosting/bandwidth-limited")
        .then(r=>{
            return r.result;
        })
   }

   function statsVali(search) {
      return vali.async(search, {
        from: {isString: true}, 
        to: {isString: true}, 
        tl_description: {isString:true}, 
        filter: {isString:true},
         "limit": {default: 100, numericality:{onlyInteger: true, strict: true, lessThanOrEqualTo: 100}},
         "offset": {default: {value:0}, isInteger:true},
         "order": {inclusion: ["tl_description", "tl_timestamp", "tl_webhosting_id", "tl_hits", "tl_bytes_sum","tl_bytes_in","tl_bytes_out"], default: "tl_description"},
         "desc": {isBooleanLazy: true},
         "groupByWebstore": {isBooleanLazy: true},
         "groupByDomain": {isBooleanLazy: true},
      })      
   }
   function statsSqlFilters(k, d, whId){

           if(whId)
             k = k.whereRaw("tl_webhosting_id=?",[whId]);
           if(d.tl_description)
             k = k.whereRaw("tl_description LIKE ?", ['%'+d.tl_description+'%']);
           if(d.filter)
             k = k.whereRaw("(tl_description LIKE ? OR tl_webhosting_id=?)", ['%'+d.filter+'%', d.filter]);
           if(d.from)
             k = k.whereRaw("tl_timestamp >= ?", [d.from]);
           if(d.to)
             k = k.whereRaw("tl_timestamp <= ?", [d.to]);
           if(d.groupByWebstore)
             k = k.groupBy("tl_webhosting_id");
           if(d.groupByDomain)
             k = k.groupBy("tl_description");
          return k;
   }

   re.QueryStats = function(whId, search) {
      return statsVali(search)
       .then(d=>{
           var k = app.knex("transferlog").select();

           return statsSqlFilters(k, d, whId);
       })
   }

   re.QueryStats2 = function(whId, search) {
      var re = {};
      var d;
      return statsVali(search)
       .then(ad=>{
           d = ad; 

           var k;
           if((d.groupByWebstore)||(d.groupByDomain)) {

              k = app.knex
                 .count("*")
                 .sum("tl_bytes_sum AS tl_bytes_sum")
                 .sum("tl_bytes_in AS tl_bytes_in")
                 .sum("tl_bytes_out AS tl_bytes_out")
                 .sum("tl_hits AS tl_hits")
                 .from(function(){
                   var ik = this.from("transferlog");
                   ik = populateSelectWhenGroupBy(ik);
                   statsSqlFilters(ik, d, whId);
                   return ik;
                 });

           } else {
              k = app.knex("transferlog").count("*")
                 .sum("tl_bytes_sum AS tl_bytes_sum")
                 .sum("tl_bytes_in AS tl_bytes_in")
                 .sum("tl_bytes_out AS tl_bytes_out")
                 .sum("tl_hits AS tl_hits");

              k = statsSqlFilters(k, d, whId);
           }

           return k;
       })
       .then((rows)=>{      
           re.count = rows[0]["count(*)"];
           delete rows[0]["count(*)"];
           re.sum = rows[0];

           var k = app.knex("transferlog");
           if((d.groupByWebstore)||(d.groupByDomain)) {

              k = k.select("tl_description", "tl_webhosting_id")
              k = populateSelectWhenGroupBy(k);
           } else {
              k = k.select();
           }

           k = statsSqlFilters(k, d, whId);

           return k.limit(d.limit)
                   .offset(d.offset)
                   .orderBy(d.order, d.desc?"desc":undefined)

       })
       .then(rows=>{
          re["rows"] = rows;
          return re;
       })

       function populateSelectWhenGroupBy(k) {
          return k.max("tl_timestamp AS tl_timestamp")
                 .sum("tl_bytes_sum AS tl_bytes_sum")
                 .sum("tl_bytes_in AS tl_bytes_in")
                 .sum("tl_bytes_out AS tl_bytes_out")
                 .sum("tl_hits AS tl_hits");
       }
   }

   re.ListWebstoresExceededCurrently = function(gGandWidthLimits, mockedTs){
      var p = !gGandWidthLimits ? re.FetchWebstoresWithBandwidthLimit() : Promise.resolve(gGandWidthLimits);

      var xre = [];

      var bandWidthLimits;
      return p
        .then((aBandWidthLimits)=>{
            bandWidthLimits = aBandWidthLimits;
            var hours = {};
            Object.keys(bandWidthLimits).forEach(whId=>{
               Object.keys(bandWidthLimits[whId]).forEach(hour=>{
                  if(!hours[hour]) hours[hour] = []
                  hours[hour].push(whId);
               });
            });

            return dotq.linearMap({array:Object.keys(hours), action: function(hour){
               var after = moment(mockedTs).add(-1*hour, 'hour').toISOString();
               var whIds = hours[hour];
               return app.knex("transferlog")
                 .select("tl_webhosting_id")
                 .sum("tl_bytes_sum AS tl_bytes_sum")
                 .whereIn("tl_webhosting_id", whIds)
                 .whereRaw("tl_timestamp > ?", after)
                 .groupBy('tl_webhosting_id')
                 .then(rows=>{
                    rows.forEach(row=>{
                        var limit = bandWidthLimits[row.tl_webhosting_id][hour] * 1024*1024; // it is in mbytes
                        // console.log("shit", rows, after, hour, limit, row.tl_bytes_sum)
                        if(limit < row.tl_bytes_sum)
                           xre.push(row.tl_webhosting_id);
                    })
                 })
            }});

        })
        .then(()=>{
          return xre;
        })

   }

   re.Compress = function(){

      const before = moment().add(-1*app.config.get('compress_vts_entries_after_days'), 'day').toISOString();

      console.log("VTS Compress", before);

      return app.knex.transaction(trx=>{
        var stats;
        var s = trx("transferlog")
         .select("tl_webhosting_id")
         .sum("tl_bytes_in AS tl_bytes_in")
         .sum("tl_bytes_out AS tl_bytes_out")
         .sum("tl_bytes_sum AS tl_bytes_sum")
         .sum("tl_hits AS tl_hits")
         .max("tl_timestamp AS tl_timestamp")
         .whereRaw("tl_description!='[archived]' AND tl_timestamp < ?", before)
         .groupBy('tl_webhosting_id')

         // console.log(s.toSQL());
         return s.then((aStats)=>{
            stats = aStats;
            // console.log(stats);process.reallyExit();

            return trx("transferlog").whereRaw("tl_description!='[archived]' AND tl_timestamp < ?", before).delete()
         })
         .then(()=>{
            return dotq.linearMap({array: stats, action:function(row){
               row.tl_description = "[archived]";
               return trx("transferlog").insert(row);
            }})
         })

      })

   }

   re.ListWebstoresExceededLastTime= function(bwLimits){
      return app.knex("transferlog").max("tl_timestamp AS tl_max").then(rows=>{
         // console.log("last timestamp:", rows);
         if((!rows)||(!rows[0].tl_max)) return [];
         return re.ListWebstoresExceededCurrently(bwLimits, rows[0].tl_max);
      })
   }

   re.Truncate = function(whId){
     return app.knex("transferlog").whereRaw("tl_webhosting_id=?", [whId]).delete().return();
   }


   re.ReprotectAuthentication = function(){
      return webserverAdmin.ReprotectAuthentication();
   }

   re.FetchAndStoreAndMarkAndRelease = function(){

      console.log("VTS FetchAndStoreAndMarkAndRelease");

      const docroot = require("lib-docroot.js")(app);

      var exceededListBefore;
      var exceededListAfter;
      var bwLimits;
      return re.FetchWebstoresWithBandwidthLimit()
        .then(aBwLimits=>{
           bwLimits = aBwLimits;

           return re.ListWebstoresExceededLastTime(bwLimits);
        })
        .then(aExceededList=>{
           exceededListBefore = aExceededList;

           return re.FetchAndReset();
        })
        .then(rawResponse=>{
           // process raw responses, store entries

           var now = useResponseMsec ? moment(new Date(rawResponse.nowMsec)).toISOString() : moment.now();
           // var now = moment.now();
           return app.knex.transaction(trx=>{

              return dotq.linearMap({array:Object.keys(rawResponse.filterZones), action: function(whId){

                  return dotq.linearMap({array:Object.keys(rawResponse.filterZones[whId]), action: function(domainName){

                      var v = rawResponse.filterZones[whId][domainName];

                      // skipping empty lines
                      if((!v.requestCounter)&&(!v.inBytes)&&(!v.outBytes)) return;

                      return trx("transferlog").insert({
                          tl_webhosting_id: whId,
                          tl_description: domainName,
                          tl_bytes_in: v.inBytes,
                          tl_bytes_out: v.outBytes,
                          tl_bytes_sum: v.inBytes+v.outBytes,
                          tl_hits: v.requestCounter,
                          tl_timestamp: now,
                      }).return();
                  }});

              }});

           });
        })
        .then(()=>{
           return re.ListWebstoresExceededCurrently(bwLimits, app.mockedNow);
        })
        .then(aExceededList=>{
           exceededListAfter = aExceededList;

           var ps = [];

           exceededListAfter.forEach(whId=>{
              ps.push(docroot.SetBandwidthExceededForWebhosting(whId, true));

              var i = exceededListBefore.indexOf(whId);
              if(i > -1)
                 exceededListBefore.splice(i, 1);
           })

           // these ones are not in the exceeded list anymore, so they can be released
           exceededListBefore.forEach(whId=>{
              ps.push(docroot.SetBandwidthExceededForWebhosting(whId, false));
           })

           return Promise.all(ps);
        });

   }

   installCron();

   return re

   function installCron(){
      if(app.vtsCron) return;
      app.vtsCron = true;

      const croner = require("Croner");

      var csp = app.config.get("cron_vts_compress");
      if(csp) {
          croner.schedule(csp, function(){
             return re.Compress();
          })
      }

      csp = app.config.get("cron_vts_process");
      if(csp) {
          croner.schedule(csp, function(){
             return re.FetchAndStoreAndMarkAndRelease()
               .catch(err=>{
                  console.error("We were not able to process VTS stats", err)
               })
          })
      }

   }


}
