module.exports = function(app, libName) {


    var router = app.ExpressPromiseRouter()

    const docrootServer = require("lib-"+libName+".js")(app)


  router.get("/raw/:domain", getDomainExpress(function(d, req,res,next){
       return d.GetRaw().then(data=>{
          req.result = data.toString('utf8');
          next()
       })
    }))

  router.route("/:domain/:entry")
    .post(getDomainExpress(function(d,req,res,next){
    	  // changes a redirect entry (like www.example.com for example.com)
       return d.AddRedirect(req.params.entry, req.body.json)
         .then(()=>{
            return d.Save()
         })
         .then(()=>{
              app.InsertEvent(req, {e_event_type: "redirect-create", e_other: true})
         })

	  }))
    .delete(getDomainExpress(function(d, req,res,next){
    	  // delete a subdomain docroot entry (like www.example.com for example.com)
        // if no more entries left, delete the file
       return d.RemoveRedirect(req.params.entry)
         .then(()=>{
            return d.Save()
         })
         .then(()=>{
              app.InsertEvent(req, {e_event_type: "redirect-remove", e_other: true})
         })

	  }))


  router.route("/:domain")
    .get(getDomainExpress(function(d, req,res,next){
       req.result = d.GetEntries()
       next()
    }))
    .put(getDomainExpress(function(d,req,res,next){
    	 // adds a new redirect entry (if it does not exists yet)
       return d.AddRedirect(req.body.json.host, req.body.json)
         .then(()=>{
            return d.Save()
         })
         .then(()=>{
              app.InsertEvent(req, {e_event_type: "redirect-create", e_other: true})
         })

    }))
    .delete(getDomainExpress(function(d, req,res,next){
        // deletes the complete file
        return d.Delete()
         .then(()=>{
              app.InsertEvent(req, {e_event_type: "redirect-remove"})
         })

	  }))


  // return domains configured with redirect
  router.route("/")
     .get(function(req,res,next){
         return docrootServer.GetDomainRedirects()
           .then(ds =>{
              req.result = ds
              next()
           })
	   })


  return router

  function getDomainPromise(req) {
    return docrootServer.GetDomainRedirect(req.params.domain)
  }

  function getDomainExpress(callback) {
    return function(req,res,next) {
        if(req.result) return

        return getDomainPromise(req)
          .then(d=>{
             var p = callback(d, req,res,next)
             if((p)&&(p.then))
               p.then(()=>{
                 if(!req.result)
                   req.result = "ok"
                 next()
               })

             return p

          })
    }
  }

}
