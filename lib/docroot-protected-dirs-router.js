module.exports = function(app) {

    var hosting = app.MonsterInfoWebhosting
    var docroot = require("lib-docroot.js")(app)

    var common_docroot = require("common-docroot.js")(app, docroot)

    var itemRouter = app.ExpressPromiseRouter({mergeParams: true})

    itemRouter.route("/")
      .get(common_docroot.getWebhostingInfoDomainExpress(function(wh, d, req,res,next){
          req.result = d.GetProtectedDirs()
          next()
      }))

      .put(common_docroot.getWebhostingInfoDomainExpress(function(wh, d, req,res,next){
         return d.AddProtectedDir(req.body.json.dir)
           .then(()=>{
              return d.Save(true)
           })
           .then(()=>{
                app.InsertEvent(req, {e_event_type: "protected-dir-create", e_other: true})
           })


      }))
      .delete(common_docroot.getWebhostingInfoDomainExpress(function(wh, d, req,res,next){
         var p;
         if(typeof req.body.json.dir == "undefined")
            p = d.DelAllProtectedDirs();
          else
            p = d.DelProtectedDir(req.body.json.dir);
          
         return p.then(()=>{
              return d.Save(true)
           })
           .then(()=>{
                app.InsertEvent(req, {e_event_type: "protected-dir-remove", e_other: true})
           })

      }))


    var router = app.ExpressPromiseRouter()
    router.use("/:webhosting_id/:domain/",  itemRouter)

  return router


}
