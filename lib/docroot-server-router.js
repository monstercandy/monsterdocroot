module.exports = function(app, subsystem) {

    var docroot = require("lib-docroot.js")(app)

    var common_docroot = require("common-docroot.js")(app, docroot)

    var router = app.ExpressPromiseRouter()

    router.post( "/rehash", function (req, res, next) {
        return req.sendOk(
          app[subsystem].Rehash()
           .then(()=>{
                app.InsertEvent(req, {e_event_type: "server-rehashed"})
           })

        )
    })

    router.get("/:webhosting_id/:domain/raw",common_docroot.getWebhostingInfoDomainExpress(function(wh,d,  req,res,next){
        return app[subsystem].GetRaw(wh, d)
          .then((c)=>{
	         req.result = c
 	         next()
          })
    }))

    router.route("/:webhosting_id/:domain/hooks")
      .get(common_docroot.getWebhostingInfoDomainExpress(function(wh,d,  req,res,next){
         return app[subsystem].GetHooks(wh, d)
          .then((c)=>{
              req.result = c;
              next();
          });
      }))
      .post(common_docroot.getWebhostingInfoDomainExpress(function(wh,d,  req,res,next){
         return app[subsystem].SetHooks(wh, d, req.body.json)
          .then((c)=>{
              app.InsertEvent(req, {e_event_type: "hook-changed", e_other: true});
              req.result = "ok";
              next();
          });
      }))


    return router
}
