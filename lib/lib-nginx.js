/*
add support for adding this block in case of domConf.httpsOnly:

    if ($scheme = http) {
        return 301 https://$server_name$request_uri;
    }

*/
var lib = module.exports = function(app) {

   const UWSGI_NGINX = true


   var directory = app.config.get("nginx_vhosts_directory")
   var hookdir = app.config.get("nginx_hooks_directory")
   var certdir = app.config.get("tls_certificate_store_directory")

   var common = require("common.js")(app)

   const awstats_nginx_per_domain_location_block = app.config.get("awstats_nginx_per_domain_location_block")
   const cmd_dnginx = app.config.get("cmd_dnginx");

   var auth_dir_root_path = app.config.get("nginx_auth_directory");
   var hook_dir_root_path = app.config.get("nginx_hooks_directory");

   var re = {"type":"nginx"}

   re.Save = function(webhosting, domain, helper, forceUsingFullDocrootPath, forceReturn, webstoreSettings) {

      var domain_name = domain.GetName()
      var domConf = domain.GetAll()
      if(domConf.suspended) return re.Delete(webhosting, domain)

      app.InsertEvent(null, {e_event_type: "docroot-regen", e_other: {"server":"nginx","webhosting":webhosting.wh_id,"domain":domain_name}})

      // console.log("foo", webhosting, domain, helper)
      // webhosting looks like: {"wh_id": storage_id, "extras":{"web_path": "/web/w3/"+storage_id+"-12345678901234567890123456"}}

      var containerName = "";
      if(domConf.webappEffective.container)
          containerName = ", container: "+domConf.webappEffective.container;

      var ps = []
      var r = ""

      Object.keys(helper).forEach(docroot=> {

         var servers = helper[docroot].slice()
         var server_names = servers.join(" ")
         server_name = servers[0]


         var listen_conf = domConf.primary ? "common/listen-default.conf" : "common/listen.conf"

         var default_proxy_type; // this is routing to the local apache instance


         var fullPathToDocroot =
           forceUsingFullDocrootPath ?
           forceUsingFullDocrootPath :
           domain.GetFullPathForDocroot(docroot)

         var logs
         var prom = domain.GetFullPathForLogfiles()
          .then(aLogs=>{
               logs = aLogs
               return re.HookFiles(webhosting, domain_name, domConf.certificate_id)
          }).then(hm =>{

               var append = ""
               var inner = `    # webapp type: ${domConf.webappEffective.type}${containerName}\n`;
               if(!hm.hooks.complete) {
                  inner += `
    include ${listen_conf};
    server_name ${server_names};
    set \$mm_webhosting_id "${webhosting.wh_id}";
`
               if(domConf.primary) {
                  inner += `
    if (\$redirect_http_host) {
        rewrite ^ \$redirect_http_host redirect;
    }
`
               }

               if(domConf.httpsonly) {
                  inner += `
    if ($scheme = http) {
        return 301 https://$host$request_uri;
    }
`
               }

               if(domConf.hstsheaders) {
                  inner += `
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;
`
               }

               if(domConf.anticlickjackingheaders) {
                  inner += `
    add_header X-Frame-Options "SAMEORIGIN";
`
               }


               if(UWSGI_NGINX){
                   if((domConf.webappEffective.type == "host-legacy")&&(domConf.uwsgi)) {
                     inner += "    set \$uwsgi_port ${domConf.uwsgi};\n";
                     default_proxy_type = "host-legacy-uwsgi";
                   }
               }


                 if((hm.hooks.cert_key)&&(hm.hooks.cert_fullchain)) {
                      inner += `
    ssl_certificate "${certdir}/${domConf.certificate_id}.fullchain.crt";
    ssl_certificate_key "${certdir}/${domConf.certificate_id}.key";
`
                 }

                 if(!hm.hooks.base) {

                   inner += `
    access_log "${logs.access}" main;
    error_log "${logs.error}" error;
`
                 if(logs.syslog_waf_socket_path_in_conf) {
                   inner += `
    access_log "syslog:server=unix:${logs.syslog_waf_socket_path_in_conf}" postdata if=\$being_proxied;
`
                 }

              if(domConf.postdata)
                   inner += `
    access_log "${logs.post}" postdata if=\$being_proxied;
`

              if((!forceUsingFullDocrootPath)&&(forceReturn)) {
                inner +=   "\n    # force returning stuff.\n";
                var message = lib.escapeStringForConfigFile(forceReturn.message);
                inner += `    return ${forceReturn.code} \"${message}\";
`;


              } else {



                   // we skip the apache layer for the following application types:
                   if(domConf.webappEffective.type == "webserver-http") {
                       inner += `
    location @target {
        set $vhost $host;
        include common/params_proxy;
        proxy_pass http://${domConf.webappEffective.container}:8080;
    }
`;
                   }
                   else
                   if(domConf.webappEffective.type == "webserver-unix") {
                       inner += `
    location @target {
        set $vhost $host;
        include common/params_proxy;
        proxy_pass http://unix:${domConf.webappEffective.unix_socket_path};
    }
`;
                   }
                   else
                   if(domConf.webappEffective.type == "uwsgi") {
                       inner += `
    location @target {
        set $being_proxied 1;

        include common/params_uwsgi;
        uwsgi_pass unix://${domConf.webappEffective.unix_socket_path};
    }
`;
                   }
                   else
                   if(domConf.webappEffective.type == "fastcgi") {
                       inner += `
    location @target {
        set $being_proxied 1;

        include common/params_fastcgi;
        # Mitigate https://httpoxy.org/ vulnerabilities
        fastcgi_param HTTP_PROXY "";
        fastcgi_param SCRIPT_FILENAME /web$fastcgi_script_name;
        fastcgi_pass unix://${domConf.webappEffective.unix_socket_path};
    }
`;
                   }
                   else
                   if(!default_proxy_type) {
                      default_proxy_type = "host-legacy"
                   }


                   if(forceUsingFullDocrootPath) {
                     inner +=   "\n    # force using this path (webhosting package has expired)\n";
                   }

                   inner += `    set $doc_root "${fullPathToDocroot}";

    include common/vhost.conf;
`
                   if(default_proxy_type) {
                    // if default_proxy_type is defined, then there is a common helper file
                   inner += `
    include common/target-${default_proxy_type}.conf;
`
                   }

              }

              var authFileFullPath = `${auth_dir_root_path}/${webhosting.wh_id}.txt`;


              if((awstats_nginx_per_domain_location_block)&&(domConf.awstats)) {

                 // note: you do not need a try_files handler here!
                 var awstatsRoot = domain.GetAwstatsDirectoryDomainRoot();
                      inner += `
    location ^~ "/awstats" {
       auth_basic "Authentication required";
       auth_basic_user_file "${authFileFullPath}";
       autoindex on;
       alias "${awstatsRoot}";
       index awstats.${server_name}-${webhosting.wh_secretcode}.html;
    }
`

              }

              if(!domConf.skipnginxstaticfiles) {

                 inner += `
    location ~* \.(gif|jpg|jpeg|png|ico|wmv|3gp|avi|mpg|mpeg|mp4|flv|mp3|mid|js|css|html|htm|wml|txt|md|rb|scss|ttf|woff|woff2|eot|otf|json|zip|pdf|tar|odt|svg|doc|docx|ppt|pptx)\$ {
        root \$doc_root;
        try_files \$uri \$uri/ @target;
        expires 1d;
    }
`

              }



                   var rootIsNotConfigured = true
                   var dirsAlreadyConfigured = {};
                   if(!domConf["protected-dirs"]) domConf["protected-dirs"] = [];
                   domConf["protected-dirs"].forEach(path => {
                      if(path == "/") rootIsNotConfigured = false
                      dirsAlreadyConfigured[path] = true;

// note: try_files is essential here, as simple return has higher priority and the auth handler wouldnt be executed!
                      inner += `
    location ^~ "${path}" {
       auth_basic "Authentication required";
       auth_basic_user_file "${authFileFullPath}";
       try_files password-authenticated-site @target;
    }
`

                   })

                 if(domConf.djangostatic) {
                    Array("/media", "/static").forEach(path=>{
                       if(dirsAlreadyConfigured[path]) return;

                 inner += `
    # serving django specific static files as is from $path
    location ${path} {
        alias "\$doc_root${path}";
    }
`
                    })
                  }

                  if(!domConf.dontblockxmlrpc) {
                 inner += `
    location /xmlrpc.php {
        deny all;
    }
`
                  }


                  if(rootIsNotConfigured) {
                       if(domConf.timeout)
                          inner += `    proxy_read_timeout ${domConf.timeout};\n`


                      // in case the request is submitted to apache, we dont try to lookup the index.html files
                      // as the user might have customized this behaviour via .htaccess files:
                      var target_line = default_proxy_type == "host-legacy" ? `
       return 450;
`
                          :
                       `
       root \$doc_root;
       try_files \$uri/index.html \$uri/index.htm @target;
`;

                       inner += `
    location / {

       if (\$frame_http_host) {
#         add_header Content-Type text/html;
         return 200 "<!DOCTYPE html><html><frameset cols='100%'><frame src='\$frame_http_host' frameborder='0'></frameset></html>";
       }

       ${target_line}
    }
`


                  }


              if(domConf.nginxgzip) {

                 inner += `
gzip on;
gzip_disable "msie6";
gzip_proxied any;
gzip_types
    text/plain
    text/css
    text/js
    text/xml
    text/javascript
    application/javascript
    application/x-javascript
    application/json
    application/xml
    application/rss+xml
    image/svg+xml;
`

              }

             if(domConf.forceredirect) {
                 inner += `
  if (\$redirect_http_host) {
        rewrite ^ \$redirect_http_host redirect;
  }
`
             }


                 } else {
                   inner += `
    include "${hook_dir_root_path}/${hm.hook_prefix}base.conf";
`

                 }


                 if(hm.hooks.append)
                   append = `
    include "${hook_dir_root_path}/${hm.hook_prefix}append.conf";
`


               } else {
                   inner = `
    include "${hook_dir_root_path}/${hm.hook_prefix}complete.conf";
`
               }

               r += `server {
${inner}
${append}
}

`


         })


         ps.push(prom)


      })


      return Promise.all(ps)
        .then(()=>{
             return re.Write(webhosting, domain, r, true)
        })
        .then(()=>{
           return re.RehashLater()
        })

   }

   re.RehashBase = function(){

      return app.commander.spawn({executable: cmd_dnginx, args:['-t']})
        .then((h)=>{
           return h.executionPromise
        })
        .then(()=>{
             return app.commander.spawn({executable:cmd_dnginx, args:['-s','reload']})
        })
        .then(h=>{
           return h.executionPromise
        })
        .then(res=>{
           console.log("nginx rehash output:", res.output)
        })
   }

   re.Reopen = function(){

      return app.commander.spawn({executable:cmd_dnginx, args: ['-s','reopen']})
        .then(h=>{
           return h.executionPromise
        })
        .then(res=>{
           console.log("nginx reopen output:", res.output)
        })

   }

   common.configureRehashAndDelete(re, directory, hookdir, certdir)

   return re

}

lib.escapeStringForConfigFile = function(s){
   if(!s) return "";
   return s.replace(/"/g, '\\"').replace(/\n/g, "\\n")
}
