var lib = module.exports = function(pathes, options) {
	options = options || {}
	const fs = require("MonsterDotq").fs()

    var ps = []

    var arrayMode = Array.isArray(pathes)
    var v = arrayMode ? pathes : Object.keys(pathes)

    var re = arrayMode ? [] : {}

	v.forEach(key=>{
		var path = arrayMode ? key : pathes[key]

		ps.push( fs.statAsync(path).then((st)=>{
			var ok = true
			if(Object.keys(options).length > 0) {
				Array("isFile", "isDirectory", "isBlockDevice", "isCharacterDevice").some((c)=>{
					if((options[c])&&(!st[c]())) {
						ok = false
						return true
					}
				})
		    }
			if(!ok) return

			if(arrayMode)
			  re.push(path)
		    else
		      re[key] = path

		}).catch(()=>{}))
	})

	return Promise.all(ps)
	  .then(()=>{
	  	 return Promise.resolve(re)
	  })
}
