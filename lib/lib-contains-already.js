const path = require("path")

var lib = module.exports = function(dir, domain, complete_host, wh_id) {

	    	// domain = foo.femforgacs.hu
	    	// host = www.foo.femforgacs.hu

	    	// should check:
	    	// femforgacs.hu
	    	// hu

   const MError = require('MonsterExpress').Error
   const fs = require("MonsterDotq").fs()


   const firstLine = require("FirstLineAsJSON")
	 

   return lib.FilterFiles(dir, domain, true, wh_id)//  // we dont filter for domain anymore, since we want to too strict
     .then(finalFiles=>{
     	"use strict"

     	var ps = []

        // console.log(domain, complete_host, finalFiles)
     	for(let fileName of finalFiles) {
     	   ps.push( 
             firstLine.ReadAsync(fileName)
     	     .then(function(d){
                  if(!d.suspended) {
                      var enumSource = Array.isArray(d.entries) ? d.entries : d
         	     	  for(var q of enumSource) {
         	     	  	 if(q.host == complete_host)
         	     	  	 	throw new MError("HOST_ALREADY_CONFIGURED")
         	     	  }
                  }

     	     	  return Promise.resolve()
     	     })
             .catch(ex=>{
                // we swallow any possible exceptions here due to robustness
                if(ex.message == "HOST_ALREADY_CONFIGURED")
                    throw ex
             }) 
          )
     	}

     	return Promise.all(ps)

     })

}

lib.GetDomainNameFromFilename = function(path, q) {
    var b = path.basename(q)
    b = b.substr(0, b.length -5) // we strip  trailing .json here
    return b

}

lib.FilterFiles = function(dir, mainDomain, getAll, wh_id) {    
    const mainDomainConfigFile = wh_id ? '/'+wh_id+'/'+mainDomain+".conf" : "------------";
	var find = require('promise-path').find;
	return find(path.join(dir,'/**/*.conf'))
	    .then(function(files) {
            var re = []
            for(var q of files) {            	
            	var b = lib.GetDomainNameFromFilename(path, q)
            	if(b == mainDomain) {
                    // console.log(q);
                    if((!wh_id)||(q.endsWith(mainDomainConfigFile)))
                      continue
                }

            	// console.log(mainDomain, b, q)
            	if((getAll)||(mainDomain.indexOf(b) > -1)||(b.indexOf(mainDomain) > -1))
            		re.push(q)
            }
	    	// console.log(files, re)

	    	return re;
	    });

}
