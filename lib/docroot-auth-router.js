module.exports = function(app) {

    var AuthLib = require("lib-auth.js")

    var authy = AuthLib(app)


    var router = app.ExpressPromiseRouter()

    router.get( "/", function (req, res, next) { // this one should list the web storages which have currently authentication configured
        return authy.ListStorages()
          .then(d=>{
             req.result = d

             next()
          })

    })

    router.delete("/:storage_id/:username", function(req,res,next){
         var as
         return req.sendOk(authy.Open(req.params.storage_id)
           .then(aAs=>{
               as = aAs
               return as.RemoveAccount(req.params.username)
           })
           .then(()=>{
               return as.Save()
           })
           .then(()=>{
              app.InsertEvent(req, {e_event_type: "grants-delete", e_username: req.params.username})
           })
         );

    })

    router.route( "/:storage_id")
         .get(function(req,res,next){
             return authy.Open(req.params.storage_id)
               .then(as=>{
                  req.result = as.GetAccounts()
                  next()
               })

         })
         .put(function(req,res,next){
             var as
             return req.sendOk(
              authy.Open(req.params.storage_id)
               .then(aAs=>{
                   as = aAs
                   return as.AddAccount(req.body.json)
               })
               .then(()=>{
                   return as.Save()
               })
               .then(()=>{
                  app.InsertEvent(req, {e_event_type: "grants-add", e_username:req.body.json.username})
               })
             );
         })

  router.authExt = AuthLib.authExt


  return router

}
