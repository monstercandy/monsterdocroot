var lib = module.exports = function(app) {

  const path = require('path')
  const fs = require("MonsterDotq").fs()

  const MError = require('MonsterExpress').Error

  const moment = require("MonsterMoment")
  const dotq = require("MonsterDotq");

  const validHookCategories = Array("complete","base","append");

  const vali = require("MonsterValidators").ValidateJs();

  var modre = {}

  // this should go first, really
  modre.configureRehashAndDelete = function(re, directory, hookdir, certdir) {
     const ms = app.config.get("rehash_delay_seconds")  * 1000

     re.RehashLater = function(){
        if(!re.rehashTimer) {

          re.rehashTimer = setTimeout(function(){
              re.rehashTimer = null

              re.Rehash().catch(ex=>{
                 console.error("Timed rehashing failed", ex);
              })
          }, ms);

        }

        return Promise.resolve()
     }
     re.Rehash = function(){
        if(re.rehashTimer) {
          clearTimeout(re.rehashTimer)
          re.rehashTimer = null
        }

        return re.RehashBase()
          .catch(ex=>{

               app.InsertException(null, {message:"WEBSERVER_REHASHING_FAILED", server: re.type}, ex);

               throw ex

          })
     }

     re.GetPathes = function(webhosting, domain) {
        var domain_name = domain.GetName()
        const fullDir = path.join(directory, ""+webhosting.wh_id)
        const fullFile = path.join(fullDir, domain_name+".conf")
        return {dir: fullDir, file: fullFile}
     }

     re.GetRaw = function(webhosting, domain) {
        var p = re.GetPathes(webhosting, domain)
        return fs.readFileAsync(p.file, "utf8")
          .catch(x=>{
             return Promise.resolve("")
          })
     }

     re.GetHooks = function(webhosting, domain){
        var domain_name = domain.GetName();
        return re.HookFiles(webhosting, domain_name)
          .then(hf=>{

            var re = {}
            return dotq.linearMap({array:Object.keys(hf.hooks), action:function(c){
                return fs.readFileAsync(hf.hooks[c], "utf8")
                .then(content=>{
                   re[c] = content;
                })
            }})
            .then(()=>{
              return re;
            })

          })
     }

     re.SetHooks = function(webhosting, domain, data, dontSave){
        var domain_name = domain.GetName();
        var hook_prefix = getHookPrefix(webhosting, domain_name);
        var constraints = {};
        validHookCategories.forEach(c=>{
          constraints[c] = {isString:{noXssFiltering:true}};
        });
        return vali.async(data, constraints)
          .then(d=>{
              return dotq.linearMap({array:Object.keys(d), action:function(c){
                  var fullFn = path.join(hookdir, hook_prefix+c+".conf");
                  if(d[c])
                    return fs.writeFileAsync(fullFn, d[c])
                  else
                    return fs.ensureRemovedAsync(fullFn);
              }})
          })
          .then(()=>{
             // need to resave the main config as well
             if(!dontSave) {
               return domain.Save();
             }
          })
     }
     re.DeleteHooks = function(webhosting, domain) {
        var data = {};
        validHookCategories.forEach(c=>{
          data[c] = "";
        });
        return re.SetHooks(webhosting, domain, data, true);
     }


     re.Delete = function(webhosting, domain) {
        const f = re.GetPathes(webhosting, domain)
        return fs.ensureRemovedAsync(f.file)
          .then(()=>{
             return re.DeleteHooks(webhosting, domain);
          })
          .then(()=>{
              return fs.rmdirAsync(f.dir).catch(ex=>{})
          })
          .then(()=>{
              return re.RehashLater()
          })
     }

     function getHookPrefix(webhosting, server_name) {
        return webhosting.wh_id+"-"+server_name+"-";
     }

     re.HookFiles = function(webhosting, server_name, certificate_id) {
         const existsLib = require("lib-exists.js")

         var hook_prefix = getHookPrefix(webhosting, server_name);
         var hookFiles = {}
         validHookCategories.forEach(q=>{
           hookFiles[q] = path.join(hookdir, hook_prefix+q+".conf")
         })

         if((certdir)&&(certificate_id)){
            hookFiles.cert_key = path.join(certdir, certificate_id+".key")
            hookFiles.cert_fullchain = path.join(certdir, certificate_id+".fullchain.crt");
         }

         return existsLib(hookFiles, {isFile: true})
           .then(hooks =>{
              return Promise.resolve({"hook_prefix": hook_prefix, "hooks": hooks})
           })

     }

     re.WriteFile = function(directory, file, content) {
        if((!content)||(content.length <= 0)) {
          return fs.ensureRemovedAsync(file).then(()=>{return "removed"});
        }

        return Promise.resolve()
            .then(()=>{
                if(!directory) return;

                return fs.mkdirAsync(directory).catch(ex=>{})
            })
            .then(()=>{
                return fs.writeFileAsync(file, content)
            })

     }
     re.Write = function(webhosting, domain, r, chmodToReadonly) {
         const f = re.GetPathes(webhosting, domain)
         return re.WriteFile(f.dir, f.file, r)
           .then((re)=>{
              if(!chmodToReadonly) return;
              if(re == "removed") return;

              return fs.chmodAsync(f.file, 0o600);
           })
     }

     re.GetWebhostingsDomains = function() {
         return modre.getConfFiles(directory)
     }

  }


  modre.hostToPrefixBasedOnDomain = function(host, domain, acceptAnything) {
    if(!host) host = ""
    if(acceptAnything) return host

    var end = host.substr(host.length - domain.length, domain.length)
    if(end != domain) throw new MError("INVALID_HOST")

    var begin = host.substr(0, host.length - domain.length)
      if(begin.length > 0) {
        if(!begin.match(/\.$/))
          throw new MError("INVALID_HOST")

        begin = begin.substr(0, begin.length - 1)

        if(begin.length <= 0)
          throw new MError("INVALID_HOST")
      } else return null // this is the intended behaviour here, since the validator wont run then

      return begin
   }


  modre.whDomainPairsAsString = function(pairs) {
     var strVersion = []
      pairs.forEach((q)=>{
        strVersion.push(q.wh+"/"+q.domain)
      })
      return strVersion
  }

  modre.getConfFiles = function(dir) {
    return modre.getFiles(dir, "*.conf")
  }
  modre.getFiles = function(dir, pattern) {
      var find = require('promise-path').find;
      return find(path.join(dir,'/**/'+pattern))
        .then(files=>{
            var aDocrootDir = dir
            if(aDocrootDir.match(/^.\//)) aDocrootDir = aDocrootDir.substr(2)
            // console.log("foo?", files, docrootDir, aDocrootDir)

            var re = []
            for(var q of files) {

              var f = q.substr(aDocrootDir.length + 1)

              var myRegexp = new RegExp('^([0-9]+)/(.+)\.conf$');
              var match = myRegexp.exec(f);
              if(!match) continue
              if(!match[1]) continue

              re.push({"wh":match[1], "domain":match[2]})

            }


            return Promise.resolve(re)
        })

  }

  return modre
}
