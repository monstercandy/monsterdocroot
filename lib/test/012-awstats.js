require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../docroot-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){
    "use strict"

    const MError = require('MonsterExpress').Error
    const path = require("path")

    var origCommander = app.commander

    var storage_id = 21001
    var certificate_id = 12345
    var Common = require("./000-common.js")(assert)

        const fs = require("MonsterDotq").fs()

    const awstats_config_dir = app.config.get("awstats_config_directory")

     var oldPath = path.join(awstats_config_dir, "awstats.awstats.hu-12345678901234567890123456.conf")
     var newPath = path.join(awstats_config_dir, "awstats.foobar.hu-12345678901234567890123456.conf")

     var template_awstats_allowed = { t_awstats_allowed: true }

    describe("delete test", function(){

        addDocroot()

        it('create awstats should be rejected if the webhosting template does not allow it', function(done) {

            mapi.post( "/awstats/"+storage_id+"/awstats.hu", {lazy: true}, function(err, result, httpResponse){
                assert.propertyVal(err, "message", "AWSTATS_NOT_ALLOWED");

                done()
             })
        })    

        addDocroot(template_awstats_allowed)

        createAwstats()

        it("awstats config file should exists", function(){
            return fs.statAsync(oldPath)
        })

            it('awstats location should be presenti n raw nginx data', function(done) {

                mapi.get( "/nginx/"+storage_id+"/awstats.hu/raw", function(err, result, httpResponse){
                    assert.isNull(err);
                    assert.isNotNull(result);
                    assert.ok(result.indexOf('location ^~ "/awstats" {') > -1);
                    assert.ok(result.indexOf('root "') > -1);
                    // assert.ok(result.substr(0,3) == '#{"');

                    done()
                 })
            })        

        deleteDomain("awstats.hu")

        it("awstats config file should not exist anymore", function(){
            return fs.statAsync(oldPath)
              .then(()=>{
                 throw new Error("should have failed")
              })
              .catch(err=>{
                 assert.propertyVal(err, "code", "ENOENT")
              })
        })

    })


    describe("test", function(){

        addDocroot(template_awstats_allowed)

        createAwstats()


        awstatsTrue("awstats.hu")
 

        it('check raw awstats config', function(done) {

            mapi.get( "/awstats/"+storage_id+"/awstats.hu/raw", function(err, result, httpResponse){
                assert.isNull(err);
                assert.ok(result.indexOf('access.log"\nSiteDomain="awstats.hu"\nHostAliases="www.awstats.hu"\n') > -1);

                done()
             })
        })


        it('delete awstats', function(done) {

            mapi.delete( "/awstats/"+storage_id+"/awstats.hu", {}, function(err, result, httpResponse){
                assert.isNull(err);
                assert.equal(result, "ok");

                done()
             })
        })

        it('awstats should be false in domain config', function(done) {

            mapi.get( "/docroots/"+storage_id+"/awstats.hu/", function(err, result, httpResponse){
                assert.isNull(err);
                assert.propertyVal(result, "awstats", false);

                done()
             })
        })

    })

    describe("cleanup", function(){

        createAwstats()

        it('global regenerate method should regenerate the previous 1 awstats config file', function(done) {

            mapi.post( "/awstats/regenerate", {}, function(err, result, httpResponse){
                assert.isNull(err);
                assert.equal(result, 1);

                done()
             })
        })

        it("prepare a domain that would have an awstats config file but no flag", function(done){


             mapi.put( "/docroots/"+storage_id+"/flagged.hu/entries", {"host": "www.flagged.hu", "docroot": "/flagged.hu/pages"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                var awstatsFile = path.join(awstats_config_dir, "awstats.flagged.hu-12345678901234567890123456.conf")
                fs.writeFileSync(awstatsFile, "test")

                done()  
             })

        })

        it("maintane should make things right again in awstats domain", function(done){

             
             fs.renameAsync(oldPath, newPath)
               .then(()=>{
                     mapi.post( "/cleanup/maintane", {}, function(err, result, httpResponse){
                        assert.deepEqual(result, { 
                            docroot: { removed: [] }, 
                            webserver: { removed: [], created: [] }, 
                            awstats: {
                                created:["21001/awstats.hu"], 
                                removed: ["awstats.foobar.hu-12345678901234567890123456.conf"], 
                                flagged: ["21001/flagged.hu"]
                            } 
                        });

                        done()  
                     })


               })


        })

        awstatsTrue("flagged.hu")
 
        deleteDomain("flagged.hu")


    })

    
    describe("awstats build", function(){

        // global and per domain awstats processing should not differ here


        it("per domain awstats process", function(done) {

            var commandsExecuted = 0

            app.commander = {
                spawn: function(options, replace) {
                    // console.log("!!!", options, replace)
                    if(commandsExecuted == 0)
                      assert.deepEqual(options,  { executable: '/usr/local/sbin/dnginx', args: [ '-s', 'reopen' ] })
                    if(commandsExecuted == 1) {
                        assert.property(options, "emitter")
                        delete options.emitter
                        assert.deepEqual(options, { omitAggregatedOutput: true, 
                              removeImmediately: true,
                              chain: { executable: '/usr/bin/perl',
                              args:
                               [ '[awstats_main_script]',
                                 '-update',
                                 '-config=[config_name]',
                                 '-configdir=/etc/awstats' ] }})
                        assert.deepEqual(replace, { config_name: 'awstats.hu-12345678901234567890123456',
                                awstats_main_script: '/path/to/awstats.pl' })                        
                    }
                    commandsExecuted++
                    return Promise.resolve({id:commandsExecuted,executionPromise: Promise.resolve({output: "output"})})
                },
            }

            mapi.post( "/awstats/"+storage_id+"/awstats.hu/process", {lazy:true}, function(err, result, httpResponse){
                assert.isNull(err);
                assert.equal(commandsExecuted, 2);
                assert.deepEqual(result, {id:2});

                done()
             })
        })    



        it('global awstats process', function(done) {

            var commandsExecuted = 0
            var emitterCalled = 0
            var emitterSpawnCalled = 0
            var emitterId = 1000
            var emitterClose

            app.commander = {
                EventEmitter: function(){
                    emitterCalled++
                    return {
                        close: function(code){
                            console.log("eventemitter close was called with", code)
                            emitterClose = code || 0
                        },
                        spawn: function(params){
                            console.log("eventemitter spawn was called")
                            assert.deepEqual(params, {"omitAggregatedOutput":true})
                            emitterSpawnCalled++
                            return Promise.resolve({id: emitterId, executionPromise: Promise.reject("noone cares")})
                        }
                    }

                },
                spawn: function(options, replace) {
                    // console.log("!!!", options, replace)
                    if(commandsExecuted == 0)
                      assert.deepEqual(options,  { executable: '/usr/local/sbin/dnginx', args: [ '-s', 'reopen' ] })
                    if(commandsExecuted == 1) {
                        assert.property(options, "emitter")
                        delete options.emitter
                        assert.deepEqual(options, {omitAggregatedOutput: true, 
                              removeImmediately: true,
                              chain: { executable: '/usr/bin/perl',
                              args:
                               [ '[awstats_main_script]',
                                 '-update',
                                 '-config=[config_name]',
                                 '-configdir=/etc/awstats' ] }})
                        assert.deepEqual(replace, { config_name: 'awstats.hu-12345678901234567890123456',
                                awstats_main_script: '/path/to/awstats.pl' })                        
                    }
                    commandsExecuted++
                    return Promise.resolve({id:commandsExecuted,executionPromise: Promise.resolve({output: "output"})})
                },
            }

            mapi.post( "/awstats/process", {lazy:true}, function(err, result, httpResponse){
                assert.isNull(err);
                assert.equal(emitterClose, 0);
                assert.equal(emitterCalled, 1);
                assert.equal(emitterSpawnCalled, 1);
                assert.equal(commandsExecuted, 2);
                assert.deepEqual(result, {id: emitterId});

                done()
             })
        })    



        // per domain awstats build



                it('per domain awstats build', function(done) {

                    var commandsExecuted = 0

                    app.commander = {
                        spawn: function(options, replace) {
                            // console.log("!!!", options, "YYY", options.chain, replace)
                            if(commandsExecuted == 0) {
                              assert.property(options, "emitter")
                              delete options.emitter
                              assert.deepEqual(options, { omitAggregatedOutput: true, 
                                  removeImmediately: true, 
                                  chain: [ { executable: '/usr/bin/perl',
                                  args:
                                   [ '/usr/local/awstats/tools/awstats_buildstaticpages.pl',
                                     '-config=[config_name]',
                                     '-lang=[lang]',
                                     '-month=[month]',
                                     '-year=[year]',
                                     '-awstatsprog=[awstats_main_script]',
                                     '-dir=[awstats_html_output_dir]' ] }]})
                                assert.deepEqual(replace,   { year: '2016',
                                      month: '12',
                                      domain: 'awstats.hu',
                                      wh_secretcode: '12345678901234567890123456',
                                      awstats_main_script: '/path/to/awstats.pl',
                                      lang: 'en',
                                      config_name: 'awstats.hu-12345678901234567890123456',
                                      ym: '201612',
                                      awstats_html_output_dir: './data/awstats.htmls.d/awstats.hu/201612' })
                            }
                            commandsExecuted++
                            return Promise.resolve({id:commandsExecuted,executionPromise: Promise.resolve({output: "output"})})
                        },
                    }

                    mapi.post( "/awstats/"+storage_id+"/awstats.hu/build", {year:"2016", month: "12"}, function(err, result, httpResponse){
                        // console.log(err)
                        assert.isNull(err);
                        assert.equal(commandsExecuted, 1);
                        assert.deepEqual(result, {id:1});

                        done()
                     })
                })    



        it('global awstats build', function(done) {

            var commandsExecuted = 0
            var emitterCalled = 0
            var emitterSpawnCalled = 0
            var emitterId = 2000
            var emitterClose

            app.commander = {
                EventEmitter: function(){
                    emitterCalled++
                    return {
                        close: function(code){
                            console.log("eventemitter close was called with", code)
                            emitterClose = code || 0
                        },
                        spawn: function(params){
                            assert.deepEqual(params, {"omitAggregatedOutput":true})
                            console.log("eventemitter spawn was called")
                            emitterSpawnCalled++
                            return Promise.resolve({id: emitterId, executionPromise: Promise.reject("noone cares")})
                        }
                    }

                },
                spawn: function(options, replace) {
                    // console.log("!!!", options, "YYY", options.chain, replace)
                    if(commandsExecuted == 0) {
                      assert.property(options, "emitter")
                      delete options.emitter
                      assert.deepEqual(options, { omitAggregatedOutput: true, 
                          removeImmediately: true,
                          chain: [ { executable: '/usr/bin/perl',
                          args:
                           [ '/usr/local/awstats/tools/awstats_buildstaticpages.pl',
                             '-config=[config_name]',
                             '-lang=[lang]',
                             '-month=[month]',
                             '-year=[year]',
                             '-awstatsprog=[awstats_main_script]',
                             '-dir=[awstats_html_output_dir]' ] }]})
                        assert.deepEqual(replace,   { year: '2016',
                              month: '12',
                              domain: 'awstats.hu',
                              wh_secretcode: '12345678901234567890123456',
                              awstats_main_script: '/path/to/awstats.pl',
                              lang: 'en',
                              config_name: 'awstats.hu-12345678901234567890123456',
                              ym: '201612',
                              awstats_html_output_dir: './data/awstats.htmls.d/awstats.hu/201612' })
                    }
                    commandsExecuted++
                    return Promise.resolve({id:commandsExecuted,executionPromise: Promise.resolve({output: "output"})})
                },
            }

            mapi.post( "/awstats/build", {year:"2016", month: "12"}, function(err, result, httpResponse){
                // console.log(err)
                assert.isNull(err);

                assert.equal(emitterClose, 0);
                assert.equal(emitterCalled, 1);
                assert.equal(emitterSpawnCalled, 1);

                assert.equal(commandsExecuted, 1);
                assert.deepEqual(result, {id: emitterId});

                done()
             })
        })    







        it('global awstats process and build', function(done) {

            var commandsExecuted = 0
            var emitterCalled = 0
            var emitterSpawnCalled = 0
            var emitterId = 3000
            var emitter_messages = 0
            var emitterClose

            app.commander = {
                EventEmitter: function(){
                    emitterCalled++
                    return {
                        send_stdout: function(msg){
                           console.log("global message:", msg)
                           emitter_messages++
                        },
                        close: function(code){
                            console.log("eventemitter close was called with", code)
                            emitterClose = code || 0

                            assert.equal(emitterClose, 0);
                            assert.equal(emitterCalled, 1);
                            assert.equal(emitterSpawnCalled, 1);

                            assert.equal(commandsExecuted, 3);

                            done()

                        },
                        spawn: function(params){
                            assert.deepEqual(params, {"omitAggregatedOutput":true})
                            console.log("eventemitter spawn was called")
                            emitterSpawnCalled++
                            return Promise.resolve({id: emitterId, executionPromise: Promise.reject("noone cares")})
                        }
                    }

                },
                spawn: function(options, replace) {
                    // console.log("!!!", options, "YYY", options.chain, replace)
                    if(commandsExecuted == 0)
                      assert.deepEqual(options,  { executable: '/usr/local/sbin/dnginx', args: [ '-s', 'reopen' ] })
                    if(commandsExecuted == 1) {
                        assert.property(options, "emitter")
                        delete options.emitter
                        assert.deepEqual(options, { omitAggregatedOutput: true, 
                              removeImmediately: true,
                              chain: { executable: '/usr/bin/perl',
                              args:
                               [ '[awstats_main_script]',
                                 '-update',
                                 '-config=[config_name]',
                                 '-configdir=/etc/awstats' ] }})
                        assert.deepEqual(replace, { config_name: 'awstats.hu-12345678901234567890123456',
                                awstats_main_script: '/path/to/awstats.pl' })                        
                    }                    
                    if(commandsExecuted == 2) {
                      assert.property(options, "emitter")
                      delete options.emitter
                      assert.deepEqual(options, { omitAggregatedOutput: true, 
                          removeImmediately: true,
                          chain: [ { executable: '/usr/bin/perl',
                          args:
                           [ '/usr/local/awstats/tools/awstats_buildstaticpages.pl',
                             '-config=[config_name]',
                             '-lang=[lang]',
                             '-month=[month]',
                             '-year=[year]',
                             '-awstatsprog=[awstats_main_script]',
                             '-dir=[awstats_html_output_dir]' ] }]})
                        assert.deepEqual(replace,   { year: '2016',
                              month: '12',
                              domain: 'awstats.hu',
                              wh_secretcode: '12345678901234567890123456',
                              awstats_main_script: '/path/to/awstats.pl',
                              lang: 'en',
                              config_name: 'awstats.hu-12345678901234567890123456',
                              ym: '201612',
                              awstats_html_output_dir: './data/awstats.htmls.d/awstats.hu/201612' })
                    }
                    commandsExecuted++
                    return Promise.resolve({id:commandsExecuted,executionPromise: Promise.resolve({output: "output"})})
                },
            }

            mapi.post( "/awstats/process-and-build", {year:"2016", month: "12"}, function(err, result, httpResponse){
                // console.log(err)
                assert.isNull(err);

                assert.deepEqual(result, {id: emitterId});
             })
        })    





    })


   function createAwstats(){

        it('create awstats', function(done) {

            mapi.post( "/awstats/"+storage_id+"/awstats.hu", {lazy: true}, function(err, result, httpResponse){
                assert.isNull(err);
                assert.equal(result, "ok");

                done()
             })
        })    
   }

   function addDocroot(webhosting_template){
        it('add a docroot', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id, webhosting_template)

             mapi.put( "/docroots/"+storage_id+"/awstats.hu/entries", {"host": "www.awstats.hu", "docroot": "/awstats.hu/pages"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()  
             })
        })

   }

   function awstatsTrue(domain) {

        it('awstats should be true in domain config for '+domain, function(done) {

            mapi.get( "/docroots/"+storage_id+"/"+domain+"/", function(err, result, httpResponse){
                assert.isNull(err);
                assert.propertyVal(result, "awstats", true);

                done()
             })
        })

   }

   function deleteDomain(domain) {
        it("deleting a domain should delete awstats config file as well: "+domain, function(done){
            mapi.delete("/docroots/"+storage_id+"/"+domain, {}, function(err, result) {
                 assert.equal(result, "ok")
                 done()

            })
        })

   }
})
