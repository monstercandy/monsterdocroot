module.exports = function(assert) {
   return {
     mockedWebhostingInfo: function(should_be, template, extra) {
            return {
                GetInfo: function(wh_id) {
                   if(should_be)
                      assert.equal(wh_id, should_be)
                   return Promise.resolve(extend(
                    {
                      "wh_id": wh_id, 
                      "hello": "world",
                      "wh_secretcode": "12345678901234567890123456",
                      "extras":{"web_path": "/web/w3/"+wh_id+"-12345678901234567890123456"},
                      template: template
                    }, 
                    extra
                   ));
                }
            }
    },
    mockedWebhostingInfoNotFound: function(storage_be) {
            return {
                GetInfo: function(wh_id) {
			        assert.equal(wh_id, storage_be)

        			throw new Error("NOT_FOUND")
        		}
        	}
    }
  }
}
