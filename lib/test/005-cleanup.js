require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../docroot-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){
    "use strict"

    const MError = require('MonsterExpress').Error

    var storage_id1 = 11001
    var storage_id2 = 11002

    var Common = require("./000-common.js")(assert)


    describe("prepare", function(){
        "use strict"


        for (let q of 
            [
              {"storage": storage_id1,"domain":"wh1d1.hu", "host": "www.wh1d1.hu", "docroot":"/docroot/pages"},
              {"storage": storage_id2,"domain":"wh2d1.hu", "host": "www.wh2d1.hu", "docroot":"/docroot/pages"},
              {"storage": storage_id2,"domain":"wh2d2.hu", "host": "www.wh2d2.hu", "docroot":"/docroot/pages"},
            ])
        it('add a docroot: '+q.host, function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(q.storage)


             mapi.put( "/docroots/"+q.storage+"/"+q.domain+"/entries", {"host": q.host, "docroot": q.docroot}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()  
             })
        })



        // we mock this one only here, since the cached version should be used by the subsequent/upcoming calls
        it('create an account', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id1)

             mapi.put( "/auths/"+storage_id1, {"username":"admin","password":"12345678"}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                done()  
             })           
        })     



        it('adding a redirect', function(done) {

             mapi.put( "/redirects/wh1d1.hu", {"host":"foo.wh1d1.hu", "redirect": "http://index.hu/"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()  
             })
        })

        it('adding a redirect', function(done) {

             mapi.put( "/redirects/wh2d1.hu", {"host":"foo.wh2d1.hu", "redirect": "http://index.hu/"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()  
             })
        })

    })

    describe("domain cleanups", function(){

        it('delete a domain by name', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id2)

             mapi.delete( "/cleanup/domains/wh2d1.hu", {}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()  
             })
        })
    })

    describe("webhosting cleanups", function(){

        it('delete a webhosting by id', function(done) {

             mapi.delete( "/cleanup/webhostings/"+storage_id1, {}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()  
             })
        })

    })

    describe("and some verify", function(){


        it('list auths', function(done) {

             mapi.get( "/auths", function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code 

                assert.deepEqual(result, []);

                done()  
             })
        })

        it('list redirects configured', function(done) {

             mapi.get( "/redirects/", function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code 

                assert.deepEqual(result, ["wh1d1.hu"]);

                done()  
             })
        })


        it('list webhostings and domains', function(done) {

             mapi.search( "/docroots/", function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code 

                assert.deepEqual(result, [{"wh":"11002","domain":"wh2d2.hu"}]);

                done()  
             })
        })

    })

})
