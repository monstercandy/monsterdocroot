require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../docroot-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){
    "use strict"

    const MError = require('MonsterExpress').Error
    const path = require("path")

    var storage_id = 22001
    var Common = require("./000-common.js")(assert)

    var moment = require("MonsterMoment");

    //"2017-09-22T19:44:16.878Z",
    const firstVtsResponse = '{"nginxVersion":"1.11.6","loadMsec":1506109415918,"nowMsec":1506109456878,"connections":{"active":93,"reading":0,"writing":8,"waiting":86,"accepted":5257666,"handled":5257666,"requests":17122920},"serverZones":{},"filterZones":{"22001":{"vts.hu":{"requestCounter":36,"inBytes":14245,"outBytes":184929300,"responses":{"1xx":0,"2xx":36,"3xx":0,"4xx":0,"5xx":0,"miss":0,"bypass":0,"expired":0,"stale":0,"updating":0,"revalidated":0,"hit":0,"scarce":0},"overCounts":{"maxIntegerSize":18446744073709551615,"requestCounter":0,"inBytes":0,"outBytes":0,"1xx":0,"2xx":0,"3xx":0,"4xx":0,"5xx":0,"miss":0,"bypass":0,"expired":0,"stale":0,"updating":0,"revalidated":0,"hit":0,"scarce":0}}}}}';

    //"2017-09-22T22:30:56.878Z"
    const secondVtsResponse = '{"nginxVersion":"1.11.6","loadMsec":1506109415918,"nowMsec":1506119456878,"connections":{"active":93,"reading":0,"writing":8,"waiting":86,"accepted":5257666,"handled":5257666,"requests":17122920},"serverZones":{},"filterZones":{"22001":{"vts.hu":{"requestCounter":10,"inBytes":10,"outBytes":20,"responses":{"1xx":0,"2xx":36,"3xx":0,"4xx":0,"5xx":0,"miss":0,"bypass":0,"expired":0,"stale":0,"updating":0,"revalidated":0,"hit":0,"scarce":0},"overCounts":{"maxIntegerSize":18446744073709551615,"requestCounter":0,"inBytes":0,"outBytes":0,"1xx":0,"2xx":0,"3xx":0,"4xx":0,"5xx":0,"miss":0,"bypass":0,"expired":0,"stale":0,"updating":0,"revalidated":0,"hit":0,"scarce":0}}}}}';

    const groupbyVtsResponse = '{"nginxVersion":"1.11.6","loadMsec":1506109415918,"nowMsec":1506109456878,"connections":{"active":93,"reading":0,"writing":8,"waiting":86,"accepted":5257666,"handled":5257666,"requests":17122920},"serverZones":{},"filterZones":{"22001":{"vts2.hu":{"requestCounter":36,"inBytes":14245,"outBytes":184929300,"responses":{"1xx":0,"2xx":36,"3xx":0,"4xx":0,"5xx":0,"miss":0,"bypass":0,"expired":0,"stale":0,"updating":0,"revalidated":0,"hit":0,"scarce":0},"overCounts":{"maxIntegerSize":18446744073709551615,"requestCounter":0,"inBytes":0,"outBytes":0,"1xx":0,"2xx":0,"3xx":0,"4xx":0,"5xx":0,"miss":0,"bypass":0,"expired":0,"stale":0,"updating":0,"revalidated":0,"hit":0,"scarce":0}}}}}';

    const timestampWhenItShouldStillBeExceeded = "2017-09-22T20:44:16.878Z";
    const timestampWhenItIsNotExceededAnymore = "2017-09-23T21:30:56.878Z";

    var webhostingBandwidthLimitCalls = 0;
	 app.GetRelayerMapiPool = function(){
	 	return {
	 		getAsync: function(uri){
	 			webhostingBandwidthLimitCalls++;
	 			assert.equal(uri, "/s/[this]/info/webhosting/bandwidth-limited");
	 			var re = {};
	 			re[storage_id] = {24: 100};
	 			return Promise.resolve({result:re});
	 		}
	 	}
	 }

    describe("vts test", function(){

    	addDocroot();


    	shouldBeEmpty();

    	shouldNotBeExceeded();

        processVtsStats(firstVtsResponse, timestampWhenItShouldStillBeExceeded);


        it('checking whether it was indeed saved', function(done) {
             mapi.post( "/vts/"+storage_id+"/search", {}, function(err, result, httpResponse){
             	// console.log("result", err, result);
             	/*
             	assert.ok(result[0].tl_timestamp);
             	delete(result[0].tl_timestamp);
             	*/
                assert.deepEqual(result,  [{ tl_webhosting_id: 22001,
    tl_description: 'vts.hu',
    tl_bytes_in: 14245,
    tl_bytes_out: 184929300,
    tl_bytes_sum: 184943545,
    tl_timestamp: "2017-09-22T19:44:16.878Z",
    tl_hits: 36 } ]);

                done()
             });
        });


        it('checking whether it was indeed saved (search2 interface)', function(done) {
             mapi.post( "/vts/"+storage_id+"/search2", {}, function(err, result, httpResponse){
                /*
                console.log("result", err, result);
                */
                assert.deepEqual(result,  { count:1, 
        sum:
           { tl_bytes_sum: 184943545,
             tl_bytes_in: 14245,
             tl_bytes_out: 184929300,
             tl_hits: 36 },
                    rows: [{ tl_webhosting_id: 22001,
    tl_description: 'vts.hu',
    tl_bytes_in: 14245,
    tl_bytes_out: 184929300,
    tl_bytes_sum: 184943545,
    tl_timestamp: "2017-09-22T19:44:16.878Z",
    tl_hits: 36 } ]});

                done()
             });
        });

        it('checking whether it was indeed saved (search2 interface, group by)', function(done) {
             mapi.post( "/vts/"+storage_id+"/search2", {groupByWebstore: true}, function(err, result, httpResponse){
                // console.log("result", err, result);
                /*
                assert.ok(result[0].tl_timestamp);
                delete(result[0].tl_timestamp);
                */
                assert.deepEqual(result,  { count:1, 
                    sum:
                               { tl_bytes_sum: 184943545,
                                 tl_bytes_in: 14245,
                                 tl_bytes_out: 184929300,
                                 tl_hits: 36 },
                    rows: [{ tl_webhosting_id: 22001,
    tl_description: 'vts.hu',
    tl_bytes_in: 14245,
    tl_bytes_out: 184929300,
    tl_bytes_sum: 184943545,
    tl_timestamp: "2017-09-22T19:44:16.878Z",
    tl_hits: 36 } ]});

                done()
             });
        });

        it('it should now return as exceeded', function(done) {
             mapi.get( "/vts/exceeded",  function(err, result, httpResponse){
             	// console.log("result", err, result);
                assert.deepEqual(result,  [storage_id]);

                done()
             });

        });

        it('and the corresponding sites should now be marked as bandwidth exceeded', function(done) {
             mapi.get( "/docroots/"+storage_id+"/vts.hu", function(err, result, httpResponse){
             	assert.propertyVal(result, 'bandwidth-exceeded', true);

                done()
             })

        });

        it('now truncating the logs (cheating)', function(done) {
             mapi.post( "/vts/"+storage_id+"/truncate", {}, function(err, result, httpResponse){
             	assert.equal(result, 'ok');

                done()
             })

        });

        shouldBeEmpty();


        processVtsStats(firstVtsResponse, timestampWhenItShouldStillBeExceeded);

        processVtsStats(secondVtsResponse, timestampWhenItIsNotExceededAnymore); // with this timestamp, the only query in the last 24 hours will be this current one


        it('and the bandwidth exceeded flag on the corresponding sites should now be unmarked', function(done) {
             mapi.get( "/docroots/"+storage_id+"/vts.hu", function(err, result, httpResponse){
             	assert.notProperty(result, 'bandwidth-exceeded');

                done()
             })

        });



        it('compressing stuffs', function(done) {
             mapi.post( "/vts/compress", {}, function(err, result, httpResponse){
             	assert.equal(result, 'ok');

                done()
             })

        });



        getCompressedStuff();


        it('compressing stuffs again should not change anything', function(done) {
             mapi.post( "/vts/compress", {}, function(err, result, httpResponse){
             	assert.equal(result, 'ok');

                done()
             })

        });

        getCompressedStuff();

        it("vts url should be exposed along with password", function(done){

             mapi.post( "/vts/vts-url", {validity_min: 60}, function(err, result, httpResponse){
             	// console.log("url", result)
             	assert.property(result, 'url');
             	assert.ok(result.url.match(/^https:\/\/admin:.+@something.foobar.hu\/vts\/$/));

                done()
             })

        })
    })


    describe("grouping", function(){


        it('add a docroot', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id, {})

             mapi.put( "/docroots/"+storage_id+"/vts2.hu/entries", {"host": "www.vts2.hu", "docroot": "/vts2.hu/pages"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        processVtsStats(groupbyVtsResponse);
        processVtsStats(groupbyVtsResponse);


        it('checking whether it was indeed saved (search2 interface)', function(done) {
             mapi.post( "/vts/"+storage_id+"/search2", {groupByDomain: true}, function(err, result, httpResponse){
                /*
                console.log("result", err, result);
                */
                assert.deepEqual(result,  { count:2, 
        sum:
           { tl_bytes_sum: 554830665,
     tl_bytes_in: 42745,
     tl_bytes_out: 554787920,
     tl_hits: 118 },
                    rows: [ { tl_description: '[archived]',
       tl_webhosting_id: 22001,
       tl_timestamp: '2017-09-22T22:30:56.878Z',
       tl_bytes_sum: 184943575,
       tl_bytes_in: 14255,
       tl_bytes_out: 184929320,
       tl_hits: 46 },
     { tl_description: 'vts2.hu',
       tl_webhosting_id: 22001,
       tl_timestamp: '2017-09-22T19:44:16.878Z',
       tl_bytes_sum: 369887090,
       tl_bytes_in: 28490,
       tl_bytes_out: 369858600,
       tl_hits: 72 } ]});

                done()
             });
        });

    });



   function addDocroot(webhosting_template){
        it('add a docroot', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id, webhosting_template)

             mapi.put( "/docroots/"+storage_id+"/vts.hu/entries", {"host": "www.vts.hu", "docroot": "/vts.hu/pages"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

   }

   function getCompressedStuff(){
        it('and see if it happened', function(done) {
             mapi.post( "/vts/"+storage_id+"/search", {}, function(err, result, httpResponse){
             	// console.log("result", err, result);

                assert.deepEqual(result,  [ { tl_webhosting_id: 22001,
    tl_description: '[archived]',
    tl_bytes_in: 14255,
    tl_bytes_out: 184929320,
    tl_bytes_sum: 184943575,
    tl_hits: 46,
    tl_timestamp: '2017-09-22T22:30:56.878Z' } ]);

                done()
             });
        });

   }

   function processVtsStats(response, mockedTimestamp){
        it('processing vts stats', function(done) {

             webhostingBandwidthLimitCalls = 0;

             var vtsCalls = 0;
        	 var oPostForm = app.PostForm;
        	 app.PostForm = function(uri, data, options){
        	 	// console.log("postform", uri, data, options);
        	 	vtsCalls++;
        	 	assert.property(options, "auth");
        	 	assert.ok(options.auth.user);
        	 	assert.ok(options.auth.pass);
        	 	if(vtsCalls == 1) {
        	 	  assert.equal(uri, "https://something.foobar.hu/vts/format/json");
          	 	  return Promise.resolve(response);
        	 	}
        	 	else
        	 	if(vtsCalls == 2) {
        	 	  assert.equal(uri, "https://something.foobar.hu/vts/status/control?cmd=reset&group=*");
          	 	  return Promise.resolve('{}');
        	 	}
        	 	else
        	 		return Promise.reject("foo!")
        	 }


        	 app.mockedNow = mockedTimestamp;


             mapi.post( "/vts/process", {}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                assert.equal(webhostingBandwidthLimitCalls, 1);

                app.PostForm = oPostForm;

                done()
             })
        })

   }

   function shouldNotBeExceeded(){

        it('it should not be returned as exceeded', function(done) {
        	webhostingBandwidthLimitCalls = 0;
             mapi.get( "/vts/exceeded",  function(err, result, httpResponse){
             	// console.log("result", err, result);
                assert.deepEqual(result,  []);

                assert.equal(webhostingBandwidthLimitCalls, 1);

                done()
             });
        });
   }

   function shouldBeEmpty(){

        it('should be empty', function(done) {
             mapi.post( "/vts/"+storage_id+"/search", {}, function(err, result, httpResponse){
             	// console.log("result", err, result);
                assert.deepEqual(result,  []);

                done()
             });
        });

   }


})
