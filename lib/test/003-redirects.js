require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../docroot-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){


  Array("redirects", "frames").forEach(mainCategory=>{

    describe(mainCategory, function(){
        "use strict"
        
        it('list redirects configured', function(done) {

             mapi.get( "/"+mainCategory+"/", function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code 

                assert.deepEqual(result, []);

                done()  
             })
        })

        it('adding a redirect', function(done) {

             mapi.put( "/"+mainCategory+"/example.hu", {"host":"www.example.hu", "redirect": "http://index.hu/"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()  
             })
        })

        it('adding a redirect without a redirect', function(done) {

             mapi.put( "/"+mainCategory+"/example.hu", {"host":"www.example.hu"}, function(err, result, httpResponse){
                assert.isNotNull(err)
                assert.equal(err.message, "VALIDATION_ERROR");

                done()  
             })
        })

        it('list redirects configured', function(done) {

             mapi.get( "/"+mainCategory+"/", function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code 

                assert.deepEqual(result, ["example.hu"]);

                done()  
             })
        })

        it('get configured redirects for a non-existing domain', function(done) {

             mapi.get( "/"+mainCategory+"/example.xxx", function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code 

                assert.deepEqual(result, []);

                done()  
             })
        })


        Array(0,1).forEach(i=>{

            if(i == 1)
                it('rebuilding', function(done) {

                     mapi.post( "/cleanup/"+mainCategory+"/rebuild", {}, function(err, result, httpResponse){
                        // we dont use assert.internal_error here since we don't have a response code 

                        assert.equal(result, "ok");

                        done()  
                     })
                })

            it('get configured redirects for a domain', function(done) {

                 mapi.get( "/"+mainCategory+"/example.hu", function(err, result, httpResponse){
                    // we dont use assert.internal_error here since we don't have a response code 

                    assert.deepEqual(result, [{"host": "www.example.hu","redirect": "http://index.hu/"}]);

                    done()  
                 })
            })

            it('get raw redirects for a domain', function(done) {

                 mapi.get( "/"+mainCategory+"/raw/example.hu", function(err, result, httpResponse){
                    // we dont use assert.internal_error here since we don't have a response code 
                    assert.isOk(-1 < result.indexOf("www.example.hu http://index.hu$uri;"));

                    done()  
                 })
            })

        })

        it('modifying a redirect', function(done) {

             mapi.put( "/"+mainCategory+"/example.hu", {"host":"www.example.hu", "redirect": "http://origo.hu/"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()  
             })
        })

        it('get raw redirects for a domain', function(done) {

             mapi.get( "/"+mainCategory+"/raw/example.hu", function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code 
                assert.isOk(-1 < result.indexOf("www.example.hu http://origo.hu$uri;"));

                done()  
             })
        })

        it('deleting a redirect', function(done) {

             mapi.delete( "/"+mainCategory+"/example.hu/www.example.hu", {}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()  
             })
        })

        it('should be empty again', function(done) {

             mapi.get( "/"+mainCategory+"/example.hu", function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code 

                assert.deepEqual(result, []);

                done()  
             })
        })


        it('adding a redirect for femforgacs.hu domain', function(done) {

             mapi.put( "/"+mainCategory+"/femforgacs.hu", {"host":"www.sub.femforgacs.hu", "redirect": "http://index.hu/"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()  
             })
        })

        it('adding a https redirect', function(done) {

             mapi.put( "/"+mainCategory+"/femforgacs.hu", {"host":"https.femforgacs.hu", "redirect": "https://otpbank.hu/"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()  
             })
        })

        it('adding a redirect for sub.femforgacs.hu domain (should be rejected since it is configured in femforgacs.hu already)', function(done) {

             mapi.put( "/"+mainCategory+"/sub.femforgacs.hu", {"host":"www.sub.femforgacs.hu", "redirect": "http://index.hu/"}, function(err, result, httpResponse){
                assert.equal(err.message, "HOST_ALREADY_CONFIGURED");

                done()  
             })
        })

        for(let q of ["sdfemforgacs.hu", ".femforgacs.hu", "femforgacs.sdf.hu"]) {
            it('attempting to add an invalid host (not belonging to the domain): '+q, function(done) {

                 mapi.put( "/"+mainCategory+"/femforgacs.hu", {"host":q, "redirect": "http://index.hu/"}, function(err, result, httpResponse){
                    assert.isNotNull(err);
                    assert.equal(err.message, "INVALID_HOST");

                    done()  
                 })
            })

        }

        for(let q of ["ftp://index.hu/", "ldap://index.hu/"]) {
            it('attempting to add an invalid redirect: '+q, function(done) {

                 mapi.put( "/"+mainCategory+"/femforgacs.hu", {"host":"invalid.femforgacs.hu", "redirect": q}, function(err, result, httpResponse){
                    assert.isNotNull(err);
                    assert.equal(err.message, "VALIDATION_ERROR");

                    done()  
                 })
            })

        }


        it('adding a redirect as a host', function(done) {

             mapi.put( "/"+mainCategory+"/hostname", {"host":"www.hostname", "redirect": "http://index.hu/"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()  
             })
        })

        it('adding the same top level host as entry', function(done) {

             mapi.put( "/"+mainCategory+"/something.hu", {"host":"something.hu", "redirect": "http://index.hu/"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()  
             })
        })



        it('adding a redirect with underscore', function(done) {

             mapi.put( "/"+mainCategory+"/example.hu", {"host":"www.example.hu", "redirect": "http://chronomeeting.blog.hu/2017/03/20/ii_chronomeeting_kiallitas_es_vasar"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()  
             })
        })


    })


  })



})
