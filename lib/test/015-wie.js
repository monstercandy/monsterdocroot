require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../docroot-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){
    "use strict"

    const MError = require('MonsterExpress').Error
    const path = require("path")

    var storage_id = 23001
    var Common = require("./000-common.js")(assert)

    var moment = require("MonsterMoment");

    var mainDomain = "wie.hu";

    var lastFullBackup;
    var template_awstats_allowed = { t_awstats_allowed: true }

    const expectedCompleteBackup = {
        webstore: {
           webapp: {
              type:"fastcgi", container: "foobar1", unix_socket_path: "/var/run/mc-app-sockets/11001-1/socket"
           }
        },
        auth: [
          {
            "username": "wie.hu"
          }
        ],
        frames: {
          "wie.hu": [
            {
              "host": "frames.wie.hu",
              "redirect": "http://frames.hu/",
            }
          ]
        },
        redirects: {
          "wie.hu": [
            {
              "host": "redirects.wie.hu",
              "redirect": "http://redirects.hu/",
            }
          ]
        },
        domains:   {
            'wie.hu': {
                 webapp: {
                    type:"default"
                 },
                entries: [ { host: 'www.wie.hu', docroot: '/wie.hu/pages/' } ],
                'protected-dirs': [
                    "/protected/"
                ],
                certificate_id: "certid",
                awstats_allowed: false
            }
        },
        'hooks-apache': {
        },
        'hooks-nginx': {
          "wie.hu": {
            "append": "append hook"
          }
        }
    };

    describe("preparation", function(){

        it('adding a docroot', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id, template_awstats_allowed);

             mapi.put( "/docroots/"+storage_id+"/"+mainDomain+"/entries", {"host": "www."+mainDomain, "docroot": "/"+mainDomain+"/pages"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('changing the default webapp', function(done) {

             mapi.post( "/docroots/webapps/"+storage_id, {type: "fastcgi", container: "foobar1", unix_socket_path: "/var/run/mc-app-sockets/11001-1/socket"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        Array("redirects", "frames").forEach(mainCategory=>{

            it('adding a redirect: '+mainCategory, function(done) {

                 mapi.put( "/"+mainCategory+"/"+mainDomain, {"host":mainCategory+"."+mainDomain, "redirect": "http://"+mainCategory+".hu/"}, function(err, result, httpResponse){
                    assert.equal(result, "ok");

                    done()
                 })
            })

        })

        it('add protected dir first should be ok', function(done) {

             mapi.put( "/protected-dirs/"+storage_id+"/"+mainDomain, {dir: "/protected"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('create an account', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id)

             mapi.put( "/auths/"+storage_id, {"username":mainDomain,"password":"12345678"}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                done()
             })
        })

        it('and also a hooker', function(done) {

            mapi.post( "/nginx/"+storage_id+"/"+mainDomain+"/hooks", {append: "append hook"},function(err, result, httpResponse){
                assert.equal(result, "ok")

                done()
             })
        })

        it('and setting a certificate', function(done) {

            mapi.post( "/docroots/"+storage_id+"/"+mainDomain+"/certificate", {"certificate_id":  "certid"}, function(err, result, httpResponse){
                assert.isNull(err);
                assert.equal(result, "ok");

                done()
             })
        })

    })

    describe("backup", function(){

        backupTest();

    })


    describe("restore", function(){


        removeAll();


        Array("redirects", "frames").forEach(mainCategory=>{

            it('removing a redirect: '+mainCategory, function(done) {

                 mapi.delete( "/"+mainCategory+"/"+mainDomain, {}, function(err, result, httpResponse){
                    assert.equal(result, "ok");

                    done()
                 })
            })

        })

        restoreTest();

        backupTest();

        // second restore should be fine
        restoreTest();

        // and should still reflect the same
        backupTest();


        it('querying docroots mapped by certificate', function(done) {

            mapi.get( "/docroots/certificates/"+storage_id, function(err, result, httpResponse){

                // console.log(result)

                assert.deepEqual(result, { 'certid': [ 'wie.hu' ] });
                done()
             })
        })


    })

    describe("complete backup test", function(){

        it('get a backup of all webstorages', function(done) {

             // we just return a dummy object for any lookups
             // app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(0);

             mapi.get( "/wie/", function(err, result, httpResponse){

                // console.log(result);

                var expected = {};
                expected[storage_id] = expectedCompleteBackup;
                delete result[storage_id].auth[0].password;

                assert.deepEqual(result, expected);

                done()
             })
        })


        // and for hygiene
        removeAll();
    })

    function restoreTest(){
        it('restore a backup for this storage', function(done) {

             mapi.post( "/wie/"+storage_id, lastFullBackup,  function(err, result, httpResponse){
                // console.log(result);

                assert.equal(result, "ok");

                done()
             })
        })
    }

    function removeAll(){
        it("removing the complete storage", function(done){
             mapi.delete( "/cleanup/webhostings/"+storage_id, {}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })

        })
    }

    function backupTest(){

        it('get a backup for this storage', function(done) {

             mapi.get( "/wie/"+storage_id, function(err, result, httpResponse){

                // console.log(result);

                lastFullBackup = simpleCloneObject(result);

                assert.ok(result.auth[0].password.startsWith("{SSHA}"));
                delete result.auth[0].password;

                assert.deepEqual(result, expectedCompleteBackup);

                done()
             })
        })

    }


})
