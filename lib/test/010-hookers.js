require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../docroot-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){
    "use strict"

    const MError = require('MonsterExpress').Error
    const path = require("path")

    var storage_id = 20001
    var Common = require("./000-common.js")(assert)

    const expectedContent = "some stuff";


    describe("setup", function(){

        const fs = require("MonsterDotq").fs()

        it('create a hooker append for apache, nginx', function() {

            var suffix = storage_id+"-hookers.hu-append.conf"
            var pathes = {}
            var ps = []
            Array("nginx","apache").forEach(c=>{
                pathes[c] = path.join(app.config.get(c+"_hooks_directory"), suffix)

                ps.push(fs.writeFileAsync(pathes[c], expectedContent+ " "+ c))
            })

            // console.log(p)

            return Promise.all(ps)

        })

        it('add a docroot', function(done) {


             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id)

             mapi.put( "/docroots/"+storage_id+"/hookers.hu/entries", {"host": "www.hookers.hu", "docroot": "/hookers.hu/pages"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })



    })



    describe("test", function(){



        it('and should contain the appender in apache', function(done) {

            mapi.get( "/apache/"+storage_id+"/hookers.hu/raw", function(err, result, httpResponse){
                assert.isNull(err);
                assert.isNotNull(result);
                assert.ok(result.indexOf('Include "./data/hooks.d-apache/20001-hookers.hu-append.conf"') > -1);

                done()
             })
        })


        it('and should contain the appender in nginx', function(done) {

            mapi.get( "/nginx/"+storage_id+"/hookers.hu/raw", function(err, result, httpResponse){
                assert.isNull(err);
                assert.isNotNull(result);
                assert.ok(result.indexOf('include "./data/hooks.d-nginx/20001-hookers.hu-append.conf";') > -1);

                done()
             })
        })

        Array("apache","nginx").forEach(s=>{
            it('and their content should be available through the api: '+s, function(done) {

                mapi.get( "/"+s+"/"+storage_id+"/hookers.hu/hooks", function(err, result, httpResponse){
                    assert.deepEqual(result, {append: expectedContent+" "+s})

                    done()
                 })
            })

        })


        Array("apache","nginx").forEach(s=>{
            it('and should be possible to change them via the api: '+s, function(done) {

                mapi.post( "/"+s+"/"+storage_id+"/hookers.hu/hooks", {base: "base "+s, append: "append "+s},function(err, result, httpResponse){
                    assert.equal(result, "ok")

                    done()
                 })
            })

        })

        Array("apache","nginx").forEach(s=>{
            it('and their content (append and base) should be available through the api: '+s, function(done) {

                mapi.get( "/"+s+"/"+storage_id+"/hookers.hu/hooks", function(err, result, httpResponse){
                    assert.deepEqual(result, {append: "append "+s, base: "base "+s})

                    done()
                 })
            })

        })

        Array("apache","nginx").forEach(s=>{
            it('and should be possible to delete them via the api as well: '+s, function(done) {

                mapi.post( "/"+s+"/"+storage_id+"/hookers.hu/hooks",{base: ""}, function(err, result, httpResponse){
                    assert.equal(result, "ok")

                    done()
                 })
            })

        })

        Array("apache","nginx").forEach(s=>{
            it('and their content (append only) should be available through the api: '+s, function(done) {

                mapi.get( "/"+s+"/"+storage_id+"/hookers.hu/hooks", function(err, result, httpResponse){
                    assert.deepEqual(result, {append: "append "+s})

                    done()
                 })
            })

        })


    })



})
