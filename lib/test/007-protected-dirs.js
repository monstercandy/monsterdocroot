require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../docroot-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const MError = require('MonsterExpress').Error

    var storage_id = 13001
    var domain = "protdir.hu"

    var Common = require("./000-common.js")(assert)

    var directory = "/path///somewhere"

    var uri = "/protected-dirs/"+storage_id+"/"+domain

    describe("prepare", function(){

        it('create one', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id)

             mapi.put( "/docroots/"+storage_id+"/"+domain+"/entries", {"host": domain, "docroot": "/docroot/something.hu"}, function(err, result, httpResponse){
                assert.equal(result, "ok");


                done()  
             })
        })

    })

    describe("test", function(){

        it('list protected dirs', function(done) {

             mapi.get( uri, function(err, result, httpResponse){
                assert.deepEqual(result, [] );

                done()  
             })
        })

        for(var i = 0; i < 3; i++) {
            it('add protected dir first should be ok', function(done) {

                 mapi.put( uri, {dir: directory}, function(err, result, httpResponse){
                    assert.equal(result, "ok");

                    done()  
                 })
            })

            if(i==2) break;

            it('and should be an exception at second', function(done) {

                 mapi.put( uri, {dir: directory}, function(err, result, httpResponse){
                    assert.propertyVal(err, "message", "ALREADY_PRESENT");

                    done()  
                 })
            })

            it('deleting protected dir first should be ok', function(done) {

                 mapi.delete( uri, {dir: directory}, function(err, result, httpResponse){
                    assert.equal(result, "ok");

                    done()  
                 })
            })

            it('and should be an exception at second', function(done) {

                 mapi.delete( uri, {dir: directory}, function(err, result, httpResponse){
                    assert.propertyVal(err, "message", "PROTECTED_DIR_NOT_FOUND");

                    done()  
                 })
            })
        }


        it('list protected dirs', function(done) {

             mapi.get( uri, function(err, result, httpResponse){
                assert.deepEqual(result, ["/path/somewhere/"] ); // path should have been normalized!

                done()  
             })
        })

        

        it('and should contain the relevant lines in nginx conf', function(done) {

            mapi.get( "/nginx/"+storage_id+"/"+domain+"/raw", function(err, result, httpResponse){
                console.log(result)
                assert.isNull(err);
                assert.isNotNull(result);
                assert.ok(result.indexOf('location ^~ "/path/somewhere/" {') > -1);
                assert.ok(result.indexOf('include common/target-host-legacy.conf;') > -1);

                done()
             })
        })

        it('protect the root of the domain', function(done) {

             mapi.put( uri, {dir: "/"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()  
             })
        })



        it('and should contain the relevant lines in nginx conf (without the generic vhost-default-proxy inclusion!)', function(done) {

            mapi.get( "/nginx/"+storage_id+"/"+domain+"/raw", function(err, result, httpResponse){
                assert.isNull(err);
                assert.isNotNull(result);
                assert.ok(result.indexOf('location ^~ "/path/somewhere/" {') > -1);
                assert.ok(result.indexOf('include common/vhost-default-proxy.conf;') <= -1); // a

                done()
             })
        })

    })

})
