require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../docroot-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const oMonsterInfoWebhosting = app.MonsterInfoWebhosting

    const MError = require('MonsterExpress').Error
    var storage_id = 12345

    var Common = require("./000-common.js")(assert)


    const simpleMonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id)


	describe('storages with authentication configured', function() {
	    it('list all storages with accounts', function(done) {

             mapi.get( "/auths", function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code 

                assert.deepEqual(result, []);

                done()  
             })
        })



        // we mock this one only here, since the cached version should be used by the subsequent/upcoming calls
        it('create an account', function(done) {

             app.MonsterInfoWebhosting = simpleMonsterInfoWebhosting

             mapi.put( "/auths/"+storage_id, {"username":"admin","password":"12345678"}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                done()  
             })           
        })     

        it('create the same again (should fail)', function(done) {

             mapi.put( "/auths/"+storage_id, {"username":"admin","password":"12345678"}, function(err, result, httpResponse){

                assert.equal(err.message, "ACCOUNT_ALREADY_EXISTS")

                done()  
             })           
        })     

        it('deleting it', function(done) {
             mapi.delete( "/auths/"+storage_id+"/admin", {}, function(err, result, httpResponse){

                assert.equal(result, "ok")

                done()  
             })           
        })     

        it('deleting it again (should fail)', function(done) {
             mapi.delete( "/auths/"+storage_id+"/admin", {}, function(err, result, httpResponse){

                assert.equal(err.message, "ACCOUNT_NOT_EXISTS")

                done()  

             })           
        })  

        it('create an account (should work again)', function(done) {
             mapi.put( "/auths/"+storage_id, {"username":"admin","password":"12345678"}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                done()  
             })           
        })     
        

       it('list accounts of a storage', function(done) {
             mapi.get( "/auths/"+storage_id, function(err, result, httpResponse){

                assert.deepEqual(result, [{"username":"admin"}]);

                done()  
             })

            
        })        



        it('list storages again', function(done) {

             mapi.get( "/auths", function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code 

                assert.deepEqual(result, [""+storage_id]);

                done()  
             })
        })


    })

})
