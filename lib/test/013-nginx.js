require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

const assert = require("ExpressTester").assert

describe("string escaping", function(){
	var lib = require("../lib-nginx.js");

	it("new lines and quotation marks should be escaped", function(){
        var actual = lib.escapeStringForConfigFile("\n\"\n\"");
        assert.equal(actual, "\\n\\\"\\n\\\"");
	})
})