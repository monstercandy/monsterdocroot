require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

const containsLib = require("lib-contains-already.js")
const assert = require("ExpressTester").assert

describe('contains lib', function() {

  const CONTAINS_DIR1 = "test/contains-test1"
  const CONTAINS_DIR2 = "test/contains-test2"
  const CONTAINS_DIR3 = "test/contains-test3"
  const CONTAINS_DIR4 = "test/contains-test4"

  it("filtering correctly", function(){

      this.timeout(10000)

      return containsLib.FilterFiles(CONTAINS_DIR1, "sub.femforgacs.hu")
        .then(d=>{
            assert.deepEqual(d, [CONTAINS_DIR1+"/2/femforgacs.hu.conf"]) // we dont examine the target domain "test/contains-test/1/sub.femforgacs.hu.conf", 
        })
  })

	    it('filtering dir 1 (should reject)', function() {

             return containsLib(CONTAINS_DIR1, "sub.femforgacs.hu", "xxx.sub.femforgacs.hu")
               .then(()=>{
                  throw new Error("should have been rejected")
               })
               .catch(ex=>{
                   assert.equal(ex.message, "HOST_ALREADY_CONFIGURED")
               })

        })


        it('filtering dir 2 (should reject)', function() {

             return containsLib(CONTAINS_DIR1, "femforgacs.hu", "www.sub.femforgacs.hu")
               .then(()=>{
                  throw new Error("should have been rejected")
               })
               .catch(ex=>{
                   assert.equal(ex.message, "HOST_ALREADY_CONFIGURED")
               })
        })


        it('filtering (should be ok)', function() {

             return containsLib(CONTAINS_DIR1, "femforgacs.hu", "anythingelse.sub.femforgacs.hu")
        })


        it('filtering dir 2 (should reject)', function() {

             return containsLib(CONTAINS_DIR2, "femforgacs.hu", "webmail.femforgacs.hu")
               .then(()=>{
                  throw new Error("should have been rejected")
               })
               .catch(ex=>{
                   assert.equal(ex.message, "HOST_ALREADY_CONFIGURED")
               })
        })

        it('filtering dir 3 (should reject)', function() {

             return containsLib(CONTAINS_DIR3, "sub1.example.hu", "sub1.example.hu")
               .then(()=>{
                  throw new Error("should have been rejected")
               })
               .catch(ex=>{
                   assert.equal(ex.message, "HOST_ALREADY_CONFIGURED")
               })
        })

        it('filtering dir 4 (should be ok due to suspended status)', function() {

             return containsLib(CONTAINS_DIR4, "sub1.example.hu", "sub1.example.hu")
        })
})
