require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

const syslogWafLib = require("lib-syslogd-waf.js")
const assert = require("ExpressTester").assert

describe('syslog parsing', function() {



  it("a dummy example", function(){

      var r = syslogWafLib.ParsePostDataLine('11005 POST php-72.test.monstermedia.hu 192.168.0.104 "curl/7.55.1" / foobar=1 200')

      assert.deepEqual(r, { webstoreId: '11005',
  httpMethod: 'POST',
  hostname: 'php-72.test.monstermedia.hu',
  remoteAddr: '192.168.0.104',
  userAgent: 'curl/7.55.1',
  queryString: '/',
  postData: 'foobar=1',
  httpResponseCode: 200 })
  })


  it("a real life example", function(){

      var r = syslogWafLib.ParsePostDataLine('11015 POST catalase.hu 136.243.79.106 "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36" //xmlrpc.php <?xml version=\x221.0\x22?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data>\\x0D\\x0A<value><struct><member><name>methodName</name><value><string>wp.getUsersBlogs</string></value></member><member><name>params</name><value><array><data><value><array><data><value><string>electron</string></value><value><string>qaz!@#</string></value></data></array></value></data></array></value></member></struct></value>\\x0D\\x0A</data></array></value></param></params></methodCall>\\x0D\\x0A 200')

      assert.deepEqual(r, { webstoreId: '11015',
  httpMethod: 'POST',
  hostname: 'catalase.hu',
  remoteAddr: '136.243.79.106',
  userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
  queryString: '//xmlrpc.php',
  postData: '<?xml version="1.0"?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data>\\x0D\\x0A<value><struct><member><name>methodName</name><value><string>wp.getUsersBlogs</string></value></member><member><name>params</name><value><array><data><value><array><data><value><string>electron</string></value><value><string>qaz!@#</string></value></data></array></value></data></array></value></member></struct></value>\\x0D\\x0A</data></array></value></param></params></methodCall>\\x0D\\x0A',
  httpResponseCode: 200 })
  })

  it("one more reallife example", function(){

      var r = syslogWafLib.ParsePostDataLine('11012 POST dotproject.eurofarm.hu 80.99.178.158 "curl/7.55.1" /index.php? username=foobar&password=12354 200')

      assert.deepEqual(r, { webstoreId: '11012',
  httpMethod: 'POST',
  hostname: 'dotproject.eurofarm.hu',
  remoteAddr: '80.99.178.158',
  userAgent: 'curl/7.55.1',
  queryString: '/index.php?',
  postData: 'username=foobar&password=12354',
  httpResponseCode: 200 })
  })
  

  Array({n:"JSimplepieFactory foo eval", v: true},{n:"JSimplepieFactory", v: false}).forEach(d=>{
    it("forbidden event test: "+d.n+": "+d.v, function(){

        assert.equal(d.v, syslogWafLib.IsForbiddenEvent({userAgent: d.n}));
    })
  })

  it("protected event test, joomla login attempt", function(){
     assert.equal(true, syslogWafLib.IsProtectedEvent({
        queryString: "/administrator/index.php",
        httpMethod: "POST",
        postData: "passwd=foobar",
     }))
  })

  it("protected event test, generic login attempt", function(){
     assert.equal(true, syslogWafLib.IsProtectedEvent({ webstoreId: '11012',
  httpMethod: 'POST',
  hostname: 'dotproject.eurofarm.hu',
  remoteAddr: '80.99.178.158',
  userAgent: 'curl/7.55.1',
  queryString: '/index.php?',
  postData: 'username=foobar&password=12354',
  httpResponseCode: 200 }))
  })

  it("protected event test, xmlrpc call", function(){
     assert.equal(true, syslogWafLib.IsProtectedEvent({
        queryString: "//xmlrpc.php",
        httpMethod: "POST",
        postData: "foo=bar",
     }))
  })

  it("protected event test, generic login event", function(){
     assert.equal(true, syslogWafLib.IsProtectedEvent({
        queryString: "/index.php?op=login",
        httpMethod: "POST",
        postData: "foo=bar",
     }))
  })

  it("protected event test, accountapi exception", function(){
     assert.equal(false, syslogWafLib.IsProtectedEvent({
        queryString: "/accountapi/loginlog",
        httpMethod: "POST",
        postData: "foo=bar",
     }))
  })

  it("some non-login event", function(){
     assert.equal(false, syslogWafLib.IsProtectedEvent({
        queryString: "/some.php?foobar=1",
        httpMethod: "POST",
        postData: "foo=bar",
     }))
  })

  it("filtering individual level", function(){
    var constraints = {
       syslog_waf_ban_individual_attempts: 2,
       syslog_waf_ban_storage_all_attempts: 3,
       syslog_waf_ban_storage_individual_attempts: 3,
    };
    var statistics = syslogWafLib.InitStatistics();
    var event = {
       remoteAddr: "123.123.123.123",
       webstoreId: "12345",
    };
    assert.equal(false, syslogWafLib.AccountProtectedRequest(event, statistics, constraints));
    assert.equal(true, syslogWafLib.AccountProtectedRequest(event, statistics, constraints));
    event.remoteAddr = "321.321.321.231";
    assert.equal(false, syslogWafLib.AccountProtectedRequest(event, statistics, constraints));
  })

  it("filtering webstore level", function(){
    var constraints = {
       syslog_waf_ban_individual_attempts: 3,
       syslog_waf_ban_storage_all_attempts: 3,
       syslog_waf_ban_storage_individual_attempts: 2,
    };
    var statistics = syslogWafLib.InitStatistics();
    var event = {
       webstoreId: "12345",
    };

    event.remoteAddr = "1";
    assert.equal(false, syslogWafLib.AccountProtectedRequest(event, statistics, constraints));

    event.remoteAddr = "2";
    assert.equal(false, syslogWafLib.AccountProtectedRequest(event, statistics, constraints));

    event.remoteAddr = "3";
    assert.equal(false, syslogWafLib.AccountProtectedRequest(event, statistics, constraints));

    // webstore limit reached, second attempt from an already abusing ip
    event.remoteAddr = "1";
    assert.equal(true, syslogWafLib.AccountProtectedRequest(event, statistics, constraints));

    // but new ips should still be ok
    event.remoteAddr = "4";
    assert.equal(false, syslogWafLib.AccountProtectedRequest(event, statistics, constraints));
  })


  it("cleanup support", function(){
    var constraints = {
       syslog_waf_ban_storage_window_sec: 2,
       syslog_waf_ban_individual_window_sec: 2,

       syslog_waf_ban_individual_attempts: 5,
       syslog_waf_ban_storage_all_attempts: 5,
       syslog_waf_ban_storage_individual_attempts: 5,
    };
    var statistics = syslogWafLib.InitStatistics();
    var event1 = {
       remoteAddr: "1",
       webstoreId: "12345",
    };
    var event2 = {
       remoteAddr: "2",
       webstoreId: "12345",
    };

    assert.equal(false, syslogWafLib.AccountProtectedRequest(event1, statistics, constraints, 100));
    assert.equal(false, syslogWafLib.AccountProtectedRequest(event1, statistics, constraints, 100));
    assert.equal(false, syslogWafLib.AccountProtectedRequest(event1, statistics, constraints, 101));
    assert.equal(false, syslogWafLib.AccountProtectedRequest(event2, statistics, constraints, 101));
    syslogWafLib.CleanStatistics(statistics, constraints, 102);
    assert.deepEqual(statistics, { 
       messagesProcessed: 0,
       perIp: { '1': [ 101 ], '2': [ 101 ] },
       perWebstore: { '12345': [ 101, 101 ] } 
    })

  })

})
