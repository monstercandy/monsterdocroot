require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../docroot-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){
    "use strict"

    const MError = require('MonsterExpress').Error

    var storage_id1 = 12001
    var storage_id2 = 12002
    var storage_id3 = 12003
    var storage_id4 = 12004


    var origMonsterInfoWebhosting = app.MonsterInfoWebhosting


    var Common = require("./000-common.js")(assert)


    describe("prepare", function(){
        "use strict"


        for (let q of
            [
              {"storage": storage_id1,"domain":"domain-nginx-only.hu", "host": "www.domain-nginx-only.hu", "docroot":"/docroot/pages"},
              {"storage": storage_id2,"domain":"domain-apache-only.hu", "host": "www.domain-apache-only.hu", "docroot":"/docroot/pages"},
              {"storage": storage_id3,"domain":"domain.hu", "host": "www.domain.hu", "docroot":"/docroot/pages"},
              {"storage": storage_id4,"domain":"docroot-should-be-removed-due-to-missing-webhosting.hu", "host": "www.docroot-should-be-removed-due-to-missing-webhosting.hu", "docroot":"/docroot/pages"},
            ])
        it('add a docroot: '+q.host, function(done) {


             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(q.storage)

             mapi.put( "/docroots/"+q.storage+"/"+q.domain+"/entries", {"host": q.host, "docroot": q.docroot}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it("chaos", function(){
            const fs = require("MonsterDotq").fs()
            const path = require("path")

            return fs.unlinkAsync(path.join(app.config.get("apache_vhosts_directory"), ""+storage_id1, "domain-nginx-only.hu.conf"))
                .then(()=>{
                    return fs.unlinkAsync(path.join(app.config.get("nginx_vhosts_directory"), ""+storage_id2, "domain-apache-only.hu.conf"))
                })
                .then(()=>{
                    return fs.unlinkAsync(path.join(app.config.get("docroot_directory"), ""+storage_id3, "domain.hu.conf"))
                })
                .then(()=>{
                    // in this one we aim to test the case when webhosting does not exists anymore, which should be detected and relevant docroots should be removed
                    return origMonsterInfoWebhosting.Delete(storage_id4)
                })

        })
    })

    describe("testing whether cleanup really recovers from inconsistent state", function(){

        it('maintane', function(done) {

             var hosting_calls = 0
             var r = new RegExp("^([0-9]+)$")

              app.MonsterInfoWebhosting = {
                  GetInfo: function(wh_id) {

                      var m = r.exec(""+wh_id)
                      if(!m) throw new MError("UNEXPECTED_INPUT")

                      hosting_calls++

                      if(m[1] == storage_id4) return Promise.reject(new MError("HOSTING_NOT_FOUND"))

                      return Promise.resolve({"wh_id": m[1], "extras":{"web_path": "/web/w3/"+m[1]+"-12345678901234567890123456"}})

                  }
              }

             mapi.post( "/cleanup/maintane", {}, function(err, result, httpResponse){
                assert.deepEqual(result, { awstats: {created:[], removed:[], flagged: []}, docroot: { removed: ["12004"] }, webserver: { removed: ["12003/domain.hu@apache","12003/domain.hu@nginx"], created: ["12001/domain-nginx-only.hu@apache","12002/domain-apache-only.hu@nginx"] } });

                assert.equal(hosting_calls, 9)

                done()
             })
        })

    })


    describe("testing rebuilding docroots of a webhosting", function(){


        it('setting root dir for expired domains', function(done) {


             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo()

             mapi.post( "/docroots/"+storage_id2+"/domain-apache-only.hu/expiredroot", {"expired-root": true}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                done()
             })
        })

        it('rebuilding a webhosting because of expiration', function(done) {

             var hosting_calls = 0
             var r = new RegExp("^([0-9]+)$")


              app.MonsterInfoWebhosting = {
                  GetInfo: function(wh_id) {

                    var m = r.exec(""+wh_id)
                    if(!m) throw new MError("UNEXPECTED_URI")

                    hosting_calls++

                    // expired root docroot is on storage_id2
                    if((m[1] != storage_id1)&&(m[1] != storage_id2)) return Promise.reject(new MError("HOSTING_NOT_FOUND"))

                    return Promise.resolve({"wh_id": m[1], "wh_is_expired": true, "extras":{"web_path": "/web/w3/"+m[1]+"-12345678901234567890123456"}})

                  }
              }


             mapi.post( "/cleanup/webhosting/"+storage_id1+"/rebuild", {}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                assert.equal(hosting_calls, 2)

                done()
             })
        })


        it('rebuilding a webhosting for which no docroot dir exists should return NOT_FOUND exception (instead of INTERNAL_ERROR)', function(done) {

             const invalid_webhosting_id = "99999"

             var hosting_calls = 0
             var r = new RegExp("^([0-9]+)$")


             var oMonsterInfoWebhosting = app.MonsterInfoWebhosting
              app.MonsterInfoWebhosting = {
                  GetInfo: function(wh_id) {

                    hosting_calls++

                    console.log("monsterinfowebhosting", wh_id)

                    // expired root docroot is on storage_id2
                    if(wh_id != invalid_webhosting_id) return Promise.reject(new MError("HOSTING_NOT_FOUND"))

                    return Promise.resolve({"wh_id": wh_id, "wh_is_expired": true, "extras":{"web_path": "/web/w3/"+wh_id+"-12345678901234567890123456"}})

                  }
              }


             mapi.post( "/cleanup/webhosting/"+invalid_webhosting_id+"/rebuild", {}, function(err, result, httpResponse){
                assert.propertyVal(err, "message", "DOCROOT_NOT_FOUND");

                assert.equal(hosting_calls, 1)

                app.MonsterInfoWebhosting = oMonsterInfoWebhosting

                done()
             })
        })


        new Array("apache", "nginx").forEach(p=>{
            it('docroot in raw data should reflect the expired root directory in: '+p, function(done) {

                mapi.get( "/"+p+"/"+storage_id1+"/domain-nginx-only.hu/raw", function(err, result, httpResponse){
                    assert.isNull(err);
                    assert.isNotNull(result);
                    assert.ok(result.match(/"\/web\/w3\/12002-12345678901234567890123456\/docroot\/pages\/"/));

                    done()
                 })
            })

        })

    })

})
