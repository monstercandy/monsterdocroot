require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../docroot-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){
    "use strict"

    const MError = require('MonsterExpress').Error

    var storage_id = 18001
    var Common = require("./000-common.js")(assert)




    describe("setup", function(){


        it('add a docroot', function(done) {


             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id)

             mapi.put( "/docroots/"+storage_id+"/circle1.hu/entries", {"host": "www.circle1.hu", "docroot": "/circle.hu/pages"}, function(err, result, httpResponse){
                assert.equal(result, "ok");


                done()  
             })
        })

        it('and one more', function(done) {
             mapi.put( "/docroots/"+storage_id+"/circle2.hu/entries", {"host": "www.circle2.hu", "docroot": "/circle.hu/pages"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()  
             })
        })
    })




    describe("primary", function(){


        it('set circle1 as primary', function(done) {

             mapi.post( "/docroots/"+storage_id+"/circle1.hu/primary", {"primary": true}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                done()  
             })
        })

        it('get settings along with entries', function(done) {

             mapi.get( "/docroots/"+storage_id+"/circle1.hu", function(err, result, httpResponse){
                assert.propertyVal(result, "primary", true)

                done()  
             })
        })



        it('set circle2 as primary', function(done) {

             mapi.post( "/docroots/"+storage_id+"/circle2.hu/primary", {"primary": true}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                done()  
             })
        })

        it('circle1 should not be primary anymore', function(done) {

             mapi.get( "/docroots/"+storage_id+"/circle1.hu", function(err, result, httpResponse){
                assert.notProperty(result, "primary")

                done()  
             })
        })

        it('circle2 should be primary', function(done) {

             mapi.get( "/docroots/"+storage_id+"/circle2.hu", function(err, result, httpResponse){
                assert.propertyVal(result, "primary", true)

                done()  
             })
        })


        it('raw data should look normal for apache', function(done) {

            mapi.get( "/apache/"+storage_id+"/circle2.hu/raw", function(err, result, httpResponse){
                assert.isNull(err);
                assert.isNotNull(result);
                assert.ok(result.indexOf("ServerName www.circle2.hu") > -1);

                done()
             })
        })


        it('and should contain default in nginx', function(done) {

            mapi.get( "/nginx/"+storage_id+"/circle2.hu/raw", function(err, result, httpResponse){
                assert.isNull(err);
                assert.isNotNull(result);
                assert.ok(result.indexOf("listen-default.conf") > -1);
                assert.ok(result.indexOf("if ($redirect_http_host) {") > -1);
                assert.ok(result.indexOf("rewrite ^ $redirect_http_host redirect;") > -1)

                done()
             })
        })

        it('nginx specific parts should not be present in old primary anymore', function(done) {

            mapi.get( "/nginx/"+storage_id+"/circle1.hu/raw", function(err, result, httpResponse){
                assert.isNull(err);
                assert.isNotNull(result);
                assert.ok(result.indexOf("listen-default.conf") <= -1);
                assert.ok(result.indexOf("if ($redirect_http_host) {") <= -1);
                assert.ok(result.indexOf("rewrite ^ $redirect_http_host redirect;") <= -1)

                done()
             })
        })

    })



})
