require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../docroot-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){
    "use strict"

    const MError = require('MonsterExpress').Error

    var storage_id = 10016

    var Common = require("./000-common.js")(assert)

    assert.sequal = function(a,b){
        var sa = a.replace(/\s+/g, " ");
        var sb = b.replace(/\s+/g, " ");
        assert.equal(sa, sb);
    }


    describe("same vhost on multiple storages", function(){

        it('add a docroot first', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id, null, {wh_php_version: "7.0"})

             mapi.put( "/docroots/"+storage_id+"/somewebapp1.hu/entries", {"host": "www.somewebapp1.hu", "docroot": "/somewebapp1.hu/pages"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                mapi.put( "/docroots/"+storage_id+"/somewebapp2.hu/entries", {"host": "www.somewebapp2.hu", "docroot": "/somewebapp2.hu/pages"}, function(err, result, httpResponse){
                    assert.equal(result, "ok");

                    mapi.put( "/docroots/"+storage_id+"/somewebapp3.hu/entries", {"host": "www.somewebapp3.hu", "docroot": "/somewebapp3.hu/pages"}, function(err, result, httpResponse){
                        assert.equal(result, "ok");

                        mapi.put( "/docroots/"+storage_id+"/somewebapp4.hu/entries", {"host": "www.somewebapp4.hu", "docroot": "/somewebapp4.hu/pages"}, function(err, result, httpResponse){
                           assert.equal(result, "ok");

                            mapi.put( "/docroots/"+storage_id+"/somewebapp5.hu/entries", {"host": "www.somewebapp5.hu", "docroot": "/somewebapp5.hu/pages"}, function(err, result, httpResponse){
                               assert.equal(result, "ok");

                                mapi.put( "/docroots/"+storage_id+"/somewebapp6.hu/entries", {"host": "www.somewebapp6.hu", "docroot": "/somewebapp6.hu/pages"}, function(err, result, httpResponse){
                                    assert.equal(result, "ok");

                                    mapi.put( "/docroots/"+storage_id+"/somewebapp7.hu/entries", {"host": "www.somewebapp7.hu", "docroot": "/somewebapp7.hu/pages"}, function(err, result, httpResponse){
                                       assert.equal(result, "ok");

                                        mapi.put( "/docroots/"+storage_id+"/somewebapp8.hu/entries", {"host": "www.somewebapp8.hu", "docroot": "/somewebapp8.hu/pages"}, function(err, result, httpResponse){
                                           assert.equal(result, "ok");

                                           done()
                                        })
                                    })

                                })
                            })
                        })
                    })
                })
             })
        })

        it('list webapps', function(done) {

             mapi.get( "/docroots/webapps/"+storage_id, function(err, result, httpResponse){
                assert.deepEqual(result, {
                    default: {type:"host-legacy"}, 
                    vhosts: {
                        "somewebapp1.hu": {
                            type: "default"
                        },
                        "somewebapp2.hu": {
                            type: "default"
                        },
                        "somewebapp3.hu": {
                            type: "default"
                        },
                        "somewebapp4.hu": {
                            type: "default"
                        },
                        "somewebapp5.hu": {
                            type: "default"
                        },
                        "somewebapp6.hu": {
                            type: "default"
                        },
                        "somewebapp7.hu": {
                            type: "default"
                        },
                        "somewebapp8.hu": {
                            type: "default"
                        },
                    }
                });

                done()
             })
        })


        it('changing the default webapp', function(done) {

             mapi.post( "/docroots/webapps/"+storage_id, {type: "fastcgi", container: "foobar0", unix_socket_path: "/var/run/mc-app-sockets/11001-1/socket"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('changing the webapp of a vhost entry #1 (webserver-http)', function(done) {

             mapi.post( "/docroots/webapps/"+storage_id+"/somewebapp1.hu", {type: "webserver-http", container: "foobar1"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('changing the webapp of a vhost entry #2 (uwsgi)', function(done) {

             mapi.post( "/docroots/webapps/"+storage_id+"/somewebapp2.hu", {type: "uwsgi", container: "foobar2", unix_socket_path: "/var/run/mc-app-sockets/11001-2/socket"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('changing the webapp of a vhost entry #3 (php-fpm)', function(done) {

             mapi.post( "/docroots/webapps/"+storage_id+"/somewebapp3.hu", {type: "php-fpm", container: "foobar3", unix_socket_path: "/var/run/mc-app-sockets/11001-3/socket"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('changing the webapp of a vhost entry #4 (fastcgi)', function(done) {

             mapi.post( "/docroots/webapps/"+storage_id+"/somewebapp4.hu", {type: "fastcgi", container: "foobar4", unix_socket_path: "/var/run/mc-app-sockets/11001-4/socket"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('changing the webapp of a vhost entry #5 (host-legacy)', function(done) {

             mapi.post( "/docroots/webapps/"+storage_id+"/somewebapp5.hu", {type: "host-legacy"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('changing the webapp of a vhost entry #6 (none)', function(done) {

             mapi.post( "/docroots/webapps/"+storage_id+"/somewebapp6.hu", {type: "none"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('changing the webapp of a vhost entry #8 (webserver-unix)', function(done) {

             mapi.post( "/docroots/webapps/"+storage_id+"/somewebapp8.hu", {type: "webserver-unix", container: "foobar8", unix_socket_path: "/var/run/mc-app-sockets/11001-8/socket"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })


        it('list webapps', function(done) {

             mapi.get( "/docroots/webapps/"+storage_id, function(err, result, httpResponse){
                assert.deepEqual(result, {
                    default: {type:"fastcgi", container: "foobar0", unix_socket_path: "/var/run/mc-app-sockets/11001-1/socket"}, 
                    vhosts: {
                        "somewebapp1.hu": {
                            type: "webserver-http",
                            container: "foobar1",
                        },
                        "somewebapp2.hu": {
                            type: "uwsgi",
                            container: "foobar2",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-2/socket", 
                        },
                        "somewebapp3.hu": {
                            type: "php-fpm",
                            container: "foobar3",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-3/socket", 
                        },
                        "somewebapp4.hu": {
                            type: "fastcgi",
                            container: "foobar4",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-4/socket", 
                        },
                        "somewebapp5.hu": {
                            type: "host-legacy",
                        },
                        "somewebapp6.hu": {
                            type: "none"
                        },
                        "somewebapp7.hu": {
                            type: "default"
                        },
                        "somewebapp8.hu": {
                            type: "webserver-unix",
                            container: "foobar8",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-8/socket", 
                        }
                    }
                });

                done()
             })
        })

        it('checking nginx conf for vhost somewebapp6.hu (none)', function(done) {

// note: this webapplication (none) goes to apache, so we dont lookup index.html/index.htm here.
            mapi.get( "/nginx/"+storage_id+"/somewebapp6.hu/raw", function(err, result, httpResponse){
                 console.log(result);
                assert.sequal(result, `server {
    # webapp type: none

    include common/listen.conf;
    server_name www.somewebapp6.hu;
    set $mm_webhosting_id "10016";

    access_log "./data/nginx.access.log" main;
    error_log "./data/nginx.error.log" error;
    set $doc_root "/web/w3/10016-12345678901234567890123456/somewebapp6.hu/pages/";

    include common/vhost.conf;

    include common/target-host-legacy.conf;

    location ~* .(gif|jpg|jpeg|png|ico|wmv|3gp|avi|mpg|mpeg|mp4|flv|mp3|mid|js|css|html|htm|wml|txt|md|rb|scss|ttf|woff|woff2|eot|otf|json|zip|pdf|tar|odt|svg|doc|docx|ppt|pptx)\$ {
        root \$doc_root;
        try_files \$uri \$uri/ @target;
        expires 1d;
    }

    location /xmlrpc.php {
        deny all;
    }

    location / {

       if (\$frame_http_host) {
#         add_header Content-Type text/html;
         return 200 "<!DOCTYPE html><html><frameset cols='100%'><frame src='\$frame_http_host' frameborder='0'></frameset></html>";
       }


       return 450;

    }
    
}

`);
                done()
             })
        })

        it('checking apache conf for vhost somewebapp6.hu (none)', function(done) {

            mapi.get( "/apache/"+storage_id+"/somewebapp6.hu/raw", function(err, result, httpResponse){
                // console.log(result);
                assert.equal(result, `
<VirtualHost *>
  # webapp type: none
  DocumentRoot "/web/w3/10016-12345678901234567890123456/somewebapp6.hu/pages/"
  ServerName www.somewebapp6.hu
  
  ErrorLog /web/w3/10016-12345678901234567890123456/weblogs/somewebapp6.hu-apache-error.log
  RemoveHandler .php

  

</VirtualHost>

`)
                done()
             })
        })

        it('checking nginx conf for vhost somewebapp1.hu (webserver-http proxy)', function(done) {

// note: this webapplication does not go to apache, so we do lookup index.html/index.htm here.
            mapi.get( "/nginx/"+storage_id+"/somewebapp1.hu/raw", function(err, result, httpResponse){
                // console.log(result);
                assert.sequal(result, `server {
    # webapp type: webserver-http, container: foobar1

    include common/listen.conf;
    server_name www.somewebapp1.hu;
    set $mm_webhosting_id "10016";

    access_log "./data/nginx.access.log" main;
    error_log "./data/nginx.error.log" error;

    location @target {
        set $vhost $host;
        include common/params_proxy;
        proxy_pass http://foobar1:8080;
    }
    set $doc_root "/web/w3/10016-12345678901234567890123456/somewebapp1.hu/pages/";

    include common/vhost.conf;

    location ~* .(gif|jpg|jpeg|png|ico|wmv|3gp|avi|mpg|mpeg|mp4|flv|mp3|mid|js|css|html|htm|wml|txt|md|rb|scss|ttf|woff|woff2|eot|otf|json|zip|pdf|tar|odt|svg|doc|docx|ppt|pptx)\$ {
        root \$doc_root;
        try_files \$uri \$uri/ @target;
        expires 1d;
    }

    location /xmlrpc.php {
        deny all;
    }

    location / {

       if (\$frame_http_host) {
#         add_header Content-Type text/html;
         return 200 "<!DOCTYPE html><html><frameset cols='100%'><frame src='\$frame_http_host' frameborder='0'></frameset></html>";
       }

       root $doc_root;
       try_files $uri/index.html $uri/index.htm @target;

    }

}

`)
                done()
             })
        })

        it('checking apache conf for vhost somewebapp1.hu (webserver-http proxy)', function(done) {

            mapi.get( "/apache/"+storage_id+"/somewebapp1.hu/raw", function(err, result, httpResponse){
                // console.log(result);

                // no apache config should be generated!
                assert.isNull(err);
                assert.equal(result, "");
                done()
             })
        })

        it('checking nginx conf for vhost somewebapp2.hu (uwsgi)', function(done) {

// note: this webapplication does not go to apache, so we do lookup index.html/index.htm here.
            mapi.get( "/nginx/"+storage_id+"/somewebapp2.hu/raw", function(err, result, httpResponse){
                // console.log(result);
                assert.sequal(result, `server {
    # webapp type: uwsgi, container: foobar2

    include common/listen.conf;
    server_name www.somewebapp2.hu;
    set $mm_webhosting_id "10016";

    access_log "./data/nginx.access.log" main;
    error_log "./data/nginx.error.log" error;

    location @target {
        set $being_proxied 1;

        include common/params_uwsgi;
        uwsgi_pass unix:///var/run/mc-app-sockets/11001-2/socket;
    }
    set $doc_root "/web/w3/10016-12345678901234567890123456/somewebapp2.hu/pages/";

    include common/vhost.conf;

    location ~* .(gif|jpg|jpeg|png|ico|wmv|3gp|avi|mpg|mpeg|mp4|flv|mp3|mid|js|css|html|htm|wml|txt|md|rb|scss|ttf|woff|woff2|eot|otf|json|zip|pdf|tar|odt|svg|doc|docx|ppt|pptx)\$ {
        root \$doc_root;
        try_files \$uri \$uri/ @target;
        expires 1d;
    }

    location /xmlrpc.php {
        deny all;
    }

    location / {

       if (\$frame_http_host) {
#         add_header Content-Type text/html;
         return 200 "<!DOCTYPE html><html><frameset cols='100%'><frame src='\$frame_http_host' frameborder='0'></frameset></html>";
       }

       root $doc_root;
       try_files $uri/index.html $uri/index.htm @target;

    }


}

`)
                done()
             })
        })

        it('checking apache conf for vhost somewebapp2.hu (uwsgi)', function(done) {

            mapi.get( "/apache/"+storage_id+"/somewebapp2.hu/raw", function(err, result, httpResponse){
                // console.log(result);

                // no apache config should be generated!
                assert.isNull(err);
                assert.equal(result, "");
                done()
             })
        })

        it('checking nginx conf for vhost somewebapp3.hu (php-fpm)', function(done) {

// note: this webapplication (php-fpm) goes to apache, so we dont lookup index.html/index.htm here.
            mapi.get( "/nginx/"+storage_id+"/somewebapp3.hu/raw", function(err, result, httpResponse){
                assert.sequal(result, `server {
    # webapp type: php-fpm, container: foobar3

    include common/listen.conf;
    server_name www.somewebapp3.hu;
    set $mm_webhosting_id "10016";

    access_log "./data/nginx.access.log" main;
    error_log "./data/nginx.error.log" error;
    set $doc_root "/web/w3/10016-12345678901234567890123456/somewebapp3.hu/pages/";

    include common/vhost.conf;

    include common/target-host-legacy.conf;

    location ~* .(gif|jpg|jpeg|png|ico|wmv|3gp|avi|mpg|mpeg|mp4|flv|mp3|mid|js|css|html|htm|wml|txt|md|rb|scss|ttf|woff|woff2|eot|otf|json|zip|pdf|tar|odt|svg|doc|docx|ppt|pptx)\$ {
        root \$doc_root;
        try_files \$uri \$uri/ @target;
        expires 1d;
    }

    location /xmlrpc.php {
        deny all;
    }

    location / {

       if (\$frame_http_host) {
#         add_header Content-Type text/html;
         return 200 "<!DOCTYPE html><html><frameset cols='100%'><frame src='\$frame_http_host' frameborder='0'></frameset></html>";
       }


       return 450;

    }


}

`)
                done()
             })
        })

        it('checking apache conf for vhost somewebapp3.hu (php-fpm)', function(done) {

            mapi.get( "/apache/"+storage_id+"/somewebapp3.hu/raw", function(err, result, httpResponse){
                 console.log(result)
                assert.sequal(result, `
<VirtualHost *>
  # webapp type: php-fpm, container: foobar3
  DocumentRoot "/web/w3/10016-12345678901234567890123456/somewebapp3.hu/pages/"
  ServerName www.somewebapp3.hu
  
  ErrorLog /web/w3/10016-12345678901234567890123456/weblogs/somewebapp3.hu-apache-error.log
  # Docker based PHP-FPM
  FastCgiExternalServer /www.somewebapp3.hu -socket /var/run/mc-app-sockets/11001-3/socket -idle-timeout 120 -pass-header Authorization
  AddHandler fastcgi/www.somewebapp3.hu .php
  SetNote fastcgi_chroot "/web/w3/10016-12345678901234567890123456"
  SetNote fastcgi_prefix "/web"  
  

</VirtualHost>

`)
                done()
             })
        })

        it('checking nginx conf for vhost somewebapp4.hu (fastcgi)', function(done) {


// note: this webapplication does not go to apache, so we do lookup index.html/index.htm here.
            mapi.get( "/nginx/"+storage_id+"/somewebapp4.hu/raw", function(err, result, httpResponse){
                // console.log(result);
                assert.sequal(result, `server {
    # webapp type: fastcgi, container: foobar4

    include common/listen.conf;
    server_name www.somewebapp4.hu;
    set $mm_webhosting_id "10016";

    access_log "./data/nginx.access.log" main;
    error_log "./data/nginx.error.log" error;

    location @target {
        set $being_proxied 1;

        include common/params_fastcgi;
        # Mitigate https://httpoxy.org/ vulnerabilities
        fastcgi_param HTTP_PROXY "";
        fastcgi_param SCRIPT_FILENAME /web$fastcgi_script_name;
        fastcgi_pass unix:///var/run/mc-app-sockets/11001-4/socket;
    }
    set $doc_root "/web/w3/10016-12345678901234567890123456/somewebapp4.hu/pages/";

    include common/vhost.conf;

    location ~* .(gif|jpg|jpeg|png|ico|wmv|3gp|avi|mpg|mpeg|mp4|flv|mp3|mid|js|css|html|htm|wml|txt|md|rb|scss|ttf|woff|woff2|eot|otf|json|zip|pdf|tar|odt|svg|doc|docx|ppt|pptx)\$ {
        root \$doc_root;
        try_files \$uri \$uri/ @target;
        expires 1d;
    }

    location /xmlrpc.php {
        deny all;
    }

    location / {

       if (\$frame_http_host) {
#         add_header Content-Type text/html;
         return 200 "<!DOCTYPE html><html><frameset cols='100%'><frame src='\$frame_http_host' frameborder='0'></frameset></html>";
       }

       root $doc_root;
       try_files $uri/index.html $uri/index.htm @target;

    }


}

`);
                done()
             })
        })

        it('checking apache conf for vhost somewebapp4.hu (fastcgi)', function(done) {

            mapi.get( "/apache/"+storage_id+"/somewebapp4.hu/raw", function(err, result, httpResponse){
                // console.log(result);

                // no apache config should be generated!
                assert.isNull(err);
                assert.equal(result, "");
                done()
             })
        })

        it('checking nginx conf for vhost somewebapp5.hu (host-legacy)', function(done) {

// note: this application goes to apache, so we dont lookup index.html/htm here
            mapi.get( "/nginx/"+storage_id+"/somewebapp5.hu/raw", function(err, result, httpResponse){
                 // console.log(result);
                 assert.sequal(result, `server {
    # webapp type: host-legacy

    include common/listen.conf;
    server_name www.somewebapp5.hu;
    set $mm_webhosting_id "10016";

    access_log "./data/nginx.access.log" main;
    error_log "./data/nginx.error.log" error;
    set $doc_root "/web/w3/10016-12345678901234567890123456/somewebapp5.hu/pages/";

    include common/vhost.conf;

    include common/target-host-legacy.conf;

    location ~* .(gif|jpg|jpeg|png|ico|wmv|3gp|avi|mpg|mpeg|mp4|flv|mp3|mid|js|css|html|htm|wml|txt|md|rb|scss|ttf|woff|woff2|eot|otf|json|zip|pdf|tar|odt|svg|doc|docx|ppt|pptx)\$ {
        root \$doc_root;
        try_files \$uri \$uri/ @target;
        expires 1d;
    }

    location /xmlrpc.php {
        deny all;
    }

    location / {

       if (\$frame_http_host) {
#         add_header Content-Type text/html;
         return 200 "<!DOCTYPE html><html><frameset cols='100%'><frame src='\$frame_http_host' frameborder='0'></frameset></html>";
       }


       return 450;

    }


}

`);
                 done();

             })
        })

        it('checking apache conf for vhost somewebapp5.hu (host-legacy)', function(done) {

            mapi.get( "/apache/"+storage_id+"/somewebapp5.hu/raw", function(err, result, httpResponse){
                // console.log(result);
                assert.equal(result, `
<VirtualHost *>
  # webapp type: host-legacy
  DocumentRoot "/web/w3/10016-12345678901234567890123456/somewebapp5.hu/pages/"
  ServerName www.somewebapp5.hu
  
  ErrorLog /web/w3/10016-12345678901234567890123456/weblogs/somewebapp5.hu-apache-error.log
  # PHP host-legacy application, PHP version: 7.0
  SetNote fastcgi_chroot "/web/w3/10016-12345678901234567890123456"
  SetNote /fcgi_server_php 127.0.0.1:10630
  
  

</VirtualHost>

`);
                done()
             })
        })


        it('checking nginx conf for vhost somewebapp7.hu (default - fallback to fastcgi)', function(done) {

            // this app does not go to apache, so we do lookup index.htmls here
            mapi.get( "/nginx/"+storage_id+"/somewebapp7.hu/raw", function(err, result, httpResponse){

                // console.log(result);
                assert.sequal(result, `server {
    # webapp type: fastcgi, container: foobar0

    include common/listen.conf;
    server_name www.somewebapp7.hu;
    set $mm_webhosting_id "10016";

    access_log "./data/nginx.access.log" main;
    error_log "./data/nginx.error.log" error;

    location @target {
        set $being_proxied 1;

        include common/params_fastcgi;
        # Mitigate https://httpoxy.org/ vulnerabilities
        fastcgi_param HTTP_PROXY "";
        fastcgi_param SCRIPT_FILENAME /web$fastcgi_script_name;
        fastcgi_pass unix:///var/run/mc-app-sockets/11001-1/socket;
    }
    set $doc_root "/web/w3/10016-12345678901234567890123456/somewebapp7.hu/pages/";

    include common/vhost.conf;

    location ~* .(gif|jpg|jpeg|png|ico|wmv|3gp|avi|mpg|mpeg|mp4|flv|mp3|mid|js|css|html|htm|wml|txt|md|rb|scss|ttf|woff|woff2|eot|otf|json|zip|pdf|tar|odt|svg|doc|docx|ppt|pptx)\$ {
        root \$doc_root;
        try_files \$uri \$uri/ @target;
        expires 1d;
    }

    location /xmlrpc.php {
        deny all;
    }

    location / {

       if (\$frame_http_host) {
#         add_header Content-Type text/html;
         return 200 "<!DOCTYPE html><html><frameset cols='100%'><frame src='\$frame_http_host' frameborder='0'></frameset></html>";
       }

       root $doc_root;
       try_files $uri/index.html $uri/index.htm @target;

    }


}

`);
                done();

             })
        })

        it('checking apache conf for vhost somewebapp7.hu (default - fallback to fastcgi)', function(done) {

            mapi.get( "/apache/"+storage_id+"/somewebapp7.hu/raw", function(err, result, httpResponse){
                // console.log(result);

                // no apache config should be generated!
                assert.isNull(err);
                assert.equal(result, "");
                done()
             })
        })



        it('checking nginx conf for vhost somewebapp8.hu (webserver-unix)', function(done) {

            // this app does not go to apache, so we do lookup index.htmls here
            mapi.get( "/nginx/"+storage_id+"/somewebapp8.hu/raw", function(err, result, httpResponse){

                // console.log(result);
                assert.sequal(result, `server {
    # webapp type: webserver-unix, container: foobar8

    include common/listen.conf;
    server_name www.somewebapp8.hu;
    set $mm_webhosting_id "10016";

    access_log "./data/nginx.access.log" main;
    error_log "./data/nginx.error.log" error;

    location @target {
        set $vhost $host;
        include common/params_proxy;
        proxy_pass http://unix:/var/run/mc-app-sockets/11001-8/socket;
    }
    set $doc_root "/web/w3/10016-12345678901234567890123456/somewebapp8.hu/pages/";

    include common/vhost.conf;

    location ~* .(gif|jpg|jpeg|png|ico|wmv|3gp|avi|mpg|mpeg|mp4|flv|mp3|mid|js|css|html|htm|wml|txt|md|rb|scss|ttf|woff|woff2|eot|otf|json|zip|pdf|tar|odt|svg|doc|docx|ppt|pptx)\$ {
        root \$doc_root;
        try_files \$uri \$uri/ @target;
        expires 1d;
    }

    location /xmlrpc.php {
        deny all;
    }

    location / {

       if (\$frame_http_host) {
#         add_header Content-Type text/html;
         return 200 "<!DOCTYPE html><html><frameset cols='100%'><frame src='\$frame_http_host' frameborder='0'></frameset></html>";
       }

       root $doc_root;
       try_files $uri/index.html $uri/index.htm @target;

    }

}

`);
                done();

             })
        })

        it('checking apache conf for vhost somewebapp8.hu (webserver-unix)', function(done) {

            mapi.get( "/apache/"+storage_id+"/somewebapp8.hu/raw", function(err, result, httpResponse){
                // console.log(result);

                // no apache config should be generated!
                assert.isNull(err);
                assert.equal(result, "");
                done()
             })
        })

        it('handling the notification about a container upgrade (target is a webstore default entry)', function(done) {

             mapi.post( "/docroots/webapps/upgrade", {oldContainer: "foobar0", newParams:{container: "newfoobar0", unix_socket_path: "/var/run/mc-app-sockets/11001-new1/socket"}}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })


        it('list webapps', function(done) {

             mapi.get( "/docroots/webapps/"+storage_id, function(err, result, httpResponse){
                assert.deepEqual(result, {
                    default: {type:"fastcgi", container: "newfoobar0", unix_socket_path: "/var/run/mc-app-sockets/11001-new1/socket"}, 
                    vhosts: {
                        "somewebapp1.hu": {
                            type: "webserver-http",
                            container: "foobar1",
                        },
                        "somewebapp2.hu": {
                            type: "uwsgi",
                            container: "foobar2",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-2/socket", 
                        },
                        "somewebapp3.hu": {
                            type: "php-fpm",
                            container: "foobar3",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-3/socket", 
                        },
                        "somewebapp4.hu": {
                            type: "fastcgi",
                            container: "foobar4",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-4/socket", 
                        },
                        "somewebapp5.hu": {
                            type: "host-legacy",
                        },
                        "somewebapp6.hu": {
                            type: "none"
                        },
                        "somewebapp7.hu": {
                            type: "default"
                        },
                        "somewebapp8.hu": {
                            type: "webserver-unix",
                            container: "foobar8",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-8/socket", 
                        },
                    }
                });

                done()
             })
        })


        it('checking nginx conf for vhost somewebapp7.hu - the unix socket should be updated here a well (default - fallback to fastcgi)', function(done) {

            mapi.get( "/nginx/"+storage_id+"/somewebapp7.hu/raw", function(err, result, httpResponse){

                // console.log(result);
                assert.sequal(result, `server {
    # webapp type: fastcgi, container: newfoobar0

    include common/listen.conf;
    server_name www.somewebapp7.hu;
    set $mm_webhosting_id "10016";

    access_log "./data/nginx.access.log" main;
    error_log "./data/nginx.error.log" error;

    location @target {
        set $being_proxied 1;

        include common/params_fastcgi;
        # Mitigate https://httpoxy.org/ vulnerabilities
        fastcgi_param HTTP_PROXY "";
        fastcgi_param SCRIPT_FILENAME /web$fastcgi_script_name;
        fastcgi_pass unix:///var/run/mc-app-sockets/11001-new1/socket;
    }
    set $doc_root "/web/w3/10016-12345678901234567890123456/somewebapp7.hu/pages/";

    include common/vhost.conf;

    location ~* .(gif|jpg|jpeg|png|ico|wmv|3gp|avi|mpg|mpeg|mp4|flv|mp3|mid|js|css|html|htm|wml|txt|md|rb|scss|ttf|woff|woff2|eot|otf|json|zip|pdf|tar|odt|svg|doc|docx|ppt|pptx)\$ {
        root \$doc_root;
        try_files \$uri \$uri/ @target;
        expires 1d;
    }

    location /xmlrpc.php {
        deny all;
    }

    location / {

       if (\$frame_http_host) {
#         add_header Content-Type text/html;
         return 200 "<!DOCTYPE html><html><frameset cols='100%'><frame src='\$frame_http_host' frameborder='0'></frameset></html>";
       }

       root $doc_root;
       try_files $uri/index.html $uri/index.htm @target;

    }

}

`);
                done();

             })
        })

        it('checking apache conf for vhost somewebapp7.hu should still be missing (default - fallback to fastcgi)', function(done) {

            mapi.get( "/apache/"+storage_id+"/somewebapp7.hu/raw", function(err, result, httpResponse){
                // console.log(result);

                // no apache config should be generated!
                assert.isNull(err);
                assert.equal(result, "");
                done()
             })
        })

        it('switching back to default application', function(done) {

             mapi.post( "/docroots/webapps/"+storage_id+"/somewebapp1.hu", {type: "default"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })
        
        it('switching back to host-legacy as default application', function(done) {

             mapi.post( "/docroots/webapps/"+storage_id, {type: "host-legacy"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('list webapps for reflected changes', function(done) {

             mapi.get( "/docroots/webapps/"+storage_id, function(err, result, httpResponse){
                assert.deepEqual(result, {
                    default: {type:"host-legacy"}, 
                    vhosts: {
                        "somewebapp1.hu": {
                            type: "default"
                        },
                        "somewebapp2.hu": {
                            type: "uwsgi",
                            container: "foobar2",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-2/socket", 
                        },
                        "somewebapp3.hu": {
                            type: "php-fpm",
                            container: "foobar3",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-3/socket", 
                        },
                        "somewebapp4.hu": {
                            type: "fastcgi",
                            container: "foobar4",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-4/socket", 
                        },
                        "somewebapp5.hu": {
                            type: "host-legacy",
                        },
                        "somewebapp6.hu": {
                            type: "none"
                        },
                        "somewebapp7.hu": {
                            type: "default"
                        },
                        "somewebapp8.hu": {
                            type: "webserver-unix",
                            container: "foobar8",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-8/socket", 
                        },

                    }
                });

                done()
             })
        })


        it('checking nginx conf for vhost somewebapp7.hu (default - fallback to host-legacy)', function(done) {

            mapi.get( "/nginx/"+storage_id+"/somewebapp7.hu/raw", function(err, result, httpResponse){
                 // console.log(result);
                 assert.sequal(result, `server {
    # webapp type: host-legacy

    include common/listen.conf;
    server_name www.somewebapp7.hu;
    set $mm_webhosting_id "10016";

    access_log "./data/nginx.access.log" main;
    error_log "./data/nginx.error.log" error;
    set $doc_root "/web/w3/10016-12345678901234567890123456/somewebapp7.hu/pages/";

    include common/vhost.conf;

    include common/target-host-legacy.conf;

    location ~* .(gif|jpg|jpeg|png|ico|wmv|3gp|avi|mpg|mpeg|mp4|flv|mp3|mid|js|css|html|htm|wml|txt|md|rb|scss|ttf|woff|woff2|eot|otf|json|zip|pdf|tar|odt|svg|doc|docx|ppt|pptx)\$ {
        root \$doc_root;
        try_files \$uri \$uri/ @target;
        expires 1d;
    }

    location /xmlrpc.php {
        deny all;
    }

    location / {

       if (\$frame_http_host) {
#         add_header Content-Type text/html;
         return 200 "<!DOCTYPE html><html><frameset cols='100%'><frame src='\$frame_http_host' frameborder='0'></frameset></html>";
       }


       return 450;

    }


}

`);
                 done();
             })
        })

        it('checking apache conf for vhost somewebapp7.hu (default - fallback to host-legacy)', function(done) {

            mapi.get( "/apache/"+storage_id+"/somewebapp7.hu/raw", function(err, result, httpResponse){
                // console.log(result);
                assert.equal(result, `
<VirtualHost *>
  # webapp type: host-legacy
  DocumentRoot "/web/w3/10016-12345678901234567890123456/somewebapp7.hu/pages/"
  ServerName www.somewebapp7.hu
  
  ErrorLog /web/w3/10016-12345678901234567890123456/weblogs/somewebapp7.hu-apache-error.log
  # PHP host-legacy application, PHP version: 7.0
  SetNote fastcgi_chroot "/web/w3/10016-12345678901234567890123456"
  SetNote /fcgi_server_php 127.0.0.1:10630
  
  

</VirtualHost>

`);
                done()
             })
        })

        
        it('handling the notification about a container upgrade (target is a vhost entry)', function(done) {

             mapi.post( "/docroots/webapps/upgrade", {oldContainer: "foobar2", newParams:{container: "newfoobar2", unix_socket_path: "/var/run/mc-app-sockets/11001-new2/socket"}}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('list webapps for reflected changes', function(done) {

             mapi.get( "/docroots/webapps/"+storage_id, function(err, result, httpResponse){
                assert.deepEqual(result, {
                    default: {type:"host-legacy"}, 
                    vhosts: {
                        "somewebapp1.hu": {
                            type: "default"
                        },
                        "somewebapp2.hu": {
                            type: "uwsgi",
                            container: "newfoobar2",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-new2/socket", 
                        },
                        "somewebapp3.hu": {
                            type: "php-fpm",
                            container: "foobar3",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-3/socket", 
                        },
                        "somewebapp4.hu": {
                            type: "fastcgi",
                            container: "foobar4",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-4/socket", 
                        },
                        "somewebapp5.hu": {
                            type: "host-legacy",
                        },
                        "somewebapp6.hu": {
                            type: "none"
                        },
                        "somewebapp7.hu": {
                            type: "default"
                        },
                        "somewebapp8.hu": {
                            type: "webserver-unix",
                            container: "foobar8",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-8/socket", 
                        },

                    }
                });

                done()
             })
        })

        it('should have the unix socket updated for vhost somewebapp2.hu (uwsgi)', function(done) {

            mapi.get( "/nginx/"+storage_id+"/somewebapp2.hu/raw", function(err, result, httpResponse){
                // console.log(result);
                assert.sequal(result, `server {
    # webapp type: uwsgi, container: newfoobar2

    include common/listen.conf;
    server_name www.somewebapp2.hu;
    set $mm_webhosting_id "10016";

    access_log "./data/nginx.access.log" main;
    error_log "./data/nginx.error.log" error;

    location @target {
        set $being_proxied 1;

        include common/params_uwsgi;
        uwsgi_pass unix:///var/run/mc-app-sockets/11001-new2/socket;
    }
    set $doc_root "/web/w3/10016-12345678901234567890123456/somewebapp2.hu/pages/";

    include common/vhost.conf;

    location ~* .(gif|jpg|jpeg|png|ico|wmv|3gp|avi|mpg|mpeg|mp4|flv|mp3|mid|js|css|html|htm|wml|txt|md|rb|scss|ttf|woff|woff2|eot|otf|json|zip|pdf|tar|odt|svg|doc|docx|ppt|pptx)\$ {
        root \$doc_root;
        try_files \$uri \$uri/ @target;
        expires 1d;
    }

    location /xmlrpc.php {
        deny all;
    }

    location / {

       if (\$frame_http_host) {
#         add_header Content-Type text/html;
         return 200 "<!DOCTYPE html><html><frameset cols='100%'><frame src='\$frame_http_host' frameborder='0'></frameset></html>";
       }

       root $doc_root;
       try_files $uri/index.html $uri/index.htm @target;

    }


}

`)
                done()
             })
        })

        it('and apache conf should be untouched for vhost somewebapp2.hu (uwsgi)', function(done) {

            mapi.get( "/apache/"+storage_id+"/somewebapp2.hu/raw", function(err, result, httpResponse){
                // console.log(result);

                // no apache config should be generated!
                assert.isNull(err);
                assert.equal(result, "");
                done()
             })
        })

        
        it('handling the notification about a container upgrade (webstore limited)', function(done) {

             mapi.post( "/docroots/webapps/"+storage_id+"/upgrade", {oldContainer: "newfoobar2", newParams:{container: "newnewfoobar2", unix_socket_path: "/var/run/mc-app-sockets/11001-newnew2/socket"}}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('list webapps for reflected changes', function(done) {

             mapi.get( "/docroots/webapps/"+storage_id, function(err, result, httpResponse){
                assert.deepEqual(result, {
                    default: {type:"host-legacy"}, 
                    vhosts: {
                        "somewebapp1.hu": {
                            type: "default"
                        },
                        "somewebapp2.hu": {
                            type: "uwsgi",
                            container: "newnewfoobar2",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-newnew2/socket", 
                        },
                        "somewebapp3.hu": {
                            type: "php-fpm",
                            container: "foobar3",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-3/socket", 
                        },
                        "somewebapp4.hu": {
                            type: "fastcgi",
                            container: "foobar4",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-4/socket", 
                        },
                        "somewebapp5.hu": {
                            type: "host-legacy",
                        },
                        "somewebapp6.hu": {
                            type: "none"
                        },
                        "somewebapp7.hu": {
                            type: "default"
                        },
                        "somewebapp8.hu": {
                            type: "webserver-unix",
                            container: "foobar8",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-8/socket", 
                        },

                    }
                });

                done()
             })
        })

        it('should have the unix socket updated for vhost somewebapp2.hu (uwsgi)', function(done) {

            mapi.get( "/nginx/"+storage_id+"/somewebapp2.hu/raw", function(err, result, httpResponse){
                // console.log(result);
                assert.sequal(result, `server {
    # webapp type: uwsgi, container: newnewfoobar2

    include common/listen.conf;
    server_name www.somewebapp2.hu;
    set $mm_webhosting_id "10016";

    access_log "./data/nginx.access.log" main;
    error_log "./data/nginx.error.log" error;

    location @target {
        set $being_proxied 1;

        include common/params_uwsgi;
        uwsgi_pass unix:///var/run/mc-app-sockets/11001-newnew2/socket;
    }
    set $doc_root "/web/w3/10016-12345678901234567890123456/somewebapp2.hu/pages/";

    include common/vhost.conf;

    location ~* .(gif|jpg|jpeg|png|ico|wmv|3gp|avi|mpg|mpeg|mp4|flv|mp3|mid|js|css|html|htm|wml|txt|md|rb|scss|ttf|woff|woff2|eot|otf|json|zip|pdf|tar|odt|svg|doc|docx|ppt|pptx)\$ {
        root \$doc_root;
        try_files \$uri \$uri/ @target;
        expires 1d;
    }

    location /xmlrpc.php {
        deny all;
    }
    
    location / {

       if (\$frame_http_host) {
#         add_header Content-Type text/html;
         return 200 "<!DOCTYPE html><html><frameset cols='100%'><frame src='\$frame_http_host' frameborder='0'></frameset></html>";
       }

       root $doc_root;
       try_files $uri/index.html $uri/index.htm @target;

    }


}

`)
                done()
             })
        })

        it('and apache conf should be still untouched for vhost somewebapp2.hu (uwsgi)', function(done) {

            mapi.get( "/apache/"+storage_id+"/somewebapp2.hu/raw", function(err, result, httpResponse){
                // console.log(result);

                // no apache config should be generated!
                assert.isNull(err);
                assert.equal(result, "");
                done()
             })
        })




        it('changing the default webapp', function(done) {

             mapi.post( "/docroots/webapps/"+storage_id, {type: "fastcgi", container: "foobar0", unix_socket_path: "/var/run/mc-app-sockets/11001-1/socket"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('handling the notification about a container removal (targeting a vhost app)', function(done) {

             mapi.post( "/docroots/webapps/detach", {oldContainer: "newnewfoobar2"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })
        
        it('handling the notification about a container removal (targeting the default app)', function(done) {

             mapi.post( "/docroots/webapps/detach", {oldContainer: "foobar0"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('list webapps for reflected changes', function(done) {

             mapi.get( "/docroots/webapps/"+storage_id, function(err, result, httpResponse){
                assert.deepEqual(result, {
                    default: {type:"none"}, 
                    vhosts: {
                        "somewebapp1.hu": {
                            type: "default"
                        },
                        "somewebapp2.hu": {
                            type: "default",
                        },
                        "somewebapp3.hu": {
                            type: "php-fpm",
                            container: "foobar3",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-3/socket", 
                        },
                        "somewebapp4.hu": {
                            type: "fastcgi",
                            container: "foobar4",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-4/socket", 
                        },
                        "somewebapp5.hu": {
                            type: "host-legacy",
                        },
                        "somewebapp6.hu": {
                            type: "none"
                        },
                        "somewebapp7.hu": {
                            type: "default"
                        },
                        "somewebapp8.hu": {
                            type: "webserver-unix",
                            container: "foobar8",
                            unix_socket_path: "/var/run/mc-app-sockets/11001-8/socket", 
                        },

                    }
                });

                done()
             })
        })

    })


})
