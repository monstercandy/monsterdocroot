require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../docroot-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){
    "use strict"

    const MError = require('MonsterExpress').Error

    var storage_id = 10001
    var storage_id_hello = 10002

    var Common = require("./000-common.js")(assert)


    describe("docroots", function(){



        it('list webhostings', function(done) {

             mapi.get( "/docroots/", function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code
                assert.deepEqual(result, []);

                done()
             })
        })

        it('list webhostings and domains', function(done) {

             mapi.search( "/docroots/", function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code

                assert.deepEqual(result, []);

                done()
             })
        })

        it('list domains of a webhosting (should fail)', function(done) {

             var storage_id_should_missing = 99999

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfoNotFound(storage_id_should_missing)


             mapi.get( "/docroots/"+storage_id_should_missing, function(err, result, httpResponse){

                assert.equal(err.message, "INTERNAL_ERROR");

                done()
             })
        })


        it('add a docroot', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id)

             mapi.put( "/docroots/"+storage_id+"/example.hu/entries", {"host": "www.example.hu", "docroot": "/example.hu/pages"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })


        it('relocating to another webhosting', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo();

             mapi.post( "/docroots/"+storage_id+"/example.hu/move", {dest_wh_id: 22222}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                done()
             })
        })


        it('list domains of a webhosting where the domain was moved from', function(done) {

             mapi.get( "/docroots/"+storage_id, function(err, result, httpResponse){
                assert.deepEqual(result, []);

                done()
             })
        })


        it('list domains of a webhosting where the domain was moved to', function(done) {

             mapi.get( "/docroots/22222", function(err, result, httpResponse){

                assert.deepEqual(result, ["example.hu"]);

                done()
             })
        })

        new Array("apache", "nginx").forEach(p=>{
            it('pathes in webserver configs: '+p, function(done) {

                mapi.get( "/"+p+"/22222/example.hu/raw", function(err, result, httpResponse){
                    assert.isNull(err);
                    assert.isNotNull(result);
                    assert.ok(result.length > 50);
                    assert.ok(result.indexOf("/web/w3/22222-12345") > 0);

                    done()
                 })
            })
        })

        it('relocating it back to original to recover the flow of the unit test', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo();

             mapi.post( "/docroots/22222/example.hu/move", {dest_wh_id: storage_id}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                // restoring the mocked info provider as well
                app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id);

                done()
             })
        })

        for (let docroot of ["/foo", "/foo/../foo", "/; ls -la", "/"]) {
            it('invalid docroot should be rejected: '+docroot, function(done) {

                 mapi.put( "/docroots/"+storage_id+"/example.hu/entries", {"host": "www.example.hu", "docroot": docroot}, function(err, result, httpResponse){
                    assert.propertyVal(err, "message", "VALIDATION_ERROR");

                    done()
                 })
            })

        }

            it('docroot without leading slash should be accepted', function(done) {

                 mapi.put( "/docroots/"+storage_id+"/example.hu/entries", {"host": "foo.example.hu", "docroot": "foo/bar"}, function(err, result, httpResponse){
                    assert.equal(result, "ok");

                    done()
                 })
            })


        it('changing a docroot by putting with the same key', function(done) {


             mapi.put( "/docroots/"+storage_id+"/example.hu/entries", {"host": "www.example.hu", "docroot": "/something/else"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('get entries', function(done) {

             mapi.get( "/docroots/"+storage_id+"/example.hu/entries", function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code
                assert.deepEqual(result, [
                 {
                    "docroot": "/foo/bar/",
                    "host": "foo.example.hu",
                 },
                 {
                    "docroot": "/something/else/",
                    "host": "www.example.hu"
                 }
                ])

                done()
             })
        })

        it('changing a docroot based on entry', function(done) {


             mapi.post( "/docroots/"+storage_id+"/example.hu/entries/www.example.hu", {"docroot": "/something/completely/different"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('get settings along with entries', function(done) {

             mapi.get( "/docroots/"+storage_id+"/example.hu", function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code
                assert.deepEqual(result, {"awstats_allowed": false,webapp:{type:"default"},"protected-dirs":[],"entries":[
                    {"docroot": "/foo/bar/","host": "foo.example.hu"},
                    {"docroot": "/something/completely/different/","host": "www.example.hu"}
                ]})

                done()
             })
        })


        it('list webhostings again, '+storage_id+' should be returned', function(done) {

             mapi.get( "/docroots/", function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code
                assert.deepEqual(result, [""+storage_id]);

                done()
             })
        })

        it('list webhostings and domains', function(done) {

             mapi.search( "/docroots/", function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code

                assert.deepEqual(result, [{"wh":""+storage_id, "domain":"example.hu"}]);

                done()
             })
        })


        it('list domains of a webhosting', function(done) {

             mapi.get( "/docroots/"+storage_id, function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code

                assert.deepEqual(result, ["example.hu"]);

                done()
             })
        })



        it('remove entry', function(done) {

             mapi.delete("/docroots/"+storage_id+"/example.hu", {}, function(err, result, httpResponse){
                assert.equal(result, "ok")

                done()
             })
        })


        it('adding some entries under the same domain', function(done) {


             mapi.post( "/docroots/"+storage_id+"/example.hu/entries/sub1.example.hu", {"docroot": "/docroot1/pages"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                 mapi.post( "/docroots/"+storage_id+"/example.hu/entries/sub2.example.hu", {"docroot": "/docroot2/pages"}, function(err, result, httpResponse){
                     assert.equal(result, "ok");

                     mapi.post( "/docroots/"+storage_id+"/example.hu/entries/sub3.example.hu", {"docroot": "/docroot2/pages"}, function(err, result, httpResponse){
                        assert.equal(result, "ok");

                        mapi.post( "/docroots/"+storage_id+"/example.hu/entries/sub4.example.hu", {"docroot": "/docroot2/pages"}, function(err, result, httpResponse){
                            assert.equal(result, "ok");

                            done()
                         })

                     })
                 })

             })
        })


        it('trying to add the same host as the domain', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id_hello)

             mapi.put( "/docroots/"+storage_id_hello+"/hello.hu/entries", {"host":"hello.hu", "docroot": "/some/docroot"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()

             })
        })

        it('trying to add the same entry under another domain', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id)

             mapi.put( "/docroots/"+storage_id+"/sub1.example.hu/entries", {"host":"sub1.example.hu", "docroot": "/some/docroot"}, function(err, result, httpResponse){
                assert.propertyVal(err, "message", "HOST_ALREADY_CONFIGURED");
                done()

             })
        })
        it('trying to add the same entry under another domain, different entry point', function(done) {

             mapi.post( "/docroots/"+storage_id+"/sub1.example.hu/entries/sub1.example.hu", {"docroot": "/some/docroot"}, function(err, result, httpResponse){
                assert.propertyVal(err, "message", "HOST_ALREADY_CONFIGURED");
                done()

             })
        })

        it('deleting a domain completely', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id_hello)

             mapi.delete("/docroots/"+storage_id_hello+"/hello.hu", {}, function(err, result, httpResponse){
                assert.equal(result, "ok")

                done()
             })
        })


        it('asterisk should be accepted', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id)

            mapi.put( "/docroots/"+storage_id+"/example.hu/entries", {"host": "*.example.hu", "docroot": "/docroot2/pages"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })


        it('adding foreign domain should be rejected', function(done) {

            mapi.put( "/docroots/"+storage_id+"/example.hu/entries", {"host": "something-else.hu", "docroot": "/docroot2/pages"}, function(err, result, httpResponse){

                assert.propertyVal(err, "message", "INVALID_HOST");

                done()
             })
        })


        it('unless the force parameter was also sent', function(done) {

            mapi.put( "/docroots/"+storage_id+"/example.hu/entries", {"host": "something-else.hu", "force": true, "docroot": "/docroot2/pages"}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                done()
             })
        })


        it('but it should still not be possible to add it twice', function(done) {

            mapi.put( "/docroots/"+storage_id+"/nyazsgem.hu/entries", {"host": "something-else.hu", "force": true,"docroot": "/docroot2/pages"}, function(err, result, httpResponse){

                assert.propertyVal(err, "message", "HOST_ALREADY_CONFIGURED");
                done()
             })
        })

        it('querying all configured host entries', function(done) {
            mapi.get( "/docroots/hostentries/"+storage_id, function(err, result, httpResponse){

                assert.deepEqual(result, [ { raw: 'example.hu/sub1.example.hu',
    domain: 'example.hu',
    host: 'sub1.example.hu' },
  { raw: 'example.hu/sub2.example.hu',
    domain: 'example.hu',
    host: 'sub2.example.hu' },
  { raw: 'example.hu/sub3.example.hu',
    domain: 'example.hu',
    host: 'sub3.example.hu' },
  { raw: 'example.hu/sub4.example.hu',
    domain: 'example.hu',
    host: 'sub4.example.hu' },
  { raw: 'example.hu/*.example.hu',
    domain: 'example.hu',
    host: '*.example.hu' },
  { raw: 'example.hu/something-else.hu',
    domain: 'example.hu',
    host: 'something-else.hu' } ]);
                done()
             })
        })

        it('querying host=>docroot mapping', function(done) {
            mapi.get( "/docroots/hostmap/"+storage_id, function(err, result, httpResponse){

                assert.deepEqual(result, { 'sub1.example.hu': '/docroot1/pages/',
  'sub2.example.hu': '/docroot2/pages/',
  'sub3.example.hu': '/docroot2/pages/',
  'sub4.example.hu': '/docroot2/pages/',
  '*.example.hu': '/docroot2/pages/',
  'something-else.hu': '/docroot2/pages/' });
                done()
             })
        })

        it('querying host=>docroot mapping (when nothing is configured)', function(done) {
            var webhosting_id_that_is_found_but_no_entry_exists = "85498714"
            app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(webhosting_id_that_is_found_but_no_entry_exists)
            mapi.get( "/docroots/hostmap/"+webhosting_id_that_is_found_but_no_entry_exists, function(err, result, httpResponse){

                assert.deepEqual(result, { });

                app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id)
                done()
             })
        })

        it('querying distinct docroots', function(done) {

            mapi.get( "/docroots/distinct/"+storage_id, function(err, result, httpResponse){

                assert.deepEqual(result, { '/docroot1/pages/': [ 'sub1.example.hu' ],
  '/docroot2/pages/':
   [ 'sub2.example.hu',
     'sub3.example.hu',
     'sub4.example.hu',
     '*.example.hu',
     'something-else.hu' ] });
                done()
             })
        })
        it('querying complete distinct pathes', function(done) {


            mapi.get( "/docroots/complete/distinct", function(err, result, httpResponse){
                assert.deepEqual(result, { '/web/w3/10001-12345678901234567890123456/docroot1/pages/': [ 'sub1.example.hu' ],
  '/web/w3/10001-12345678901234567890123456/docroot2/pages/':
   [ 'sub2.example.hu',
     'sub3.example.hu',
     'sub4.example.hu',
     '*.example.hu',
     'something-else.hu' ] });
                done()
             })
        })
        it('querying complete distinct logdirs', function(done) {
            mapi.get( "/docroots/complete/distinct-logdirs", function(err, result, httpResponse){
                assert.deepEqual(result, { '/web/w3/10001-12345678901234567890123456/weblogs': [ 'example.hu' ]});
                done()
             })
        })
        new Array("docroots", "apache", "nginx").forEach(p=>{
            it('raw data should be returned for: '+p, function(done) {

                mapi.get( "/"+p+"/"+storage_id+"/example.hu/raw", function(err, result, httpResponse){
                    assert.isNull(err);
                    assert.isNotNull(result);
                    assert.ok(result.length > 50);
                    // assert.ok(result.substr(0,3) == '#{"');

                    done()
                 })
            })

        })

        it('suspending the domain', function(done) {

            mapi.post( "/docroots/"+storage_id+"/example.hu/suspend", {"suspended": true}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                done()
             })
        })


        it('and now it should be possible to be added to somewhere else', function(done) {

            mapi.put( "/docroots/"+storage_id+"/nyazsgem.hu/entries", {"host": "something-else.hu", "force": true,"docroot": "/docroot2/pages"}, function(err, result, httpResponse){

                assert.equal(result, "ok");
                done()
             })
        })

        new Array("apache", "nginx").forEach(p=>{
            it('raw data should be empty in the webservers: '+p, function(done) {

                mapi.get( "/"+p+"/"+storage_id+"/example.hu/raw", function(err, result, httpResponse){
                    assert.isNull(err);
                    assert.equal(result, "");

                    done()
                 })
            })

        })


        it('reenabling the domain should throw exception since there is a collision', function(done) {

            mapi.post( "/docroots/"+storage_id+"/example.hu/suspend", {"suspended": false}, function(err, result, httpResponse){

                assert.propertyVal(err, "message", "HOST_ALREADY_CONFIGURED");
                done()
             })
        })


        it('removing the duplicate', function(done) {

             mapi.delete("/docroots/"+storage_id+"/nyazsgem.hu/entries/something-else.hu", {force:true}, function(err, result, httpResponse){
                assert.equal(result, "ok")

                done()
             })
        })

        it('reenabling the domain should be ok now', function(done) {

            mapi.post( "/docroots/"+storage_id+"/example.hu/suspend", {"suspended": false}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })

        new Array("apache", "nginx").forEach(p=>{
            it('raw data should be returned again for: '+p, function(done) {

                mapi.get( "/"+p+"/"+storage_id+"/example.hu/raw", function(err, result, httpResponse){
                    assert.isNull(err);
                    assert.isNotNull(result);
                    assert.ok(result.length > 50);
                    // assert.ok(result.substr(0,3) == '#{"');

                    done()
                 })
            })

        })

        it('changing the request timeout should be ok', function(done) {

            mapi.post( "/docroots/"+storage_id+"/example.hu/timeout", {"timeout": 300}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })


        it('it should be reflected in raw nginx data', function(done) {

            mapi.get( "/nginx/"+storage_id+"/example.hu/raw", function(err, result, httpResponse){
                // console.log(result); process.reallyExit();

                assert.isNull(err);
                assert.isNotNull(result);
                assert.ok(result.indexOf("proxy_read_timeout 300;") > 0);
                assert.ok(result.indexOf("gif|jpg|jpeg|png|ico|wmv") > 0);
                assert.ok(result.indexOf("gzip on;") < 0);

                assert.ok(result.indexOf("location /xmlrpc.php") > 0); // must be present

                done()
             })
        })

        it('it should be reflected in raw apache data', function(done) {

            mapi.get( "/apache/"+storage_id+"/example.hu/raw", function(err, result, httpResponse){
                assert.isNull(err);
                assert.isNotNull(result);
                assert.ok(result.indexOf("SetNote fastcgi_idle_timeout 300") > 0);

                assert.ok(result.indexOf("ErrorLog ") > 0);
                assert.ok(result.indexOf("/weblogs/") > 0);

                done()
             })
        })



        it('changing httpsOnly: true', function(done) {

            mapi.post( "/docroots/"+storage_id+"/example.hu/httpsonly", {"httpsonly": true}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })

        it('changing hstsHeaders: true', function(done) {

            mapi.post( "/docroots/"+storage_id+"/example.hu/hstsheaders", {"hstsheaders": true}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })

        it('changing antiClickjackingHeaders: true', function(done) {

            mapi.post( "/docroots/"+storage_id+"/example.hu/anticlickjackingheaders", {"anticlickjackingheaders": true}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })

        it('changing skipnginxstaticfiles: true', function(done) {

            mapi.post( "/docroots/"+storage_id+"/example.hu/skipnginxstaticfiles", {"skipnginxstaticfiles": true}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })

        it('changing nginxgzip: true', function(done) {

            mapi.post( "/docroots/"+storage_id+"/example.hu/nginxgzip", {"nginxgzip": true}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })

        it('changing forceredirect: true', function(done) {

            mapi.post( "/docroots/"+storage_id+"/example.hu/forceredirect", {"forceredirect": true}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })

        it('turning on django mode', function(done) {

            mapi.post( "/docroots/"+storage_id+"/example.hu/djangostatic", {"djangostatic": true}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })

        it('changing dontblockxmlrpc', function(done) {

            mapi.post( "/docroots/"+storage_id+"/example.hu/dontblockxmlrpc", {"dontblockxmlrpc": true}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })

        it('the previous 5 should be reflected in raw nginx data', function(done) {

            mapi.get( "/nginx/"+storage_id+"/example.hu/raw", function(err, result, httpResponse){
                // console.log(result); process.reallyExit();

                assert.isNull(err);
                assert.isNotNull(result);
                assert.ok(result.indexOf("return 301 https:") > 0);
                assert.ok(result.indexOf("add_header Strict-Transport-Secu") > 0);
                assert.ok(result.indexOf("add_header X-Frame-Options") > 0);

                assert.ok(result.indexOf("rewrite ^ $redirect_http_host redirect") > 0);

                assert.ok(result.indexOf("location /xmlrpc.php") < 0); // the block shall be missing

                Array("/static", "/media").forEach(path=>{
                    assert.ok(result.indexOf("location "+path+" {") > 0);
                    assert.ok(result.indexOf('alias "$doc_root'+path+'";') > 0);

                })

                done()
             })
        })


        it('changing httpsOnly: false', function(done) {

            mapi.post( "/docroots/"+storage_id+"/example.hu/httpsonly", {"httpsonly": false}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })

        it('changing hstsHeaders: false', function(done) {

            mapi.post( "/docroots/"+storage_id+"/example.hu/hstsheaders", {"hstsheaders": false}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })

        it('changing antiClickjackingHeaders: false', function(done) {

            mapi.post( "/docroots/"+storage_id+"/example.hu/anticlickjackingheaders", {"anticlickjackingheaders": false}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })

        it('the previous 5 should have disappeared in raw nginx data', function(done) {

            mapi.get( "/nginx/"+storage_id+"/example.hu/raw", function(err, result, httpResponse){
                // console.log(result); process.reallyExit();

                assert.isNull(err);
                assert.isNotNull(result);
                assert.ok(result.indexOf("return 301 https:") < 0);
                assert.ok(result.indexOf("add_header Strict-Transport-Secu") < 0);
                assert.ok(result.indexOf("add_header X-Frame-Options") < 0);
                assert.ok(result.indexOf("gif|jpg|jpeg|png|ico|wmv") < 0);
                assert.ok(result.indexOf("gzip on;") > 0);

                done()
             })
        })
    })



    describe("bandwidth exceeded", function(){

        it('add a docroot', function(done) {

             mapi.put( "/docroots/"+storage_id+"/bandwidth.hu/entries", {"host": "www.bandwidth.hu", "docroot": "/bandwidth.hu/pages"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('marking the domain as bandwidth exceeded', function(done) {

            mapi.post( "/docroots/"+storage_id+"/bandwidth.hu/bandwidthexceeded", {"bandwidth-exceeded": true}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                done()
             })
        })

        it('it should be reflected in raw nginx data', function(done) {

            mapi.get( "/nginx/"+storage_id+"/bandwidth.hu/raw", function(err, result, httpResponse){
                assert.isNull(err);
                assert.isNotNull(result);
                assert.ok(result.match(/return 503 ".+";/));
                done();
             })
        })

        it('releasing the domain bandwidth exceeded flag', function(done) {

            mapi.post( "/docroots/"+storage_id+"/bandwidth.hu/bandwidthexceeded", {"bandwidth-exceeded": false}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                done()
             })
        })

        it('it should be reflected in raw nginx data', function(done) {

            mapi.get( "/nginx/"+storage_id+"/bandwidth.hu/raw", function(err, result, httpResponse){
                assert.isNull(err);
                assert.isNotNull(result);
                assert.ok(!result.match(/return 503 ".+";/));
                done();
             })
        })

    });

    describe("postdata", function(){

        it('add a docroot', function(done) {

             mapi.put( "/docroots/"+storage_id+"/postdata.hu/entries", {"host": "www.postdata.hu", "docroot": "/postdata.hu/pages"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('turning on postdata for the domain', function(done) {

            mapi.post( "/docroots/"+storage_id+"/postdata.hu/postdata", {"postdata": true}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                done()
             })
        })

        it('it should be reflected in raw nginx data', function(done) {

            mapi.get( "/nginx/"+storage_id+"/postdata.hu/raw", function(err, result, httpResponse){
                assert.isNull(err);
                assert.isNotNull(result);
                assert.ok(result.indexOf(" postdata if=\$being_proxied;") > 0);
                done();
             })
        })

        it('turning off postdata', function(done) {

            mapi.post( "/docroots/"+storage_id+"/postdata.hu/postdata", {"postdata": false}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                done()
             })
        })

        it('it should be reflected in raw nginx data', function(done) {

            mapi.get( "/nginx/"+storage_id+"/postdata.hu/raw", function(err, result, httpResponse){
                assert.isNull(err);
                assert.isNotNull(result);
                assert.ok(result.indexOf(" postdata if=\$being_proxied;") < 0);
                done();
             })
        })

    });

    describe("same vhost on multiple storages", function(){

        it('add a docroot first', function(done) {

             mapi.put( "/docroots/"+storage_id+"/samevhost.hu/entries", {"host": "www.samevhost.hu", "docroot": "/samevhost.hu/pages"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        it('add a docroot: second attempt of the same host should be rejceted', function(done) {


             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id_hello)

             mapi.put( "/docroots/"+storage_id_hello+"/samevhost.hu/entries", {"host": "www.samevhost.hu", "docroot": "/samevhost.hu/pages"}, function(err, result, httpResponse){
                assert.propertyVal(err, "message", "HOST_ALREADY_CONFIGURED");

                done()
             })
        })


    })


})
