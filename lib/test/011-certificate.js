require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../docroot-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){
    "use strict"

    const MError = require('MonsterExpress').Error
    const path = require("path")

    var storage_id = 21001
    var certificate_id = 12345
    var Common = require("./000-common.js")(assert)


    describe("setup", function(){

        const fs = require("MonsterDotq").fs()

        it('create some dummy certificate/key pair', function() {

            var ps = []
            Array("key","fullchain.crt").forEach(suffix=>{
                var fn = path.join(app.config.get("tls_certificate_store_directory"), certificate_id+"."+suffix)

                ps.push(fs.writeFileAsync(fn, "some stuff"))

            })

            // console.log(p)

            return Promise.all(ps)

        })

        it('add a docroot', function(done) {

             app.MonsterInfoWebhosting = Common.mockedWebhostingInfo(storage_id)

             mapi.put( "/docroots/"+storage_id+"/certificate.hu/entries", {"host": "www.certificate.hu", "docroot": "/certificate.hu/pages"}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })



    })



    describe("test", function(){


        it('setting invalid certificate id should be rejected', function(done) {

            mapi.post( "/docroots/"+storage_id+"/certificate.hu/certificate", {"certificate_id": "../../foobar"}, function(err, result, httpResponse){
                assert.propertyVal(err, "message", "VALIDATION_ERROR");

                done()
             })
        })


        setCert()


        it('should be visible in the docroot properties', function(done) {

            mapi.get( "/docroots/"+storage_id+"/certificate.hu", function(err, result, httpResponse){
                assert.propertyVal(result, "certificate_id", ""+certificate_id);

                done()
             })
        })


        it('and should contain the relevant lines in nginx conf', function(done) {

            mapi.get( "/nginx/"+storage_id+"/certificate.hu/raw", function(err, result, httpResponse){
                assert.isNull(err);
                assert.isNotNull(result);
                assert.ok(result.match(/ssl_certificate ".+[0-9]+\.fullchain\.crt";/));
                assert.ok(result.match(/ssl_certificate_key ".+[0-9]+\.key";/));

                done()
             })
        })


        it('querying docroots that belong to a certificate', function(done) {

            mapi.get( "/certificates/"+certificate_id, function(err, result, httpResponse){
                assert.isNull(err);
                assert.property(result, "docroots");
                assert.deepEqual(result.docroots, ["certificate.hu"]);
                done()
             })
        })

        it('detaching certificate', function(done) {

            mapi.post( "/docroots/"+storage_id+"/certificate.hu/certificate", {"certificate_id": ""}, function(err, result, httpResponse){
                assert.isNull(err);
                assert.equal(result, "ok");

                done()
             })
        })

        it('querying docroots that belong to a certificate should not be returned it anymore', function(done) {

            mapi.get( "/certificates/"+certificate_id, function(err, result, httpResponse){
                assert.isNull(err);
                assert.property(result, "docroots");
                assert.deepEqual(result.docroots, []);
                done()
             })
        })

        it('should not be visible in the docroot properties anymore', function(done) {

            mapi.get( "/docroots/"+storage_id+"/certificate.hu", function(err, result, httpResponse){
                assert.notProperty(result, "certificate_id");

                done()
             })
        })

        shouldNotBeInNginx()


        setCert()


        it('querying docroots mapped by certificate', function(done) {

            mapi.get( "/docroots/certificates/"+storage_id, function(err, result, httpResponse){

                console.log(result)

                assert.deepEqual(result, { '12345': [ 'certificate.hu' ] });
                done()
             })
        })


        it('detaching certificate from all mappings at once', function(done) {

            mapi.delete( "/certificates/"+certificate_id, {}, function(err, result, httpResponse){
                assert.equal(result, "ok");

                done()
             })
        })

        shouldNotBeInNginx()

    })


  function setCert(){
        it('set certificate', function(done) {

            mapi.post( "/docroots/"+storage_id+"/certificate.hu/certificate", {"certificate_id": certificate_id}, function(err, result, httpResponse){
                assert.isNull(err);
                assert.equal(result, "ok");

                done()
             })
        })
  }

  function shouldNotBeInNginx(){

        it('relevant lines should have disappeared in nginx conf', function(done) {

            mapi.get( "/nginx/"+storage_id+"/certificate.hu/raw", function(err, result, httpResponse){
                assert.isNull(err);
                assert.isNotNull(result);
                assert.notOk(result.match(/ssl_certificate_key/));
                assert.notOk(result.match(/ssl_certificate/));

                done()
             })
        })
  }


})
