require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

const existsLib = require("lib-exists.js")
const assert = require("ExpressTester").assert

describe('exists lib', function() {


  it("filtering array input correctly", function(){

      return existsLib(["foo", "lib-apache.js", "nonexistent"])
        .then(d=>{
            assert.deepEqual(d, ["lib-apache.js"])
        })
  })

  it("filtering hash input correctly", function(){

      return existsLib({a:"foo", b:"lib-apache.js", c:"nonexistent"})
        .then(d=>{
            assert.deepEqual(d, {b:"lib-apache.js"})
        })
  })

})
