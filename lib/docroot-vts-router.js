module.exports = function(app, subsystem) {

    var vts = require("lib-vts.js")(app)

    var router = app.ExpressPromiseRouter()

    router.get( "/raw", function (req, res, next) {
        return req.sendPromResultAsIs(vts.Fetch());
    })

    router.post( "/process", function (req, res, next) {
        return req.sendOk(vts.FetchAndStoreAndMarkAndRelease());
    })

    router.post( "/compress", function (req, res, next) {
        return req.sendOk(vts.Compress());
    })

    router.get( "/exceeded", function (req, res, next) {
        return req.sendPromResultAsIs(vts.ListWebstoresExceededCurrently(null, app.mockedNow));
    })

    router.post( "/reprotect-auth", function (req, res, next) {
        return req.sendOk(vts.ReprotectAuthentication());
    })

    router.post( "/vts-url", function (req, res, next) {
        return req.sendPromResultAsIs(vts.GetVtsUrl(req.body.json));
    })

    router.post( "/mrtg-url", function (req, res, next) {
        return req.sendPromResultAsIs(vts.GetMrtgUrl(req.body.json));
    })

   // this one is needed for unit tests
    router.post( "/:webhosting_id/truncate", function (req, res, next) {
        return req.sendOk(vts.Truncate(req.params.webhosting_id));
    })

    router.post( "/:webhosting_id/search", function (req, res, next) {
        return req.sendPromResultAsIs(vts.QueryStats(req.params.webhosting_id, req.body.json));
    })

    router.post( "/:webhosting_id/search2", function (req, res, next) {
        return req.sendPromResultAsIs(vts.QueryStats2(req.params.webhosting_id, req.body.json));
    })

    router.post( "/search", function (req, res, next) {
        return req.sendPromResultAsIs(vts.QueryStats(null, req.body.json));
    })

    router.post( "/search2", function (req, res, next) {
        return req.sendPromResultAsIs(vts.QueryStats2(null, req.body.json));
    })

  return router

}
