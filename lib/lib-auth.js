var lib = module.exports = function(app) {

	const fs = require("MonsterDotq").fs()

	const path = require("path")
	const sshaLib = require("ssha");
	const ssha = sshaLib.ssha
	const MError = require('MonsterExpress').Error

	const authDir = app.config.get("nginx_auth_directory")

    var vali = require("MonsterValidators").ValidateJs()

    var firstLine = require("FirstLineAsJSON")



    const validators = {
             "storage_id": {presence: true, isInteger:true},
             "username": {presence: true, isString: {ascii:true, trim:true}},
             "password": {presence: true, isString: {ascii:true, trim:true}, length: {"minimum": 6}},
          }

    var re = {}


    re.OpenByFilename = function(fullfn, keepClean){

        return firstLine.ReadAsync(fullfn, function(line, originalParser){
             var recs = originalParser(line)

             if(!Array.isArray(recs))
                throw new MError("INVALID_JSON_IN_DOMAIN_FILE")

             return recs
          })
         .catch(ex=>{
            return Promise.resolve([])
         })
        .then(rows=>{

            return turnIntoObject(fullfn, rows, keepClean)
        })

    }

    re.OpenByWh = function(wh){
        // this means it exists
        var fullfn = path.join(authDir, wh.wh_id+"."+lib.authExt);
        return re.OpenByFilename(fullfn);
    }


    re.Open = function(storage_id){
    	var fullfn
    	return app.MonsterInfoWebhosting.GetInfo(storage_id)
         .then(wh=>{
            return re.OpenByWh(wh)
         })
    }

    re.ListStorages = function() {

      return fs.readdirAsync(authDir)
        .then(files=>{
            var re = []
            for(var q of files) {

              var myRegexp = new RegExp('^([0-9]+)\.'+lib.authExt+'$');
                    var match = myRegexp.exec(q);
                    if(!match) continue
                    if(match[1])
                      re.push(match[1])
            }

            return Promise.resolve(re)

        })
    }


    return re


  function turnIntoObject(fullFn, rows, keepClean) {
  	  var re = {}
  	  re.GetAccounts = function(keepCrypted){
  	  	  var re = []
  	  	  for(var q of rows) {
              var r = {"username": q.username};
              if(keepCrypted)
                r.password = q.password;
              if((keepClean)&&(q.cleanPassword))
                r.password = q.cleanPassword;
  	  	  	  re.push(r);
  	  	  }
  	  	  return re
  	  }

      re.Delete = function() {
          return fs.unlinkAsync(fullFn)
      }
      re.DeleteIfExists = function() {
          if(re.GetAccounts().length <= 0) return Promise.resolve()
          return re.Delete()
      }

  	  re.RemoveAccount = function(username) {
          var i = findUser(rows, username)
          if(i < 0)
          	 	throw new MError("ACCOUNT_NOT_EXISTS")

          rows.splice(i, 1);

  	  	  return Promise.resolve()
  	  }

      re.Truncate = function(){
        rows = [];
      }

  	  re.AddAccount = function(idata, force) {

  	  	return vali.async(idata, {username: validators.username, password: validators.password})
  	  	  .then(d=>{
  	  	  	  if(d.username.match(/:/))
  	  	  	  	throw new MError("VALIDATION_ERROR", {"username":"can't contain colon"})

              var i = findUser(rows, d.username)
              if(i >= 0) {

                if(!force)
                  throw new MError("ACCOUNT_ALREADY_EXISTS")

                rows.splice(i, 1);
              }

              if(keepClean)
                d.cleanPassword = d.password;

              if(!d.password.match(/^{S?SHA}/)) // {SHA} and {SSHA} prefixes are not transformed (needed for migration)
              	d.password = ssha.create(d.password);

              rows.push(d)

  	  	  })

  	  }
  	  re.Save = function(){

  	  	var d = "#"+JSON.stringify(rows)+"\n";
  	  	for(var q of rows) {
  	  		d += q.username + ":" + q.password +"\n"
  	  	}

        var options;
        var fileMode = app.config.get("nginx_auth_file_mode");
        var uid = app.config.get("nginx_auth_file_group_owner_uid") || 0; // crappy shit nodejs...
        var gid = app.config.get("nginx_auth_file_group_owner_gid") || 0;
        if(fileMode) {
           options = {mode: fileMode};
        }

  	  	return fs.writeFileAsync(fullFn, d, options)
          .then(()=>{
              if((uid > 0)||(gid > 0)) {
                 return fs.chownAsync(fullFn, uid, gid);
              }
          })

  	  }

  	  return re

  }

  function findUser(rows, username){
      for(var i = 0; i < rows.length; i++) {
      	var q = rows[i]
      	 if(q.username == username)
      	 	return i
      }

      return -1
  }
}


lib.authExt = "txt"
