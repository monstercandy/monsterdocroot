// permissions needed: ["EMIT_LOG","INFO_WEBHOSTING","BAN_IP"]
module.exports = function(moduleOptions) {
  moduleOptions = moduleOptions || {}
  moduleOptions.subsystem = "docrootapi"

  require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

  const CONFIG_FILE_PATTERN = "*.conf"

  const path = require('path')
  const fs = require("MonsterDotq").fs()

  var config = require('MonsterConfig');
  var me = require('MonsterExpress');
  const MError = me.Error

  for(var q of [
    "docroot_directory",

    "apache_vhosts_directory",
    "nginx_vhosts_directory",

    "nginx_redirect_directory",
    "nginx_frame_directory",
    "nginx_auth_directory",

    "tls_certificate_store_directory",
    "tls_certificate_map_directory",

    "hosting_info_directory",

    "awstats_config_directory",
  ])
  {
    var v = config.get(q)
    if(!v)
      throw new Error("Mandatory option is missing from config file: "+q);

     var s = fs.statSync(v);
     if(!s.isDirectory())
      throw new Error("Option specified for "+q+" must be a directory: "+v);

  }

  for(var q of [
    "awstats_base_config_file",
    "awstats_main_script",
    "commander_awstats_process",
    "commander_awstats_build",
    "settings_dir",
    "settings_helper_file",
    "nginx_default_accesslog_file",
    "nginx_default_errorlog_file",
    "webserver_admin_auth_file_path",
    "vts_url_prefix",
    "mrtg_url_prefix",
  ]) {
    var v = config.get(q)
    if(!v)
      throw new Error("Mandatory option is missing from config file: "+q);

  }

  config.appRestPrefix = "/docrootapi"


  config.defaults({
     cleanup: {
        "30 5 1 * *": [
          {table: "transferlog", agePrefix: "tl_", ageDays: 365, rawFilter:"tl_description='[archived]'"},
        ]
     },

    "cmd_dapache": "/usr/local/sbin/dapache",
    "cmd_dnginx": "/usr/local/sbin/dnginx",

    "apache_fastcgi_idle_timeout": 120,

    "awstats_nginx_per_domain_location_block": true,

    "vts_use_response_now": false,
  
    "debian_version": "stretch",

    "syslog_waf_enabled": true,
    "syslog_waf_whitelisted_domains": [],
    "syslog_waf_socket_path": "/var/run/mc-app-sockets/sys-syslog-waf/mc/socket",
    "syslog_waf_socket_path_in_conf": "/var/run/mc-app-sockets/sys-syslog-waf/latest/socket",
    "syslog_waf_cleanup_sec": 60,

    // 15 attempts in 15 minutes
    "syslog_waf_ban_individual_window_sec": 15*60,
    "syslog_waf_ban_individual_attempts": 15,

    // or if the storage is attacked by a distributed network,
    // the tresholds are lower (when there was 50 attempts in 15 minutes then 5 attempts from the
    // same ip is enough for a ban)
    "syslog_waf_ban_storage_all_attempts": 50,
    "syslog_waf_ban_storage_individual_attempts": 5,
    "syslog_waf_ban_storage_window_sec": 15*60,



    "webserver_admin_username": "admin",
    "webserver_admin_password_length_in_bytes": 16,
    "webserver_admin_expose_credential_max_interval_minute": 24*60,
    "cron_vts_process":"45 * * * *",
    "cron_vts_compress":"0 5 1 * *",
    "compress_vts_entries_after_days":30,
    "cron_awstats_process_and_build": "0 4 * * *",
    "cache_webhosting_info_seconds": 3600,
    "rehash_delay_seconds": 60,
    "docroot_min_path_level": 1,
    "awstats_default_language": "en",
    "html_response_for_expired": `<html><body><h3>The webstore behind this site has expired</h3>
    <div>As soon as the pending invoice is settled, the site will be re-activated automatically. Please visit back later!</div></body></html>`,
    "html_response_for_bandwidth_exceeded":`<html><body><h3>The webstore behind this site has exceeded its bandwidth limit</h3>
    <div>As soon as the bandwidth consumption is relaxed, this site will be re-activated automatically. Please visit back later!</div></body></html>`,

  })


  var app = me.Express(config, moduleOptions);
  app.enable("trust proxy")  // this application receives x-forwarded-for headers from other internal systems (typically relayer)


  var router = app.PromiseRouter(config.appRestPrefix)

  router.RegisterPublicApis(["/tasks/"])
  router.RegisterDontParseRequestBody(["/tasks/"])

  app.MonsterInfoWebhosting = require("MonsterInfo").webhosting({config: app.config, webhostingServerRouter: app.ServersRelayer})

  var commanderOptions = app.config.get("commander") || {}
  commanderOptions.routerFn = app.ExpressPromiseRouter
  app.commander = require("Commander")(commanderOptions)
  router.use("/tasks", app.commander.router)

  app.GetSettingsHelper = function(){
     const KeyValueLib = require("lib-keyvalue.js")
     return KeyValueLib({filename: app.config.get("settings_helper_file")})
  }

  const requestLib = require("RequestCommon");
  app.PostForm = function(uri, data, options){
     return requestLib.PostForm(uri, data, options);
  }

  app.webserver_systems = ["nginx", "apache"];
  for(var server of app.webserver_systems) {
      app[server] = require("lib-"+server+".js")(app)

      router.use("/"+server, require("docroot-server-router.js")(app, server))
  }


  var authRouter = require("docroot-auth-router.js")(app)
  router.use("/auths", authRouter)

  // TODO:
  // dyn app settings (here? or to the webhosting server?)

  var protectedDirsRouter = require("docroot-protected-dirs-router.js")(app)
  router.use("/protected-dirs", protectedDirsRouter)

  var docrootRouter = require("docroot-docroot-router.js")(app)
  router.use("/docroots", docrootRouter)

  var awstatsRouter = require("docroot-awstats-router.js")(app)
  router.use("/awstats", awstatsRouter)

  const redirectRouterLib = require("docroot-redirect-router.js")
  router.use("/redirects", redirectRouterLib(app, "redirect"))
  router.use("/frames", redirectRouterLib(app, "frames"))

  router.use("/cleanup", require("docroot-cleanup-router.js")(app))

  router.use("/certificates", require("docroot-certificates-router.js")(app))

  router.use("/vts", require("docroot-vts-router.js")(app))

  require("Wie")(app, router, require("lib-wie.js")(app));
  var wafRouter = require("lib-syslogd-waf.js")(app).StartWhenNeeded();
  if(wafRouter){
    router.use("/waf", wafRouter);
  }

  app.Prepare = function() {
     var knexLib = require("MonsterKnex");
     app.knex = knexLib(config);

     return app.knex.migrate.latest()
       .then(()=>{
          knexLib.installCleanup(app.knex, app.config.get("cleanup"));
       })
  }

  app.Cleanup = function() {

      var patternsToDel = []
      var dirsWithSubdirs = [
        config.get("apache_hooks_directory"),
        config.get("nginx_hooks_directory"),
        config.get("apache_vhosts_directory"),
        config.get("nginx_vhosts_directory"),
        config.get("docroot_directory"),
        config.get("settings_dir"),
      ]
      for(var q of dirsWithSubdirs) {
        patternsToDel.push(path.join(q, "**", CONFIG_FILE_PATTERN))
      }


      patternsToDel.push(path.join(config.get("tls_certificate_store_directory"), "*.key"))
      patternsToDel.push(path.join(config.get("tls_certificate_store_directory"), "*.crt"))
      patternsToDel.push(path.join(config.get("tls_certificate_map_directory"), "*.json"))

      patternsToDel.push(path.join(config.get("apache_vhosts_directory"), "default"))

      patternsToDel.push(path.join(config.get("nginx_frame_directory"), CONFIG_FILE_PATTERN))
      patternsToDel.push(path.join(config.get("nginx_redirect_directory"), CONFIG_FILE_PATTERN))
      patternsToDel.push(path.join(config.get("nginx_auth_directory"), "*."+authRouter.authExt))
      patternsToDel.push(path.join(config.get("hosting_info_directory"), "*.json"))

      patternsToDel.push(path.join(config.get("settings_helper_file"), "*.json"))

      patternsToDel.push(path.join(config.get("awstats_config_directory"), "*.conf"))

      patternsToDel.push(path.join(config.get("webserver_admin_auth_file_path")))


      const del = require('MonsterDel');
      // console.log("xxx",patternsToDel)
      return del(patternsToDel,{"force": true})
       .then(()=>{
         var fn = config.get("db").connection.filename
         if(!fn) return Promise.resolve()

         return new Promise(function(suc,rej){
            fs.unlink(fn, (err)=>{
                // the database might not exists, which is not an error in this case
                //if(err) return rej(err)

                suc()
            });
         })

       })
  }






  return app

}

