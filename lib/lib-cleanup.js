var lib = module.exports = function(app) {

  const MError = require('MonsterExpress').Error

	 const authy = require("lib-auth.js")(app)
	 const redirect = require("lib-redirect.js")(app)
	 const frames = require("lib-frames.js")(app)
	 const docroot = require("lib-docroot.js")(app)
	 const hosting = app.MonsterInfoWebhosting

     const awstats_config_dir = app.config.get("awstats_config_directory")

	 var common = require("common.js")(app)

	 var re = {}

	 re.RemoveDomain = function(domain) {

	 	 return docroot.GetWebhostingsDomains()
	 	   .then(x=>{

	 	   	   var ps = []
	 	   	   x.forEach((q)=>{
	 	   	   	   if(q.domain != domain) return;

                   var p = app.MonsterInfoWebhosting.GetInfo(q.wh)
                   	  .then(wh=>{

                   	      return docroot.GetDomain(wh, q.domain)
                   	  })
                   	  .then(d=>{
                   	  	  return d.Delete();
                   	  })

                   ps.push(p)

	 	   	   })

	 	   	   return Promise.all(ps)
	 	   })
	 	   .then(()=>{
	 	   	   return redirect.GetDomainRedirect(domain)
	 	   	     .then(d=>{
	 	   	     	return d.DeleteIfExists()
	 	   	     })
	 	   })
	 	   .then(()=>{
	 	   	   return frames.GetDomainRedirect(domain)
	 	   	     .then(d=>{
	 	   	     	return d.DeleteIfExists()
	 	   	     })
	 	   })

	 }

	 re.RemoveWebhosting = function(webhosting_id, dont_return_hosting) {

	      // 1. delete attached domains one by one (this should trigger deleting the domains, which should trigger deleting the webhosting directory)
	      // 2. delete webhosting info cache
          // 3. delete auth directory

         var wh = {}
         return hosting.Validate(webhosting_id)
           .then(aD => {
           	  wh = {"wh_id": aD.storage_id}
	 	      return docroot.GetDomainsOfWebhosting(wh)
           })
	 	   .then(domains=>{
     	 	  "use strict"


	 	   	  var ps = []

	 	   	  for(let q of domains) {
	 	   	  	 var p = docroot.GetDomain(wh, q)
	 	   	  	   .then(d=>{
	 	   	  	   	  return d.Delete()
	 	   	  	   })

	 	   	  	 ps.push(p)
	 	   	  }

	 	   	  return Promise.all(ps)
	 	   })
	 	   .then(()=>{

	 	   	  return authy.OpenByWh(wh)
	 	   	    .then(d=>{
	 	   	        return d.DeleteIfExists(webhosting_id)

	 	   	    })
	 	   })
	 	   .then(()=>{
	 	   	  return docroot.RemoveWebstoreSettings(wh);
	 	   })
	 	   .then(()=>{
	 	   	  if(dont_return_hosting) return Promise.resolve()

	 	   	  return hosting.Delete(webhosting_id)
	 	   })
	 	   .catch(enoenthandler)

	 }

	 function enoenthandler(ex){
        if(ex.code == "ENOENT")
          throw new MError("DOCROOT_NOT_FOUND")
        throw ex
	 }

     Array({n:"Frames",l:frames},{n:"Redirects",l:redirect}).forEach(c=>{
     	re["RebuildAll"+c.n] = function(){

            	return c.l.GetDomainRedirects()
		  	     .then(domains=>{
		  	     	var ps = []
		  	     	domains.forEach(domain=>{
		  	     		var p = c.l.GetDomainRedirect(domain)
		  	     		  .then(d=>{
		  	     		  	 return d.Save()
		  	     		  })
		  	     		ps.push(p)
		  	     	})

         	     	return Promise.all(ps)
         	     })

     	}
     })

	 re.RebuildAllRedirectsAndFrames = function(){

	 	return re.RebuildAllRedirects()
	 	  .then(()=>{
	 	  	 return re.RebuildAllFrames()
	 	  })

	 }

	 re.RebuildAllWebhostings = function(){

	 	return docroot.GetWebhostings()
	 	  .then(whs=>{
	 	  	 var ps = []

	 	  	 whs.forEach(wh_id=>{
                 ps.push(re.RebuildWebhosting(wh_id))
	 	  	 })

	 	  	 return Promise.all(ps)
	 	  })

	 }


	 re.RebuildWebhosting = function(webhosting_id) {

	 	console.log("doing rebuildwebhosting", webhosting_id)

         var wh
   	  	 return app.MonsterInfoWebhosting.GetInfo(webhosting_id, true)
           .then(aWh => {
           	  wh = aWh
       	 	  return docroot.GetDomainsOfWebhosting(wh)
           })
	 	   .then(domains=>{

	 	   	  var ps = []

	 	   	  domains.forEach(q => {
	 	   	  	 var p = docroot.GetDomain(wh, q)
	 	   	  	   .then(d=>{
	 	   	  	   	  return d.Save()
	 	   	  	   })

	 	   	  	 ps.push(p)
	 	   	  })

	 	   	  return Promise.all(ps)
	 	   })
	 	   .catch(enoenthandler)


	 }

	 re.Maintane = function() {

	 	 var docrootStrs
	 	 var docrootPairs
	 	 var stats = { docroot: { removed: [] }, webserver: { removed: [], created: [] }, awstats: { removed: [], created: [], flagged: []} }

	 	 return docroot.GetWebhostings()
	 	   .then((whs)=>{
	 	   	  var ps = []
	 	   	  whs.forEach((wh_id)=>{
	 	   	  	  var p = app.MonsterInfoWebhosting.GetInfo(wh_id, true)
	 	   	  	    .catch((ex)=>{

	 	   	  	    	if(ex.message == "HOSTING_NOT_FOUND") {

	 	   	  	    		stats.docroot.removed.push(wh_id)
	 	   	  	    		return re.RemoveWebhosting(wh_id, true)
	 	   	  	    	}

	 	   	  	    	throw ex
	 	   	  	    })

	 	   	  	  ps.push(p)
	 	   	  })
	 	   	  return Promise.all(ps)
	 	   })
	 	   .then(()=>{
  	 	      return docroot.GetWebhostingsDomains()
	 	   })
	 	   .then(pairs=>{
	 	      docrootStrs = common.whDomainPairsAsString(pairs)
	 	      docrootPairs = pairs


	 	      var ps = []
	 	      new Array(app.apache, app.nginx).forEach((s)=>{
	 	      	 var p = s.GetWebhostingsDomains()
	 	      	 p = p.then((pairs)=>{
 	 	   	         return comparePairs(s, pairs)
	 	      	 })

	 	      	 ps.push(p)

	 	      })

	 	   	  return Promise.all(ps)
           })
           .then(()=>{
           	   return cleanupAwstats(stats)
           })
           .then(()=>{
	 	   	  return Promise.resolve(stats)
           })


           function comparePairs(server, pairs) {
           	  var strs = common.whDomainPairsAsString(pairs)
           	  var ps = []
           	  for(var i = 0; i < strs.length; i++) {
           	  	 var q = strs[i]

           	  	 if(docrootStrs.indexOf(q) < 0) // it is present among the webserver config files but not among the docroot ones!
           	  	 {
           	  	 	var domain_name = pairs[i].domain
           	  	 	var wh_id = pairs[i].wh
           	  	 	console.log("deleting ", wh_id, domain_name)
           	  	 	var wh
           	  	 	ps.push(
           	  	 		app.MonsterInfoWebhosting.GetInfo(wh_id)
           	  	 		.then(awh=>{
           	  	 			wh = awh
           	  	 			return docroot.GetDomain(wh, domain_name)
           	  	 	    })
           	  	 	    .then(domain=>{
           	  	 			return server.Delete(wh, domain)
           	  	 	    })
           	  	 	)
   	  	 	        stats.webserver.removed.push(wh_id+"/"+domain_name+"@"+server.type)
           	  	 }

           	  }

           	  return Promise.all(ps)
           	    .then(()=>{
                      ps = [];

		           	  for(var i = 0; i < docrootStrs.length; i++) {
		           	  	 var q = docrootStrs[i]

		           	  	 if(strs.indexOf(q) < 0) // it is present among the docroot config files but not among the webserver ones!
		           	  	 {
		           	  	 	var wh
		           	  	 	var wh_id = docrootPairs[i].wh
		           	  	 	var domain_name = docrootPairs[i].domain
		           	  	 	console.log("rebuilding", wh_id, domain_name)
		           	  	 	ps.push(
		           	  	 		app.MonsterInfoWebhosting.GetInfo(wh_id)
		           	  	 		  .then(awh=>{
		           	  	 		  	wh = awh
		           	  	 		  	return docroot.GetDomain(wh, domain_name)
		           	  	 		  })
		           	  	 		  .then(domain=>{
		           	  	 		  	 return domain.assignWebappEffective()
		           	  	 		  	   .then(()=>{
                                           return server.Save(wh, domain, domain.GetGroupedEntries());
		           	  	 		  	   })
		           	  	 		  })
		           	  	 	)
		  	 	            stats.webserver.created.push(wh_id+"/"+domain_name+"@"+server.type)
		           	  	 }

		           	  }

		           	 return Promise.all(ps);

           	    })


           }

	 }


	 return re


	 function cleanupAwstats(stats) {

	 	  const path = require("path")
	 	  const fs = require("MonsterDotq").fs()

          var pathes = {}

          var existingFiles = {}

          var find = require('promise-path').find;
	      return find(path.join(awstats_config_dir,'*.conf'))
	        .then(files=>{
	        	files.map(function(q){existingFiles[path.basename(q)]=q})
	        	// console.log("4!! existing files", existingFiles)
               return docroot.GetAllWebhostingDomainObjects()
            })
            .then((all)=>{

                 var re = []
                 all.forEach(who=>{
		               who.domains.forEach(domain=>{
                         	var name = domain.GetAwstatsBasename()
                         	var d = domain.GetAll()

                         	var statName = who.wh_id+"/"+domain.GetName()

                         	var configExists = false
	                        if(existingFiles[name]) {
	                        	delete existingFiles[name]
	                        	configExists = true
	                        }

                         	if((d.awstats)&&(configExists)) return
                     		if((!d.awstats)&&(!configExists)) return

                         	if(d.awstats){
                         		// config file should exist but it does not
		                       re.push(domain.CreateAwstatsAndSaveDomain(true)
		                         .then(()=>{
		                            stats.awstats.created.push(statName)
		                         }))

                         	} else {
                         		// config file exists, but it is not marked in the object
                         		d.awstats = true
                         		re.push(
                         			domain.Save()
                         			.then(()=>{
		                              stats.awstats.flagged.push(statName)
                         		    })
                         		)
                         	}

		               })
                 })

                return Promise.all(re)
            })
            .then(()=>{
            	var ps = []
            	Object.keys(existingFiles).forEach(q=>{
            		var fullPath = existingFiles[q]
            		ps.push(
	            		fs.unlinkAsync(fullPath)
	            		  .then(()=>{
	            		  	stats.awstats.removed.push(q)
	            		  })
	            		  .catch(x=>{})
                    )
            	})

            	return Promise.all(ps)

            })

	 }

}
