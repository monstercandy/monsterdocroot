module.exports = function(app) {

    var router = app.ExpressPromiseRouter()

    var docroot = require("lib-docroot.js")(app)

    var common_docroot = require("common-docroot.js")(app, docroot)



  Array( "awstats_language" ).forEach(q=>{

    router.post("/:webhosting_id/:domain/"+q, common_docroot.getWebhostingInfoDomainExpress(function(wh,d, req,res,next){
         var m = "Set"+common_docroot.capitalize(q)
         return d[m](req.body.json)
           .then(()=>{
              return d.Save()
           })

      }))

  })

  router.post("/regenerate", function(req,res,next){
     // this one should regenerate all the awstats configuration files
     return docroot.RegenerateAwstats()
       .then(c=>{
          app.InsertEvent(req, {e_event_type: "awstats-regen"})

          req.result = c
          next()
       })
  })

  function thenReopenLogfiles(prom){
        return app.nginx.Reopen()
  }

  function getEmitter(){
     return app.commander.EventEmitter()
  }

  function processAllAwstats(params, gemitter){
       var executionPromises = []
       var emitter
       var domainsWithAwstats
        return docroot.RenameAllLogfilesForAwstatsProcessing(params.lazy)
          .then(adomainsWithAwstats =>{
             domainsWithAwstats = adomainsWithAwstats
             return Promise.resolve()
          })
          .then(thenReopenLogfiles)
          .then(()=>{

             emitter = gemitter|| getEmitter()

             var ps = []
             domainsWithAwstats.forEach(domain=>{
               var p = domain.ProcessAwstats(emitter)
                 .then(h =>{
                     var p = h.executionPromise
                       .catch(ex=>{
                          console.error("awstats process command execution failed", domain.GetName(), ex)
                       })
                     executionPromises.push(p)
                 })

               ps.push(p)
             })

             return Promise.all(ps)

          })
          .then(()=>{
               app.InsertEvent(null, {e_event_type: "awstats-processed"})

               return Promise.resolve({emitter: emitter, executionPromises:executionPromises})
          })
  }

  function buildAllAwstats(params, gemitter) {

          var executionPromises = []
          var emitter
          return docroot.GetAllDomainsWithAwstatsEnabled()
            .then(domains=>{

               emitter = gemitter|| getEmitter()

               var ps = []
               domains.forEach(domain=>{
                  ps.push(
                    domain.BuildAwstats(params, emitter)
                    .then(h=>{

                       var p = h.executionPromise
                         .catch(ex=>{
                            console.error("awstats build command execution failed for", domain.GetName(), ex)
                         })

                        executionPromises.push(p)
                    })
                  )
               })

               return Promise.all(ps)

            })
            .then(()=>{
               app.InsertEvent(null, {e_event_type: "awstats-built"})

               return Promise.resolve({emitter: emitter, executionPromises:executionPromises})
            })

  }

  // this is to process the logfiles and then build the htmls
  router.post("/process-and-build", function(req,res,next){
    return processAndBuild(req)
  })

  function processAndBuild(req){
    var gemitter = getEmitter()
    return gemitter.spawn({"omitAggregatedOutput":true})
      .then(x=>{
          if(req)
            req.sendResponse({id:x.id})
          return Promise.resolve()
      })
      .then(()=>{
        var params = req ? req.body.json : {}
        gemitter.send_stdout("starting with log processing\n")
        return processAllAwstats(params, gemitter)
          .then((x) =>{
             return Promise.all(x.executionPromises)
          })
          .then(()=>{
             gemitter.send_stdout("processing logfiles has finished, starting the build\n")
             return buildAllAwstats(params, gemitter)
          })
          .then((x) =>{
             return Promise.all(x.executionPromises)
          })
          .then(() =>{
             gemitter.send_stdout("build process has finished\n")
             gemitter.close() // everything was cool
          })
          .catch(ex=>{
             console.error("error during awstats process and build:", ex)
             gemitter.close(1)
          })

      })
  }

  function longWorkWithEmitter(req, cmd, desc) {
        var mainCommandResponse
        return cmd(req.body.json)
          .then(ax=>{
              mainCommandResponse = ax

              return mainCommandResponse.emitter.spawn({"omitAggregatedOutput":true})
          })
          .then(h=>{
              req.sendResponse({id: h.id})

              return Promise.all(mainCommandResponse.executionPromises)
                .then(()=>{
                   mainCommandResponse.emitter.close()
                })
                .catch(ex=>{
                   console.error("error during "+desc, ex)
                   mainCommandResponse.emitter.close(1)
                })

          })

  }

  // this is to process the logfiles
  router.post("/process", function(req,res,next){
      return longWorkWithEmitter(req, processAllAwstats, "awstats log processing")

  })

  // and this one is to build the html pages
  router.post("/build", function(req,res,next){
      return longWorkWithEmitter(req, buildAllAwstats, "awstats build process")

  })

  // this is to process the logfiles of this domain only
  router.post("/:webhosting_id/:domain/process", common_docroot.getWebhostingInfoDomainExpress(function(wh,d,  req,res,next){

        return d.RotateAccessLogFiles(req.body.json.lazy)
          .then(thenReopenLogfiles)
          .then(()=>{
             return d.ProcessAwstats()
          })
          .then(h=>{
             app.InsertEvent(req, {e_event_type: "awstats-processed"})
             req.result = {id: h.id}
          })

    }))

  // and this is to build html for this domain only
  router.post("/:webhosting_id/:domain/build", common_docroot.getWebhostingInfoDomainExpress(function(wh,d,  req,res,next){
         return d.BuildAwstats(req.body.json)
          .then(h=>{
             app.InsertEvent(req, {e_event_type: "awstats-built"})
             req.result = {id: h.id}
          })

    }))

  router.get("/:webhosting_id/:domain/raw", common_docroot.getWebhostingInfoDomainExpress(function(wh,d,  req,res,next){

        return d.GetAwstatsRawConfig()
          .then(data=>{
             req.result = data
          })

    }))


  router.route("/:webhosting_id/:domain")
    .post(common_docroot.getWebhostingInfoDomainExpress(function(wh,d,  req,res,next){

        return d.CreateAwstatsAndSaveDomain(req.body.json.lazy)
          .then(()=>{
             app.InsertEvent(req, {e_event_type: "awstats-create"})
          })

    }))
    .delete(common_docroot.getWebhostingInfoDomainExpress(function(wh,d,req,res,next){

        return d.DeleteAwstatsAndSaveDomain()
          .then(()=>{
             app.InsertEvent(req, {e_event_type: "awstats-delete"})
          })

    }))


  install_cron()


  return router



  function install_cron(){
    if(app.cron_awstats_process_and_build) return
    app.cron_awstats_process_and_build = true

    var csp = app.config.get("cron_awstats_process_and_build")
    if(!csp) return

    const cron = require('Croner');

    cron.schedule(csp, function(){
        return processAndBuild()
    })
  }

}
