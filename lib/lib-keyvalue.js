var lib = module.exports = function(options){
   if(!options) options = {}
   if(!options.filename) throw new Error("Option filename is missing")

   	var fs = require("MonsterDotq").fs()

    return fs.readFileAsync(options.filename)
      .catch(x=>{
      	return Promise.resolve()
      })
      .then(d=>{
      	  var re = {keys:{}}
      	  try {
      	  	re.keys = JSON.parse(d)
      	  }
      	  catch(err)
      	  {
      	  }
      	  

      	  re.Save = function() {
      	  	  return fs.writeFileAsync(options.filename, JSON.stringify(re.keys));
      	  }

      	  return Promise.resolve(re)
      })
   
}
