var lib = module.exports = function(app) {

   const UWSGI_APACHE = false


   const path = require("path")
   const fs = require("MonsterDotq").fs()
   var directory = app.config.get("apache_vhosts_directory")
   var hookdir = app.config.get("apache_hooks_directory")



   var common = require("common.js")(app)

   var re = {"type":"apache"}


   re.Save = function(webhosting, domain, helper, forceUsingFullDocrootPath, forceReturn, webstoreSettings) {

      var domain_name = domain.GetName()

      var domConf = domain.GetAll();


      // the following application types are routed here: none, host-legacy (for php only), php-fpm
      // in the other cases, nginx takes care of everything
      if((domConf.suspended)||(Array("none", "host-legacy", "php-fpm").indexOf(domConf.webappEffective.type) < 0)) {
         // this is a web application for which we do not need a config file for apache.
         return re.Delete(webhosting, domain)         
      }

      app.InsertEvent(null, {e_event_type: "docroot-regen", e_other: {"server":"apache","webhosting":webhosting.wh_id,"domain":domain_name}})

      var containerName = "";
      if(domConf.webappEffective.container)
          containerName = ", container: "+domConf.webappEffective.container;


      // console.log("foo", webhosting, domain, helper)
      // webhosting looks like: {"wh_id": storage_id, "extras":{"web_path": "/web/w3/"+storage_id+"-12345678901234567890123456"}}

      var ps = []
      var r = ""
      Object.keys(helper).forEach(docroot=>{

         var servers = helper[docroot].slice()
         var server_name = servers.shift()
         var server_alias_line = servers.length > 0 ? "ServerAlias "+servers.join(" ") : ""

          var php_block =  "";

          var php_hostport;
          if(Array("host-legacy", "php-fpm").indexOf(domConf.webappEffective.type) < 0) {
              php_block = "RemoveHandler .php\n";
          }

          if(domConf.webappEffective.type == "php-fpm") {
            var idle_timeout = app.config.get("apache_fastcgi_idle_timeout");

            if(domConf.timeout) {
               idle_timeout = domConf.timeout;
            }

            php_block = `# Docker based PHP-FPM
  FastCgiExternalServer /${server_name} -socket ${domConf.webappEffective.unix_socket_path} -idle-timeout ${idle_timeout} -pass-header Authorization
  AddHandler fastcgi/${server_name} .php
  SetNote fastcgi_chroot "${webhosting.extras.web_path}"
  SetNote fastcgi_prefix "/web"
  `
          }
          else
          if(domConf.webappEffective.type == "host-legacy") {
             const phpLib = require("MonsterPhp");
             var php_version = domConf.php_version || webhosting.wh_php_version;
             php_hostport = phpLib.phpport(php_version, webhosting.wh_id, app.config.get("php_ip_model"));             
          }

         if(php_hostport) {
            php_block = `# PHP host-legacy application, PHP version: ${php_version}
  SetNote fastcgi_chroot "${webhosting.extras.web_path}"
  SetNote /fcgi_server_php ${php_hostport.host}:${php_hostport.port}
  `
         }
         if(domConf.timeout) {
            php_block += `SetNote fastcgi_idle_timeout ${domConf.timeout}\n`
         }

         var uwsgi_block = "";

         if(UWSGI_APACHE){
            if((domConf.webappEffective.type == "host-legacy")&&(domConf.uwsgi)) {
              uwsgi_block = `# UWSGI host-legacy application
  ProxyPass / uwsgi://127.0.0.1:${domConf.uwsgi}/`;
            }

         }


         var fullPathToDocroot =
           forceUsingFullDocrootPath ?
           forceUsingFullDocrootPath :
           domain.GetFullPathForDocroot(docroot)

         var apacheErrorLog = domain.GetFullPathForApacheErrorlog();

         ps.push(re.HookFiles(webhosting, domain_name)
           .then(hm =>{


         var append_block = "";
         var base_block = "";

         if(!hm.hooks.complete) {

           if(!hm.hooks.base) {
              if(forceUsingFullDocrootPath)
                base_block +=   "  # force using this path (webhosting package has expired)\n"

              base_block +=   `  DocumentRoot "${fullPathToDocroot}"
  ServerName ${server_name}
  ${server_alias_line}
  ErrorLog ${apacheErrorLog}
  ${php_block}
  ${uwsgi_block}`


           } else {
             base_block =  `  Include "${hookdir}/${hm.hook_prefix}base.conf"`
           }

           if(hm.hooks.append) {
             append_block =  `  Include "${hookdir}/${hm.hook_prefix}append.conf"`
           }

         } else {
            base_block =  `  Include "${hookdir}/${hm.hook_prefix}complete.conf"`
         }



           r += `
<VirtualHost *>
  # webapp type: ${domConf.webappEffective.type}${containerName}
${base_block}
${append_block}
</VirtualHost>

`


           })
         )


      })


      return Promise.all(ps)
        .then(()=>{

            var p = re.Write(webhosting, domain, r, true)
            if(domConf["primary"]) {
               // lets make a parking config:

                const f = re.origGetPathes(webhosting, domain)
                p = p.then(()=>{ return re.WriteFile(null, f.file, "# this is the primary domain see ../default\n"); })

            }

            return p

        })
        .then(()=>{
           return re.RehashLater()
        })


   }

   re.RehashBase = function(){

      const cmd_dapache = app.config.get("cmd_dapache");

      return app.commander.spawn({executable: cmd_dapache, args:['-t']})
        .then((h)=>{
           return h.executionPromise
        })
        .then(()=>{
             return app.commander.spawn({executable:cmd_dapache, args:['-k','graceful']})
        })
        .then((h)=>{
           return h.executionPromise
        })
        .then(res=>{
           console.log("apache rehash result:",res.output)
        })
   }


   common.configureRehashAndDelete(re, directory, hookdir)

   re.origGetPathes = re.GetPathes

   re.GetPathes = function(webhosting, domain) {
        if(!domain.GetAll()["primary"]) return re.origGetPathes(webhosting, domain)

        const fullDir = directory
        const fullFile = path.join(fullDir, "default")
        return {dir: fullDir, file: fullFile}
     }

   return re



}
