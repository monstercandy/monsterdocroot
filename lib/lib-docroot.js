var lib = module.exports = function(app) {

   var ValidatorsLib = require("MonsterValidators")
   var vali = ValidatorsLib.ValidateJs()
   var firstLine = require("FirstLineAsJSON");

   const lockfile = require("MonsterLockfile");

  const MError = require('MonsterExpress').Error

  const dotq = require("MonsterDotq");
  const fs = dotq.fs()
  const path = require("path").posix
  const docrootDir = app.config.get("docroot_directory")
  const awstats_config_dir = app.config.get("awstats_config_directory")
  const awstats_base_config_file = app.config.get("awstats_base_config_file")
  const awstats_main_script = app.config.get("awstats_main_script")

  var mkdirpAsync = dotq.single(require("mkdirp"))

  const syslog_waf_socket_path_in_conf = app.config.get("syslog_waf_socket_path_in_conf");
  const syslog_waf_enabled = app.config.get("syslog_waf_enabled");

  const docrootServers = [app.nginx, app.apache]

  var common = require("common.js")(app)

    const validators = {
             "domain": {presence: true, isHost: true},
             "hostPrefix": {isHostWildcard: true},
             "dir": {presence: true, isPath: {lazy: true}},
             "docroot": {presence: true, isPath: {lazy: true, minPathLevels: app.config.get("docroot_min_path_level")}},
             "certificate_id": {isString: {strictName: true, emptyOk: true, lazy: true}},
             "uwsgi": {presence: true, isInteger: true, numericality: {greaterThanOrEqualTo: 10000, lessThanOrEqualTo: 50000}},
             "timeout": {presence: true, isInteger: true, numericality: {greaterThanOrEqualTo: 0, lessThanOrEqualTo: 86400}},
             "suspended": {presence: true, isBooleanLazy: true},
             "bandwidth-exceeded": {presence: true, isBooleanLazy: true},
             "primary": {presence: true, isBooleanLazy: true},
             "expired-root": {presence: true, isBooleanLazy: true},
             "bandwidth-exceeded-root": {presence: true, isBooleanLazy: true},
             "php_version": {presence:true, isMajorPhpVersion: {emptyStringAccepted: true}},
             "awstats_language": {presence: true, format: /^[a-z][a-z]$/},
             "postdata": {presence: true, isBooleanLazy: true},

             "httpsonly": {presence: true, isBooleanLazy: true},
             "hstsheaders": {presence: true, isBooleanLazy: true},
             "anticlickjackingheaders": {presence: true, isBooleanLazy: true},
             "skipnginxstaticfiles":  {presence: true, isBooleanLazy: true},
             "nginxgzip":  {presence: true, isBooleanLazy: true},
             "forceredirect":  {presence: true, isBooleanLazy: true},
             "djangostatic":  {presence: true, isBooleanLazy: true},
             "dontblockxmlrpc":  {presence: true, isBooleanLazy: true},
          }


   var re = {}

   function validateWebstoreUpgrade(in_data){
        return vali.async(in_data, {
           oldContainer: {presence:true,isString:true},
           newParams: {presence:true,isSimpleObject:true},
        })    
   }

   function upgradeContainerReferences(d, wconfigs, domains){
           const cleanupLib = require("lib-cleanup.js");
           var cleanup = cleanupLib(app);
           return dotq.linearMap({array: Object.keys(wconfigs), catch: true, action: whId=>{
              var n = wconfigs[whId];
              if((!n.webapp)||(!n.webapp.container)||(n.webapp.container != d.oldContainer)) return;

              extend(n.webapp, d.newParams);
              return re.ModifyWebstoreSettings({wh_id: whId}, function(data){
                 data.webapp = n.webapp;
              })
              .then(()=>{
                 return cleanup.RebuildWebhosting(whId);
              })
           }})
           .then(()=>{
               return dotq.linearMap({array: domains, catch: true, action: domain => {
                  var all = domain.GetAll();
                  if((!all.webapp)||(!all.webapp.container)||(all.webapp.container != d.oldContainer)) return;

                  extend(all.webapp, d.newParams);
                  return domain.Save();
               }})
           })
   }

   function detachContainerReferences(d, wconfigs, domains){
           const cleanupLib = require("lib-cleanup.js");
           var cleanup = cleanupLib(app);
           return dotq.linearMap({array: Object.keys(wconfigs), catch: true, action: whId=>{
              var n = wconfigs[whId];
              if((!n.webapp)||(!n.webapp.container)||(n.webapp.container != d.oldContainer)) return;

              // so this was a webstore default application. It needs to be disabled.
              n.webapp = { type: "none" };

              return re.ModifyWebstoreSettings({wh_id: whId}, function(data){
                 data.webapp = n.webapp;
              })
              .then(()=>{
                 return cleanup.RebuildWebhosting(whId);
              })
           }})
           .then(()=>{
               return dotq.linearMap({array: domains, catch: true, action: domain => {
                  var all = domain.GetAll();
                  if((!all.webapp)||(!all.webapp.container)||(all.webapp.container != d.oldContainer)) return;

                  // so this was a vhost bound webapp container. we need to fall back to webstore default.

                  all.webapp = {type: "default"};
                  return domain.Save();
               }})
           })
   }

   re.UpgradeWebappContainerReferencesForWebstore = function(wh_id, in_data){
        var d;
        var webhosting;
        var wconfigs = {};
        return validateWebstoreUpgrade(in_data)
         .then(ad=>{
            d = ad;
            return app.MonsterInfoWebhosting.GetInfo(wh_id);
         })
         .then(awebhosting=>{
            webhosting = awebhosting;
            return re.GetWebstoreSettings(webhosting);
         })
         .then(awconfig=>{
            wconfigs[""+wh_id] = awconfig;
            return re.GetDomainObjectsOfWebhosting(webhosting)
              .catch(ex=>{
                 if(ex.code == "ENOENT") {
                   // empty store...
                   return [];
                 }

                 throw ex;
              })
         })
         .then(domains=>{
            // console.log("SHHII", domains, wconfigs, wh_id)
            return upgradeContainerReferences(d, wconfigs, domains);
         })
   }

   re.UpgradeWebappContainerReferences = function(in_data){
        var d;
        var wconfigs;
        return validateWebstoreUpgrade(in_data)
        .then(ad=>{
           d = ad;
           return re.GetAllWebstoreConfigs();
        })
        .then(awconfigs=>{
           // console.log("XXXXXXXX", awconfigs)
           wconfigs = awconfigs;
           return re.GetAllDomains();
        })
        .then(domains=>{
           return upgradeContainerReferences(d, wconfigs, domains);
        })
   }
   re.DetachWebappContainerReferences = function(in_data){
        var d;
        var wconfigs;
        return vali.async(in_data, {oldContainer: {presence: true, isString: true}})
        .then(ad=>{
           d = ad;
           return re.GetAllWebstoreConfigs();
        })
        .then(awconfigs=>{
           // console.log("XXXXXXXX", awconfigs)
           wconfigs = awconfigs;
           return re.GetAllDomains();
        })
        .then(domains=>{
           return detachContainerReferences(d, wconfigs, domains);
        })
   }

   re.GetAllWebstoreConfigs = function(){
      return fs.readdirAsync(app.config.get("settings_dir"))
        .then(files=>{
            var are = {};
            var r = new RegExp('^([0-9]+)\.');
            return dotq.linearMap({array: files, action: x => {
               var m = r.exec(x);
               if(!m) return;

               var wh_id = m[1];
               return re.GetWebstoreSettings({wh_id: wh_id})
                 .then(r=>{
                    are[wh_id] = r;
                 })
            }})
            .then(()=>{
               return are;
            })
        })
   }

   re.SetBandwidthExceededForWebhosting = function(whId, newState){
      // console.log("SetBandwidthExceededForWebhosting", whId, newState);
      return app.MonsterInfoWebhosting.GetInfo(whId)
        .then(wh=>{
           return re.GetDomainObjectsOfWebhosting(wh);
        })
        .then(domains=>{
            return dotq.linearMap({array:domains, action:function(domain){
               return domain.SetBandwidthexceeded({"bandwidth-exceeded": newState})
                .then(()=>{
                  return domain.Save();
                })
            }})
        })
   }

   re.getDomainsForCertificate = function(cert_id, full) {
     var d = getCertificateMapDirectory(cert_id)
     return fs.readdirAsync(d)
       .then(files=>{
          var re = []
          var r = new RegExp('(.+)\.json')
          files.forEach(x=>{
             var m = r.exec(x)
             if(!m) return
             re.push(m[1])
          })
          return Promise.resolve(re)
       })
       .then(fileNames=>{
          if(!full) return Promise.resolve(fileNames)
          var ps = []
          var re = []
          fileNames.forEach(filename=>{
             ps.push(
                fs.readFileAsync(path.join(d, filename+".json"), "utf-8")
                  .then(c=>{
                     re.push(JSON.parse(c))
                  })
             )
          })
          return Promise.all(ps)
            .then(()=>{
               return Promise.resolve(re)
            })
       })
       .catch(ex=>{
          console.error("Unable to list certs", d, ex)
          return Promise.resolve([])
       })
   }

   function getDocrootOfSomething(category) {
      var s
      return app.GetSettingsHelper()
        .then(as=>{
            s = as

            var er = s.keys[category]
            if(!er) return Promise.resolve() // it was not found

            return app.MonsterInfoWebhosting.GetInfo(er.wh)
              .then(wh=>{
                 return re.GetDomain(wh, er.domain)
              })
              .then(domain=>{
                  var e = domain.GetEntries()
                  var x
                  if((e)&&(e.length)) x = domain.GetFullPathForDocroot(e[0].docroot)
                  return Promise.resolve(x)
              })

        })
   }

   re.GetExpiredRootDocroot = function(){
      return getDocrootOfSomething("expired-root");
   }
   re.GetBandwidthExceededDocroot = function(){
      return getDocrootOfSomething("bandwidth-exceeded-root");
   }

   re.GetDomainsOfWebhosting = function(webhosting) {
      return fs.readdirAsync(re.getPathOfWebhosting(webhosting))
        .then(files=>{
            var re = []
            for(var q of files) {

              var myRegexp = new RegExp('^(.+)\.conf$');
              var match = myRegexp.exec(q);
              if(!match) continue
              if(match[1])
                re.push(match[1])
            }

            return Promise.resolve(re)

        })

   }
   re.GetDomainObjectsOfWebhosting = function(webhosting){
          var realDomains = []
          return re.GetDomainsOfWebhosting(webhosting)
            .then((domains)=>{
                var ps = []
                domains.forEach(domain=>{
                    var p = re.GetDomain(webhosting, domain)
                      .then(domain=>{
                         realDomains.push(domain)
                      })
                    ps.push(p)
                })
                return Promise.all(ps)
            })
            .then(()=>{
               return Promise.resolve(realDomains)
            })

   }

   re.GetWebhostingObjects = function(){
       var whos = []
       return re.GetWebhostings()
         .then(whs=>{

            var ps = []
            whs.forEach(wh=>{
                var p = app.MonsterInfoWebhosting.GetInfo(wh)
                  .then(who=>{
                     whos.push(who)
                  })
                ps.push(p)
            })

            return Promise.all(ps)
         })
         .then(()=>{
            return Promise.resolve(whos)
         })
   }

   re.GetAllWebhostingDomainObjects = function(){
          var all = []
          return re.GetWebhostingObjects()
            .then((whos)=>{
              var ps = []

              whos.forEach(who=>{
                 all.push(who)
                 var p = re.GetDomainObjectsOfWebhosting(who)
                   .then(d=>{
                      who.domains = d
                   })

                 ps.push(p)

              })

              return Promise.all(ps)
            })
            .then(()=>{
               return Promise.resolve(all)
            })

   }

   re.GetHostEntriesOfWebhosting = function(webhosting){
          return re.GetDomainObjectsOfWebhosting(webhosting)
            .then((domains)=>{
                var re = []

                   domains.forEach(domain=>{
                       var entries = domain.GetEntries()
                       entries.forEach(entry=>{
                          re.push({"raw": domain.GetName() + "/" + entry.host, "domain": domain.GetName(), "host": entry.host})
                       })

                   })

                return Promise.resolve(re)

            })

   }

   re.GetAllDomains = function(){
      var mre = []
      return re.GetAllWebhostingDomainObjects()
        .then((all)=>{
            var re = []
            all.forEach(who=>{

               who.domains.forEach(domain=>{

                   mre.push(domain)
               })

            })

            return Promise.all(mre)

        })

   }

   re.GetAllDomainsWithAwstatsEnabled = function(alldomains_prefetched){
      var mre = []
      var p = alldomains_prefetched ? Promise.resolve(alldomains_prefetched) : re.GetAllDomains()
      return p.then(alldomains=>{
            alldomains.forEach(domain=>{
                   var conf = domain.GetAll()
                   if(!conf["awstats"]) return

                   mre.push(domain)
            })

            return Promise.resolve(mre)
        })

   }

   re.RenameAllLogfilesForAwstatsProcessing = function(lazy) {
      var alldomains
      return re.GetAllDomains()
        .then(aalldomains=>{
            alldomains = aalldomains

               var ps = []
               alldomains.forEach(domain=>{
                  ps.push(
                    domain.RotateAccessLogFiles(lazy)
                    .catch(ex=>{
                        if(ex.code != "ENOENT")
                           console.error("unable to rename logfile for", domain.GetName(), ex)
                    })
                  )
               })

               return Promise.all(ps)


        })
        .then(()=>{
            return re.GetAllDomainsWithAwstatsEnabled(alldomains)
        })
   }

   re.RegenerateAwstats = function(){
          var counter = 0
          return re.GetAllDomainsWithAwstatsEnabled()
            .then((domains)=>{
                var re = []

                domains.forEach(domain=>{

                     re.push(domain.CreateAwstatsAndSaveDomain(true)
                       .then(()=>{
                          counter++
                       }))

                })

                return Promise.all(re)
            })
            .then(()=>{
               return Promise.resolve(counter)
            })

   }

   re.GetDistinctCompletePathes = function(){

          return re.GetAllWebhostingDomainObjects()
            .then((all)=>{
                var re = {}
                all.forEach(who=>{

                   who.domains.forEach(domain=>{
                       var entries = domain.GetEntries()
                       entries.forEach(entry=>{
                          var fp = domain.GetFullPathForDocroot(entry.docroot)
                          if(!re[fp]) re[fp] = []
                          re[fp].push(entry.host)
                       })

                   })

                })
                return Promise.resolve(re)

            })
  }
  function getWebstoreConfigPath(webhosting){
     var dir = app.config.get("settings_dir");
     var fullFn = path.join(dir, webhosting.wh_id+".conf");
     return fullFn;    
  }
  function validateWebapp(in_data, dontAcceptDefault){
     var validTypes = ["none", "host-legacy", "php-fpm", "fastcgi", "webserver-http", "webserver-unix", "uwsgi"];
     if(!dontAcceptDefault) validTypes.push("default");
     var d = {};
     return vali.async(in_data, {
        type: {presence: true, inclusion: validTypes}
     })
     .then(ad=>{
        extend(d, ad);
        if(Array("none", "host-legacy", "default").indexOf(d.type) > -1)
           return;

        return vali.async(in_data, {container: {presence: true, isString: {strictName: true}}}) // we need to remember this so that upgrade operations will find it
             .then(ad=>{
                extend(d, ad);

                switch(d.type) {
                  case "php-fpm":
                  case "fastcgi":
                  case "uwsgi":
                  case "webserver-unix":
                    return vali.async(in_data, {
                      unix_socket_path: {presence: true, isString: true, isPath: {stripTailingSlash: true}}
                    })

                  // note: we've already got all the info we need for webserver-http type webapps
                  // we will just proxy the requests there on port 8080
                }

             })
             .then(ad=>{
                extend(d, ad);              
             })

     })
     .then(()=>{
        return d;
     })
  }
  re.RemoveWebstoreSettings = function(webhosting){
     var fullFn = getWebstoreConfigPath(webhosting);
     return fs.ensureRemovedAsync(fullFn);
  }
  re.GetWebstoreSettings = function(webhosting){
     var fullFn = getWebstoreConfigPath(webhosting);
     return firstLine.ReadAsync(fullFn)
       .catch(ex=>{
          if(ex.code != "ENOENT") throw ex;
          return {};
       })
       .then((config)=>{
          if(!config.webapp) config.webapp = {type: "host-legacy"};
          return config;
       })
  }
  re.ModifyWebstoreSettings = function(webhosting, cb){
     var fullFn = getWebstoreConfigPath(webhosting);
     return lockfile.logic({lockfile:"docroot-"+webhosting.wh_id, callback: function(lockfilePath){
        var data;
        return re.GetWebstoreSettings(webhosting)
         .then(adata=>{
            data = adata;
            return cb(data);
         })
         .then(()=>{
            return fs.writeFileAsync(fullFn, "#"+JSON.stringify(data));
         })
     }});
  }
  re.GetWebAppsOfWebhosting = function(webhosting){
      var are = {vhosts:{}};
      return re.GetWebstoreSettings(webhosting)
        .then(ws=>{
           are.default = ws.webapp;
           return re.GetDomainObjectsOfWebhosting(webhosting);
        })
        .catch(ex=>{
           if(ex.code != "ENOENT")
             throw ex;

           return [];
        }) 
        .then(domains=>{
            domains.forEach(domain=>{
               var name = domain.GetName();
               var d = domain.GetAll();
               are.vhosts[name] = d.webapp;
            });
            
            return are;
        })        
   }
   re.SetDefaultWebAppOfWebhosting = function(webhosting, in_data) {
      return validateWebapp(in_data, true)
        .then(d=>{
           return re.ModifyWebstoreSettings(webhosting, function(data){
              data.webapp = d;
           })
        })
        .then(()=>{
          return re.GetDomainObjectsOfWebhosting(webhosting)
            .catch(ex=>{
              if(ex.code != "ENOENT")
                throw ex;
              return [];
            })
        })
        .then((realDomains)=>{
           return dotq.linearMap({array: realDomains, action: function(domain){
              return domain.Save();
           }})
        })
   }
   re.GetDistinctCompleteLogdirs = function(){

          var logdirRe = {}
          return re.GetAllWebhostingDomainObjects()
            .then((all)=>{

                all.forEach(who=>{

                   who.domains.forEach(domain=>{
                       var fp = domain.GetFullPathForLogdir()
                       if(!logdirRe[fp]) logdirRe[fp] = []
                       logdirRe[fp].push(domain.GetName())

                   })

                })

                return Promise.resolve(logdirRe)

            })
  }
   re.GetDistinctDocrootsOfWebhosting = function(webhosting){

          return re.GetDomainObjectsOfWebhosting(webhosting)
            .then((realDomains)=>{
                var re = {}
                realDomains.forEach(domain=>{
                   var entries = domain.GetEntries()
                   entries.forEach(entry=>{
                      if(!re[entry.docroot]) re[entry.docroot] = []
                      re[entry.docroot].push(entry.host)
                   })
                })
                return Promise.resolve(re)

            })
  }
   re.GetDocrootsMappedByCertificate = function(webhosting){

          return re.GetDomainObjectsOfWebhosting(webhosting)
            .then((realDomains)=>{
                var re = {}
                realDomains.forEach(domain=>{
                   var cert_id = domain.GetAll().certificate_id || ""
                   if(!re[cert_id]) re[cert_id] = []
                   re[cert_id].push(domain.GetName())
                })
                return Promise.resolve(re)

            })
  }

   re.GetHostDocrootMappingOfWebhosting = function(webhosting){

          return re.GetDomainObjectsOfWebhosting(webhosting)
            .catch(ex=>{
              if(ex.code != "ENOENT")
                throw ex;
              return [];
            })
            .then((realDomains)=>{
                var re = {}
                realDomains.forEach(domain=>{
                   var entries = domain.GetEntries()
                   entries.forEach(entry=>{
                      re[entry.host] = entry.docroot
                   })
                })
                return Promise.resolve(re)

            })

   }

   re.GetWebhostingsDomains = function() {
      return common.getConfFiles(docrootDir)
   }


   re.GetWebhostings = function() {

      return re.GetWebhostingsDomains()
        .then(pairs=>{
           var re = {}

           for(var q of pairs) {
              re[q.wh] = 1
           }

           return Promise.resolve(Object.keys(re))
        })

   }

   re.getPathOfWebhosting = function(webhosting) {
      return path.join(docrootDir, ""+webhosting.wh_id)
   }

   re.detachCertificate = function(cert_id){
      return re.getDomainsForCertificate(cert_id, true)
        .then(domains=>{
            var ps = []
            domains.forEach(dom=>{
               ps.push(
                  re.GetDomain(dom.webhosting, dom.domain)
                    .then(d=>{
                       return d.SetCertificate("")
                          .then(()=>{
                              return d.Save()
                          })

                    })
               )
            })
            return Promise.all(ps)

        })
   }

   re.GetDomain = function(webhosting, domain) {
      var fullFn
      var fullDir
      var canonDomain

      function adjustPathes(){
          fullDir = re.getPathOfWebhosting(webhosting)
          fullFn = path.join(fullDir, canonDomain+".conf")        
      }

      return vali.async({domain: domain}, {domain: validators.domain})
        .then(d=>{
          canonDomain= d.domain.d_host_canonical_name

          adjustPathes();

          return firstLine.ReadAsync(fullFn, function(line, originalParser){
             var recs = originalParser(line)

             if(!vali.isObject(recs))
                throw new MError("INVALID_JSON_IN_DOMAIN_FILE")

             return recs
          }).catch(ex=>{
            return Promise.resolve({"entries":[]})
          })

        })
        .then(d=>{

          if((!d["protected-dirs"])||(!Array.isArray(d["protected-dirs"]))) d["protected-dirs"] = []
          if(!d.webapp) d.webapp = {type:"default"};

          d["awstats_allowed"] = (webhosting.template && webhosting.template.t_awstats_allowed) ? true : false

          var dre = {}

          dre.GetWebhosting = function(){
            return webhosting
          }

          dre.GetFullPathForLogfiles = function(skipStat) {
             var logdirShouldbe = dre.GetFullPathForLogdir()

             var canon = dre.GetName()

             var p = skipStat ? Promise.resolve({isDirectory: function(){return true}}) : fs.statAsync(logdirShouldbe)
             p = p.then((s)=>{
                  // resolving succeeded
                  if(!s.isDirectory())
                    throw new Error("Not a directory")
                  var re = {
                      logdir: logdirShouldbe,
                      access_for_awstats: path.join(logdirShouldbe, canon+".access.log"),
                      access: path.join(logdirShouldbe, canon+".access.active.log"),
                      post: path.join(logdirShouldbe, canon+".postdata.log"),
                      error: path.join(logdirShouldbe, canon+"-nginx-error.log"),
                  };
                  if(syslog_waf_enabled) {
                      re.syslog_waf_socket_path_in_conf = syslog_waf_socket_path_in_conf;
                  }
                  return Promise.resolve(re);
               })
               .catch((ex)=>{
                  console.error("logdir does not exist for ", canon, logdirShouldbe, ex)
                  var default_accesslog = app.config.get("nginx_default_accesslog_file")
                  var default_errorlog = app.config.get("nginx_default_errorlog_file")
                  return Promise.resolve({access: default_accesslog, error: default_errorlog})
               })

             return p

          }

          dre.RotateAccessLogFiles = function(lazy) {

             return dre.GetFullPathForLogfiles(lazy)
               .then(pathes=>{
                  if(!pathes.access_for_awstats) throw new MError("NO_LOG_DIR_FOUND")

                  return fs.renameAsync(pathes.access, pathes.access_for_awstats)
                    .catch(ex=>{
                        if(ex.code != "ENOENT")
                           console.error("cannot rename logfile", pathes.access, pathes.access_for_awstats, ex)
                        if(lazy) return
                        throw ex
                    })
               })

          }

          dre.ProcessAwstats = function(emitter) {
             if(!d["awstats"]) return Promise.reject(new MError("AWSTATS_NOT_ENABLED"))

             var params = {
                config_name: dre.GetAwstatsConfigname(),
                awstats_main_script: awstats_main_script,
             }

            return app.commander.spawn({"omitAggregatedOutput":true, emitter: emitter, removeImmediately: true, chain: app.config.get("commander_awstats_process")}, params)
          }

          dre.GetAwstatsDirectoryDomainRoot = function(){
                return path.join(webhosting.extras.web_path, "awstats", dre.GetName());
          }

          function getAwstatsDirectory(params){
                const replacer = require("Replacer");
                var dstDirTemplate = app.config.get("awstats_html_output_dir") || path.join(dre.GetAwstatsDirectoryDomainRoot(),"[ym]");
                return replacer(dstDirTemplate, params);            
          }

          dre.BuildAwstats = function(in_params, emitter) {
              if(!d["awstats"]) return Promise.reject(new MError("AWSTATS_NOT_ENABLED"))

              var params
              const moment = require("MonsterMoment")()
              return vali.async(in_params, {
                 year: {format: /^\d\d\d\d$/, default: moment.format("YYYY")},
                 month: {format: /^\d\d$/, default: moment.format("MM")},
                 lang: {format: /^[a-z][a-z]$/, default: d["awstats_language"] || app.config.get("awstats_default_language")}
              })
              .then(aparams=>{
                  params = aparams

                  params.domain = dre.GetName()
                  params.wh_secretcode = webhosting.wh_secretcode

                  params.awstats_main_script = awstats_main_script

                  params.config_name= dre.GetAwstatsConfigname()

                  params.ym = ""+params.year+params.month

                  if(params.ym.length != 6) return Promise.reject(new MError("INVALID_YEAR_MONTH"))

                  params.awstats_html_output_dir = getAwstatsDirectory(params);

                  return mkdirpAsync(params.awstats_html_output_dir)
                    .catch(ex=>{
                      if((ex.code)&&(ex.code == 'EEXIST'))
                         return;
                      console.error("error creating directory", params.awstats_html_output_dir, ex)
                      throw ex
                    })
              })
              .then(()=>{
                 return app.commander.spawn({"omitAggregatedOutput":true, removeImmediately: true, emitter: emitter, chain: app.config.get("commander_awstats_build")}, params)
              })

          }

          dre.SetWebAppOfWebhostingDomain = function(in_data){
              return validateWebapp(in_data)
                .then((ad)=>{
                   d.webapp = ad;
                   return dre.Save();
                })
          }


          dre.GetAwstatsConfigname = function(){
             return dre.GetName()+"-"+webhosting.wh_secretcode
          }
          dre.GetAwstatsBasename = function(){
             return "awstats."+dre.GetAwstatsConfigname()+".conf"
          }
          dre.GetAwstatsFilename = function(){
             return path.join(awstats_config_dir, dre.GetAwstatsBasename())
          }
          dre.GetAwstatsRawConfig = function(){
             var awstatsFilename = dre.GetAwstatsFilename()
             return fs.readFileAsync(awstatsFilename, "utf8")
          }

          dre.CreateAwstatsAndSaveDomain = function(lazy, force){
             var awstatsFilename = dre.GetAwstatsFilename()
             var mainDomain = dre.GetName()
             var hosts = dre.GetHosts().join(" ")

             if(!d["awstats_allowed"]) return Promise.reject(new MError("AWSTATS_NOT_ALLOWED"))

             return dre.GetFullPathForLogfiles(lazy)
               .then(pathes=>{
                   if(!pathes.logdir) throw new MError("LOGDIR_NOT_CONFIGURED")

                   var awstatsContent = `
DirData="${pathes.logdir}"
LogFile="${pathes.access_for_awstats}"
SiteDomain="${mainDomain}"
HostAliases="${hosts}"
Include "${awstats_base_config_file}"
`

                  return fs.writeFileAsync(awstatsFilename, awstatsContent)

               })
                .then(()=>{
                    d["awstats"] = true
                    return dre.Save(false, false)
                })
          }
          dre.DeleteAwstatsIfExists = function(){
             var awstatsFilename = dre.GetAwstatsFilename()
             return fs.unlinkAsync(awstatsFilename)
               .catch(ex=>{
                  console.error("cannot delete awstats config file", awstatsFilename, ex)
               })
          }
          dre.DeleteAwstatsAndSaveDomain = function(){
             return dre.DeleteAwstatsIfExists()
                .then(()=>{
                    d["awstats"] = false
                    return dre.Save(false, true)
                })
          }

          dre.GetFullPathForLogdir = function() {
             return path.join(webhosting.extras.web_path,"weblogs")
          }
          dre.GetFullPathForApacheErrorlog = function(){
            var name = dre.GetName();
            return path.join(webhosting.extras.web_path, "weblogs", `${name}-apache-error.log`);
          }
          dre.GetFullPathForDocroot = function(docroot) {
             return `${webhosting.extras.web_path}${docroot}`
          }
          dre.GetName = function(){
             return canonDomain
          }
          dre.GetRaw = function(){
            return fs.readFileAsync(fullFn, "utf8")
          }
          dre.GetAll = function(){
            return d
          }
          dre.SetAll = function(newDetails){
            d = newDetails;
          }
          dre.GetEntries = function(){
            return d.entries
          }
          dre.Delete = function() {
            return dre.SetCertificate({})
              .then(()=>{
                return dre.DeleteAwstatsIfExists()
              })
              .then(()=>{
                 return docrootCall("Delete", webhosting, dre)
              })
              .then(()=>{

                 return fs.ensureRemovedAsync(fullFn)
              })
              .then(()=>{
                 // we attempt to remove the containing directory as well
                 return fs.rmdirAsync(fullDir).catch(ex=>{})
              })

          }
          function setFlagLogic(ad, key) {
              var current = d[key];
              var proposed = ad[key];
              if(current == proposed)
                return "unchanged";

              if(!ad[key]) delete d[key]; else d[key] = ad[key]
              return "ok";
          }

          function finisher(ad, key) {
              setFlagLogic(ad, key);

              return app.GetSettingsHelper()
                .then(s=>{
                   s.keys[key] = {wh: webhosting.wh_id, domain: canonDomain}

                   return s.Save()
                })
                .then(()=>{
                   return Promise.resolve("ok")
                })

          }
          function valiHelper(p,key) {
             var vs = {}
             vs[key] = validators[key]
             return vali.async(p, vs)
          }
          dre.SetCertificate = function(p){
             function getCertFile(cert_id){
                var dir = getCertificateMapDirectory(cert_id)
                var file = path.join(dir, dre.GetName()+".json")
                return {dir:dir, file:file}
             }


             var key = "certificate_id"
             var validParams
             return valiHelper(p, key)
               .then(ad=>{
                    validParams = ad

                    var current = dre.GetAll()
                    // console.log("we are here", current)

                    if(current.certificate_id) {
                      // there was an old one attached already
                      var r = getCertFile(current.certificate_id)

                      return fs.unlinkAsync(r.file)
                        .then(()=>{
                           return fs.rmdirAsync(r.dir)
                        })
                        .catch(ex=>{
                           console.error("Unable to remove old certificate maps", r, ex)
                        })

                    }

                    return Promise.resolve()
               })
               .then(()=>{

                    // in case of detaching, map is not needed to be written
                    if(!validParams.certificate_id) return Promise.resolve()

                    var r = getCertFile(validParams.certificate_id)
                    return fs.mkdirAsync(r.dir)
                      .catch(ex=>{
                         console.error("Unable to create directory", r, ex)
                         return Promise.resolve()
                      })
                      .then(()=>{
                         return fs.writeFileAsync(r.file, JSON.stringify({domain:dre.GetName(),webhosting:webhosting}))
                      })
               })
               .then(()=>{
                    return setFlagLogic(validParams, key);
               })
          }

         dre.SetPhp = function(p){
             var key = "php_version"
             return valiHelper(p, key)
               .then(ad=>{
                    return setFlagLogic(ad, key)
               })
          }

          Array(
             "Uwsgi", "Awstats_language", "Timeout", "Httpsonly", "Hstsheaders", 
             "Anticlickjackingheaders", "Skipnginxstaticfiles", "Nginxgzip", 
             "Djangostatic", "Forceredirect", "Dontblockxmlrpc"
          ).forEach(method=>{
                dre["Set"+method] = function(p){
                   var key = method.toLowerCase()
                   return valiHelper(p, key)
                     .then(ad=>{
                          return setFlagLogic(ad, key)
                     })
                }              
            })


          function removeFlagInOthers(methodName, ad, key) {

                   // this is the easier one. the domain was expired root and it should not be anymore.
                   if(!ad[key])
                      return Promise.resolve()

                   // this is the harder one, we should enumerate over all the domains and remove the current primary
                   return re.GetWebhostingsDomains()
                     .then(pairs=>{

                         var ps = []


                         pairs.forEach(p=>{

                              var wh
                              var domain
                              ps.push(
                                app.MonsterInfoWebhosting.GetInfo(p.wh)
                                .then(awh=>{
                                    wh = awh
                                    return re.GetDomain(wh, p.domain)
                                })
                                .then(adomain=>{
                                  domain = adomain
                                  var d = {}
                                  d[key] = false
                                  return domain[methodName](d)
                                })
                                .then(waschange=>{
                                   if(waschange != "ok") return Promise.resolve()

                                   return domain.Save()
                                })
                              )

                         })

                         return Promise.all(ps)
                     })

          }

          function setRootForSomething(p, key, method, callback){
             var ad
             return valiHelper(p, key)
               .then(aad=>{
                   ad = aad
                   if(((!ad[key])&&(!d[key])) || ((ad[key])&&(d[key]))) return Promise.resolve() // no changes are neccessary


                   if(callback) return callback();

                   return Promise.resolve();
               })
               .then(()=>{

                   return removeFlagInOthers(method, ad, key)
               })
               .then(()=>{
                   console.log("setting "+method+" for", ad)
                   return finisher(ad, key)
               })

          }

          dre.SetBandwidthExceededroot = function(p){
             return setRootForSomething(p, "bandwidth-exceeded-root", "SetBandwidthExceededroot");
          }

          dre.SetExpiredroot = function(p){
             return setRootForSomething(p, "expired-root", "SetExpiredroot");
          }

          dre.SetPrimary = function(p){

             return setRootForSomething(p, "primary", "SetPrimary", function(){
                   // either way, the current apache config must go (it will be placed under new location after resaving)
                   return app.apache.Delete(webhosting, dre);
             })
          }

          dre.SetPostdata = function(p){
             var key = "postdata"
             return valiHelper(p, key)
               .then(ad=>{
                    return setFlagLogic(ad, key)
               })
          }

          dre.SetBandwidthexceeded = function(p){
             var key = "bandwidth-exceeded"
             return valiHelper(p, key)
               .then((ad)=>{
                    return setFlagLogic(ad, key)
               })
               .then(x=>{
                  if(x == "ok")
                    app.InsertEvent(null, {e_event_type: p["bandwidth-exceeded"] ? "vhost-bandwidth-limit-exceeded": "vhost-bandwidth-limit-relaxed", e_other: {domain: dre.GetName()}})

                  return x;
               })
          }
          dre.SetSuspend = function(p){
             var key = "suspended"
             var ad
             return valiHelper(p, key)
               .then(aad=>{
                  ad = aad

                  var ps = []

                  if(!ad[key]){
                         // we ensure here that there would be no collisions after reenablign the domain
                         console.log("ensuring there would be no collision after reenabling ", dre.GetName())

                         const containsLib = require("lib-contains-already.js")

                         dre.GetEntries().forEach(e=>{
                            console.log("checking ", e)
                            ps.push( containsLib(docrootDir, "xxxxxxxxxxxxxxxxxx.xxx", e.host) )
                         })

                  }


                  return Promise.all(ps)
               })
               .then(()=>{
                    return setFlagLogic(ad, key)
               })
          }

          dre.GetProtectedDirs = function(){
            return d["protected-dirs"]
          }

          function protectedDirCommon(dir) {
              return vali.async({dir: dir}, {dir: validators.dir})

          }
          dre.AddProtectedDir = function(dir) {
            return protectedDirCommon(dir)
                .then(v=>{


                    if(d["protected-dirs"].indexOf(v.dir) >= 0)
                      throw new MError("ALREADY_PRESENT")

                    d["protected-dirs"].push(v.dir)
                })
          }
          dre.DelProtectedDir = function(dir) {
            return protectedDirCommon(dir)
                .then(v=>{
                    var i = d["protected-dirs"].indexOf(v.dir)
                    if(i < 0)
                      throw new MError("PROTECTED_DIR_NOT_FOUND")

                    d["protected-dirs"].splice(i)

                })
          }

          dre.DelAllProtectedDirs = function() {
            return Promise.resolve()
              .then(()=>{
                 d["protected-dirs"] = [];                
              })
          }

          dre.RemoveDocroot = function(host, data) {

            return Promise.resolve()
              .then(()=>{
                    var hostPrefix = common.hostToPrefixBasedOnDomain(host, canonDomain, data.force)
                    return vali.async({"host": hostPrefix}, {"host":validators.hostPrefix})

              })
              .then(d=>{
                  if(!removeHost(host))
                    throw new MError("HOST_NOT_FOUND")

                  return Promise.resolve()
              })
          }

          dre.AddDocroot = function(host, data) {

             var d

             return Promise.resolve()
                 .then(()=>{
                      data.hostPrefix = common.hostToPrefixBasedOnDomain(host, canonDomain, data.force)
                      // console.log("validating", host, canonDomain, data)

                      return vali.async(data, {"hostPrefix":validators.hostPrefix, "docroot": validators.docroot})

                 })
              .then(ad=>{
                    d = ad
                    const containsLib = require("lib-contains-already.js")
                    // console.log("calling containslib", docrootDir, canonDomain, host)
                    return containsLib(docrootDir, canonDomain, host, webhosting.wh_id);
              })
              .then(()=>{
                  removeHost(host)
                  dre.GetEntries().push({"host": host, "docroot": d.docroot})

                  return Promise.resolve()
              })
          }


          dre.GetFirstDocroot = function() {
             var d = dre.GetEntries()
             for(var q of d) {
                return q.docroot
             }
          }
          dre.GetHosts = function() {
             var d = dre.GetEntries()
             var re = {}
             for(var q of d) {
                re[q.host] = 1
             }
             return Object.keys(re)
          }
          dre.GetGroupedEntries = function() {
             var d = dre.GetEntries()
             var re = {}
             for(var q of d) {
                if(!re[q.docroot]) re[q.docroot] = []

                re[q.docroot].push(q.host)
             }
             return re
          }

          dre.assignWebappEffective = function() {

              return re.GetWebstoreSettings(webhosting)
                .then((asettings)=>{
                    var webstoreSettings = asettings;
                    var all = dre.GetAll();

                    all.webappEffective = all.webapp;
                    if(all.webappEffective.type == "default")
                       all.webappEffective = webstoreSettings.webapp;            

                    return webstoreSettings;
                 });
          }

          dre.Move = function(in_data) {
              var d;
              var origWh;
              return vali.async(in_data, {
                dest_wh_id: {presence: true, isInteger: {lazy:true}},
              })
              .then(ad=>{
                  d = ad;

                  return app.MonsterInfoWebhosting.GetInfo(d.dest_wh_id);
              })
              .then(newWh=>{
                  origWh = webhosting;
                  webhosting = newWh;
                  adjustPathes();

                  return dre.Save();
              })
              .then(()=>{
                  webhosting = origWh;
                  adjustPathes();

                  return dre.Delete();
              });

          }

          dre.Save = function(nginxOnly, docrootOnly){
              if(docrootOnly) return SaveDocroot()

              var forceReturn;
              var webstoreSettings;

              return dre.assignWebappEffective()
                .then((asettings)=>{
                    webstoreSettings = asettings;

                    if(dre.GetEntries().length <= 0)
                      return dre.Delete();

                    if(webhosting.wh_is_expired) {
                       forceReturn = {code: 504, message: app.config.get("html_response_for_expired")};
                       return re.GetExpiredRootDocroot();
                    }

                    var all = dre.GetAll();
                    if(all['bandwidth-exceeded']) {
                       forceReturn = {code: 503, message: app.config.get("html_response_for_bandwidth_exceeded")};
                       return re.GetBandwidthExceededDocroot();
                    }

                    return Promise.resolve()
                })
                .then((forceDocroot)=>{
                    // this is the generation

                    var groups = dre.GetGroupedEntries()
                    return (nginxOnly ?
                            app.nginx.Save(webhosting, dre, groups, forceDocroot, forceReturn, webstoreSettings) :
                            docrootCall("Save", webhosting, dre, groups, forceDocroot, forceReturn, webstoreSettings))

                })
                .then(()=>{
                   return fs.mkdirAsync(fullDir).catch(ex=>{})
                })
                .then(SaveDocroot)

              function SaveDocroot(){
                   var e = dre.GetAll();
                   delete e.webappEffective;
                   var r = "#"+JSON.stringify(e)+"\n"

                   return fs.writeFileAsync(fullFn, r)
              }

          }

          return Promise.resolve(dre)

          function removeHost(host) {
            var db = 0
            var re = []
            for(var q of d.entries) {
              if(q.host != host)
                re.push(q)
              else
                db++
            }

            d.entries = re

            return db
          }
        })
   }


   return re



  function docrootCallPromise(then) {
     var params = Array.prototype.slice.call(arguments);
     var promiseTransform = params.shift();
     var op = params.shift();

     var re = []
     for(var docrootServer of docrootServers) {
      // console.log("here!", docrootServers, docrootServer)
      if(!docrootServer) continue;
      var p = docrootServer[op].apply(null, params)
      if(promiseTransform)
        p = promiseTransform(p, docrootServer)
      re.push(p)
     }
     return Promise.all(re)
  }

  function docrootCall() {
     var params = [null].concat([].slice.call(arguments))
     return docrootCallPromise.apply(null, params)
  }

  function getCertificateMapDirectory(cert_id) {
     return path.join(app.config.get("tls_certificate_map_directory"), cert_id)
  }


}
