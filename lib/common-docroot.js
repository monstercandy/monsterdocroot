var lib = module.exports = function(app, docroot) {

  const path = require('path')

  const os = require('os')

  const MError = require('MonsterExpress').Error

  var modre = {}

  const lockFile = require('MonsterLockfile')


  modre.capitalize = function(s)
  {
      return s[0].toUpperCase() + s.slice(1);
  }

  modre.getWebhostingInfoExpress = function(callback, sendPromiseResult) {
     return function(req,res,next){

      return getWebhostingInfoPromise(req)
        .then(webhosting=>{
           return andCallNext(callback(webhosting, req,res, next), req, next, sendPromiseResult)

        })

     }
  }

  modre.getWebhostingInfoDomainExpress = function(callback, sendPromiseResult) {

     return modre.getWebhostingInfoExpress((webhosting, req,res,next)=>{

        return lockFile.logic({lockfile: getLockFileName(req.params.domain), callback: function(){

             return docroot.GetDomain(webhosting, req.params.domain)
                .then(domain=>{
                   return andCallNext(callback(webhosting, domain, req,res, next), req, next, sendPromiseResult)
                })


        }})

     })
  }


  return modre

  function getLockFileName(name) {
     var re = name.replace(/[^a-z0-9]/, "")
     if(!re) re = "unknown"
     return "docrootapi-lockfile-" + re
  }

  function andCallNext(p, req, next, sendPromiseResult) {
         if((p)&&(p.then))
           p = p.then((re)=>{
             if(sendPromiseResult)
               return req.sendResponse(re)

             if(typeof req.result === "undefined")
               req.result = "ok"
             next()
           })

         return p
  }

  function getWebhostingInfoPromise(req) {
      return app.MonsterInfoWebhosting.GetInfo(req.params.webhosting_id)
  }



}
