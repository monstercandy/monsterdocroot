#!/bin/bash

OLD_WH=$1
DOMAIN=$2
DEST_WH_ID=$3

if [ -z "$DEST_WH_ID" ]; then
   echo "Usage: $0 old_wh_id domain dest_wh_id"
   exit 1
fi

/opt/client-libs-for-other-languages/internal/perl/generic-light.pl docroot POST "/docrootapi/docroots/$OLD_WH/$DOMAIN/move" \
   "{\"dest_wh_id\":$DEST_WH_ID}"


echo Entries migrated, now you need to move and chown the related files by hand!
echo "mv /storage/d1/w3/$OLD_WH-*/$DOMAIN /storage/d1/w3/$DEST_WH_ID-*"
echo "chown -R $DEST_WH_ID /storage/d1/w3/$DEST_WH_ID-*/$DOMAIN"
