#!/bin/bash

set -e

. /opt/MonsterCommon/lib/common.inc

mkdir -p "/var/lib/monster/$INSTANCE_NAME"
mkdir -p "/var/lib/monster/$INSTANCE_NAME/settings.d"
mkdir -p "/var/lib/monster/$INSTANCE_NAME/hostings.d"
mkdir -p "/var/lib/monster/$INSTANCE_NAME/docroots.d"
mkdir -p "/var/lib/monster/$INSTANCE_NAME/sslmap.d"

mkdir -p "/var/log/monster/$INSTANCE_NAME"
