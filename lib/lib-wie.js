var lib = module.exports = function(app) {

   var wie = {};

   const dotq = require("MonsterDotq");

   wie.BackupWh = function(wh, req) {
      var re = {};
      var domainsLib = getDomainsLib();
      return domainsLib.GetDomainObjectsOfWebhosting(wh)
        .then(adomains=>{

            var ps = [];
            re.domains = {};
            app.webserver_systems.forEach(webserver=>{
              re["hooks-"+webserver] = {};
            });
            re.redirects = {};
            re.frames = {};

            const redirects = getRedirectsLib();
            const frames = getFramesLib();

            adomains.forEach(dre =>{
                var name = dre.GetName();
                re.domains[name] = dre.GetAll();

                app.webserver_systems.forEach(webserver=>{
                   ps.push(
                      app[webserver].GetHooks(wh, dre)
                       .then(hooks=>{
                          if(Object.keys(hooks).length > 0) {
                              re["hooks-"+webserver][name] = hooks;
                          }
                       })
                   );

                   ps.push(
                      redirects.GetDomainRedirect(name)
                        .then((rd=>{
                            var entries = rd.GetEntries();
                            if(entries.length > 0)
                              re.redirects[name] = entries;
                        }))
                   );

                   ps.push(
                      frames.GetDomainRedirect(name)
                        .then((rd=>{
                            var entries = rd.GetEntries();
                            if(entries.length > 0)
                              re.frames[name] = entries;
                        }))
                   );

                   ps.push(
                      domainsLib.GetWebstoreSettings(wh)
                        .then((rd=>{
                            re.webstore = rd;
                        }))
                   );

                })
            });


            return Promise.all(ps);
        })
        .catch(ex=>{
           if(ex.code != "ENOENT") throw ex;
           console.error("Error while creating backup for ", wh.wh_id," (ignoring)");
        })
        .then(()=>{
            return getAuthLib().OpenByWh(wh).catch(ex=>{});
        })
        .then((auth)=>{
            re.auth = auth.GetAccounts(true);

            return re;
        })
   }

   wie.GetAllWebhostingIds = function(){
      return getDomainsLib().GetWebhostings()
   }

   wie.RestoreWh = function(wh, in_data, req) {

          const domains = getDomainsLib();

          return dotq.linearMap({array: Object.keys(in_data.domains || {}), action: function(domainName){

            console.log("restoring", domainName, wh);

             var data = in_data.domains[domainName];
             var domain;
             var awstats;
             var cert;
             return domains.ModifyWebstoreSettings(wh, function(d){
                extend(d, in_data.webstore);
             })
               .then(()=>{
                  return domains.GetDomain(wh, domainName);
               })
               .then(adomain=>{
                   domain = adomain;

                   // the following two have some action handlers, so they must be set through the setters
                   cert = data.certificate_id;
                   delete data.certificate_id;

                   awstats = data.awstats;
                   delete data.awstats;

                   domain.SetAll(data);

                   if(cert)
                      return domain.SetCertificate({certificate_id: cert});
               })
               .then(()=>{
                  if(awstats)
                     return domain.CreateAwstatsAndSaveDomain(false, true);

                  return domain.Save();
               })
               .then(()=>{
                  var ps = [];

                  app.webserver_systems.forEach(webserver=>{
                      var hookData = in_data['hooks-'+webserver][domainName];
                      if(hookData)
                        return app[webserver].SetHooks(wh, domain, hookData)

                  });

                  return Promise.all(ps);

               })

          }})

          .then(()=>{
             if(in_data.auth.length <= 0)
               return;

             return getAuthLib().OpenByWh(wh)
               .then(auth=>{
                  return dotq.linearMap({array: in_data.auth || [], action: function(d){
                     return auth.AddAccount(d, true);
                  }})
                  .then(()=>{
                     return auth.Save();
                  })
               })
          })
          .then(()=>{
            var libs = {
               redirects: getRedirectsLib(),
               frames: getFramesLib()
            }

            return dotq.linearMap({array: Object.keys(libs), action: function(libName){

                return dotq.linearMap({array: Object.keys(in_data[libName] || {}), action: function(domainName){
                   return libs[libName].GetDomainRedirect(domainName)
                     .then(dre=>{

                        return dotq.linearMap({array: in_data[libName][domainName], action: function(h){
                            return dre.AddRedirect(h.host, h);
                        }})
                        .then(()=>{
                           return dre.Save();
                        })
                     })

                }})


            }})

          })
          .then(()=>{
              app.InsertEvent(req, {e_event_type: "restore-wh-docroot", e_other: wh.wh_id})

          })

   }



   return wie;

   function getDomainsLib(){
      return require("lib-docroot.js")(app);
   }

   function getAuthLib(){
      return require("lib-auth.js")(app);
   }

   function getRedirectsLib(){
      return require("lib-redirect.js")(app);
   }

   function getFramesLib(){
      return require("lib-frames.js")(app);
   }
}
