const libBase = require("lib-redirect-base.js")
var lib = module.exports = function(app) {
   return libBase(app, "nginx_frame_directory")
}
