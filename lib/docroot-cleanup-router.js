var lib = module.exports = function(app){

    var router = app.ExpressPromiseRouter()

    var cleanup = require("lib-cleanup.js")(app)


     router.delete("/domains/:domain_name", finisher((req)=>{
     	 return cleanup.RemoveDomain(req.params.domain_name)
          .then(()=>{
             app.InsertEvent(req, {e_event_type: "domain-removed"})
          })
	 }))

     router.post("/redirects/rebuild", finisher((req)=>{
       return cleanup.RebuildAllRedirects()
          .then(()=>{
             app.InsertEvent(req, {e_event_type: "redirects-rebuilt"})
          })
    }))

     router.post("/frames/rebuild", finisher((req)=>{
       return cleanup.RebuildAllFrames()
          .then(()=>{
             app.InsertEvent(req, {e_event_type: "frames-rebuilt"})
          })
    }))

     router.post("/webhostings/rebuild", finisher((req)=>{
       return cleanup.RebuildAllWebhostings()
          .then(()=>{
             app.InsertEvent(req, {e_event_type: "webhostings-rebuilt"})
          })
    }))

     router.post("/webhosting/:webhosting_id/rebuild", finisher((req)=>{
       return cleanup.RebuildWebhosting(req.params.webhosting_id)
          .then(()=>{
             app.InsertEvent(req, {e_event_type: "webhostings-rebuilt"})
          })
    }))


     router.delete("/webhostings/:webhosting_id", finisher((req)=>{
     	 return cleanup.RemoveWebhosting(req.params.webhosting_id)
          .then(()=>{
             app.InsertEvent(req, {e_event_type: "webhosting-remove"})
          })
	  }))

     router.post("/maintane", finisher((req)=>{
     	 return cleanup.Maintane()
           .then((stats)=>{
              app.InsertEvent(req, {e_event_type: "maintane"})
              req.result = stats
           })
     }))

     return router


    function finisher(callback) {

    	return function(req,res,next){
        if(req.result) next()

    		return callback(req,res,next)
				.then(()=>{
                  if(!req.result)
	     	   	    req.result = "ok"
	     	   	  next()
	     	   })
    	}

    }
}
