module.exports = function(app) {


    var router = app.ExpressPromiseRouter()

    const docrootServer = require("lib-docroot.js")(app)

  const MError = app.MError

  router.route("/:cert_id/:domain")
    .delete(function(req,res,next){
       throw new MError("NOT_IMPLEMENTED")
    })


  router.route("/:cert_id")
    .delete(function(req,res,next){
       return req.sendOk(
          docrootServer.detachCertificate(req.params.cert_id)
          .then(()=>{
             app.InsertEvent(req, {e_event_type: "certificate-detach"})
          })
       )
    })
    .get(function(req,res,next){
       return docrootServer.getDomainsForCertificate(req.params.cert_id)
         .then(doms=>{
            req.sendResponse({docroots:doms})
         })
    })



  return router

}
