var lib = module.exports = function(app, directory_key) {

   var ValidatorsLib = require("MonsterValidators")
   var vali = ValidatorsLib.ValidateJs()
   var firstLine = require("FirstLineAsJSON")
	const MError = require('MonsterExpress').Error

    const fs = require("MonsterDotq").fs()	
	const path = require("path")
	const redirectDir = app.config.get(directory_key)

   var common = require("common.js")(app)

    const validators = {
             "domain": {presence: true, isHost: true},
             "hostPrefix": {isHostWildcard: true}, // empty is ok (when the domain matches the host)
             "url": {presence: true, isSimpleWebUrl: true},
          }


   var re = {}

   re.GetDomainRedirects = function() {
      return fs.readdirAsync(redirectDir)
        .then(files=>{
            var re = []
            for(var q of files) {

              var myRegexp = new RegExp('(.+)\.conf$');
                    var match = myRegexp.exec(q);
                    if(!match) continue
                    if(match[1])
                      re.push(match[1])
            }

            return Promise.resolve(re)

        })   	  
   }

   re.GetDomainRedirect = function(domain) {
   	  var fullFn
   	  var canonDomain
   	  return vali.async({domain: domain}, {domain: validators.domain})
   	    .then(d=>{
   	    	canonDomain= d.domain.d_host_canonical_name
   	    	fullFn = path.join(redirectDir, canonDomain+".conf")

   	    	return firstLine.ReadAsync(fullFn, function(line, originalParser){
	           var recs = originalParser(line)

	           if(!Array.isArray(recs))
	              throw new MError("INVALID_JSON_IN_DOMAIN_FILE")

	           return recs
	        }).catch(ex=>{
	        	return Promise.resolve([])
	        })

   	    })
   	    .then(d=>{

   	    	var dre = {}
   	    	dre.GetRaw = function(){
   	    		return fs.readFileAsync(fullFn, "utf8")
   	    	}
   	    	dre.GetEntries = function(){
   	    		return d
   	    	}
   	    	dre.Delete = function() {
   	    		return fs.unlinkAsync(fullFn)
   	    		  .then(()=>{
   	    		  	  re.rehashLater()
   	    		  	  return Promise.resolve()
   	    		  })
   	    	}
          dre.DeleteIfExists = function() {
            if(dre.GetEntries().length <= 0) return Promise.resolve()
            return dre.Delete()
          }

   	    	dre.RemoveRedirect = function(host) {

   	    		return Promise.resolve()
   	    		  .then(()=>{
		   	    		 var hostPrefix = common.hostToPrefixBasedOnDomain(host, canonDomain)
      	    		 return vali.async({"host": hostPrefix}, {"host":validators.hostPrefix})

   	    		  })
	   	    	  .then(d=>{
	   	    		  	if(!removeHost(host))
	   	    		  		throw new MError("REDIRECT_NOT_FOUND")

	   	    		  	return Promise.resolve()
	   	    		})
   	    	}

   	    	dre.AddRedirect = function(host, data) {

                 var d

                 return Promise.resolve()
                 .then(()=>{
	   	    		data.hostPrefix = common.hostToPrefixBasedOnDomain(host, canonDomain)
	   	    		return vali.async(data, {"hostPrefix":validators.hostPrefix, "redirect": validators.url})

                 })
   	    		  .then(ad=>{
   	    		  	    d = ad

		                const containsLib = require("lib-contains-already.js")
		                return containsLib(redirectDir, canonDomain, host)
   	    		  })
   	    		  .then(()=>{
	   	    		  	removeHost(host)
	   	    		  	dre.GetEntries().push({"host": host, "redirect": d.redirect})

	   	    		  	return Promise.resolve()   	    		  	
   	    		  })
   	    	}

   	    	dre.Save = function(){
   	    		if(dre.GetEntries().length <= 0) return dre.Delete()

   	    		// this is the generation
   	    	    var e = dre.GetEntries()
   	    	    var r = "#"+JSON.stringify(e)+"\n"
   	    	    for(var q of e) {
                    r += q.host+" "

                    var surl = ValidatorsLib.isSimpleUrl(q.redirect)
                    if(surl !== false)
                    	r += surl+"$uri"
                    else
                    	r += q.redirect

                    r += ";\n"
   	    	    }

              re.rehashLater()
   	    	    return fs.writeFileAsync(fullFn, r)
   	    	}

   	    	return Promise.resolve(dre)

   	    	function removeHost(host) {
   	    		var db = 0
   	    		var re = []
   	    		for(var q of d) {
   	    			if(q.host != host)
   	    				re.push(q)
   	    			else
   	    				db++
   	    		}

   	    		d = re

   	    		return db
   	    	}
   	    })
   }


   re.rehashLater = function(){
   	  return app.nginx.RehashLater()
   }
   re.rehashNow = function(){
   	  return app.nginx.Rehash()
   }

   return re


}
