
/*
Thsi is how a syslogd callback looks like:
{ facility: 6,
  severity: 23,
  tag: 'nginx',
  time: 2018-10-23T05:26:07.000Z,
  hostname: 'test',
  address: {},
  family: undefined,
  port: undefined,
  size: 114,
  msg: '11005 POST php-72.test.monstermedia.hu 192.168.0.104 "curl/7.55.1" / foobar=1 200' }
  

Banning examples:
11015 POST catalase.hu 136.243.79.106 "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36" //xmlrpc.php <?xml version=\x221.0\x22?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data>\x0D\x0A<value><struct><member><name>methodName</name><value><string>wp.getUsersBlogs</string></value></member><member><name>params</name><value><array><data><value><array><data><value><string>electron</string></value><value><string>qaz!@#</string></value></data></array></value></data></array></value></member></struct></value>\x0D\x0A</data></array></value></param></params></methodCall>\x0D\x0A 200
*/
const moment = require("MonsterMoment");
var olib = module.exports = function(app){
  var started = false;
  var statistics;
  var banParameters = {};
  Array(
  	"syslog_waf_ban_individual_attempts",
    "syslog_waf_ban_individual_window_sec",
  	"syslog_waf_ban_storage_all_attempts",
    "syslog_waf_ban_storage_individual_attempts",
    "syslog_waf_ban_storage_window_sec",
  ).forEach(n=>{
  	 banParameters[n] = app.config.get(n);
  });

  var whitelistedDomains = app.config.get("syslog_waf_whitelisted_domains");

  var router;
  var re = {
  	 Start: function(){
  	 	if(started) {
  	 		return router;
  	 	}
  	 	statistics = olib.InitStatistics();
		const unix = require("/opt/MonsterDocroot/lib/unix-dgram-"+app.config.get("debian_version"));
		var socket_path = app.config.get("syslog_waf_socket_path");
		var Syslogd = require('lib-syslogd.js');
		Syslogd(function(info) {
		    /*
		    info = {
		          facility: 7
		        , severity: 22
		        , tag: 'tag'
		        , time: Mon Dec 15 2014 10:58:44 GMT-0800 (PST)
		        , hostname: 'hostname'
		        , address: '127.0.0.1'
		        , family: 'IPv4'
		        , port: null
		        , size: 39
		        , msg: 'info'
		    }
		    */
		   //console.log("line received", info);
       statistics.messagesProcessed++;

		   var r = olib.ParsePostDataLine(info.msg);
		   if(!r) {
		   	  console.error("Unable to parse incoming syslog postdata message:", info);
		   	  return;
		   }

		   if(olib.IsForbiddenEvent(r)) {
		   	  console.error("Forbidden event!", info);
			  app.InsertEvent(null, {
                   e_ip: r.remoteAddr,
                   e_event_type: "forbidden-http-request",
                   e_other: {queryString: r.queryString, userAgent: r.userAgent},
                })

		   	  return banIp(r);
		   }

		   if(!olib.IsProtectedEvent(r)) return;

       // whitelist feature
       if(whitelistedDomains.indexOf(r.hostname) > -1) {
         // request on a whitelisted domain
         return;
       }

		   // this is a protected event, ban logic needs to be applied
		   if(olib.AccountProtectedRequest(r, statistics, banParameters)) {
		   	  console.log("Too many protected requests from", info);
			  app.InsertEvent(null, {
                   e_ip: r.remoteAddr,
                   e_event_type: "protected-http-request-ban",
                   e_other: {queryString: r.queryString, userAgent: r.userAgent},
                })

		   	  return banIp(r);

		   }


		}, {
			server: unix.createSocket('unix_dgram'), unlinkSocket: true
		})
		.listen(socket_path, function(err) {
			if(err) {
				console.error("Unable to start syslog waf server", err);
				return;
			}
			started = true;
		    console.log('Syslog waf server has started');
		    setInterval(cleanUp, app.config.get("syslog_waf_cleanup_sec")*1000);
		});

      router = app.ExpressPromiseRouter();
      router.get("/current", function(req,res,next){
         return req.sendResponse(statistics);
      });
      return router;

  	 },
  	 StartWhenNeeded: function(){
  	 	var syslogEnabled = app.config.get("syslog_waf_enabled");
  	 	if(!syslogEnabled) return;
  	 	return re.Start();
  	 }
  }

  return re;

  function cleanUp(){
  	olib.CleanStatistics(statistics, banParameters);
  }


  function banIp(r){
  	  var ip = r.remoteAddr;
      return app.GetRelayerMapiPool().putAsync("/s/[this]/iptables/bans/HTTP", {ip:ip})
        .catch(ex=>{
            console.error("Unable to ban IP", ip, ex);
        })
  }

}
olib.CleanStatistics = function(statistics, banParameters, now) {
     if(!statistics) return;

     if(!now)
       now = moment.nowUnixtime();

     cleanCategory(now-banParameters.syslog_waf_ban_individual_window_sec, statistics.perIp);

     cleanCategory(now-banParameters.syslog_waf_ban_storage_window_sec, statistics.perWebstore);

	  function cleanCategory(before, hash){
	  	 Object.keys(hash).forEach(c=>{
	  	 	var d = hash[c];

	  	 	d.forEach(ts=>{
	  	 		if(ts <= before) {
	  	 			while(true){
		  	 			var i = d.indexOf(ts);
		  	 			if(i < 0) break;
		  	 			d.splice(i, 1);	  	 				
	  	 			}
	  	 		}
	  	 	})

	  	 	if(d.length <= 0) {
	  	 		delete hash[c];
	  	 	}
	  	 })
	  }

}
olib.ParsePostDataLine = function(line){
  // this is a line:
  // 11005 POST php-72.test.monstermedia.hu 192.168.0.104 "curl/7.55.1" / foobar=1 200

  const r = /^([0-9]+) ([^ ]+) ([^ ]+) ([^ ]+) "([^"]+)" ([^ ]+) (.+) ([0-9]{3})$/;//
  var m = r.exec(line);
  if(!m) return;
  return {
  	webstoreId: m[1],
  	httpMethod: m[2],
  	hostname: m[3],
  	remoteAddr: m[4],
  	userAgent: m[5],
  	queryString: m[6],
  	postData: m[7],
  	httpResponseCode: parseInt(m[8],10),
  }
}
olib.InitStatistics = function(){
	return {
        perIp: {},
        perWebstore: {},
        messagesProcessed: 0,
	};
}
olib.IsForbiddenEvent = function(r){
  if(r.userAgent.match(/JSimplepieFactory.*eval/)) {
  	 return true;
  }
  return false;
}
olib.IsProtectedEvent = function(r){
  // detecting login events, xmlrpc calls and similar

  if(r.httpMethod == "GET") return false;
  if(r.httpMethod == "HEAD") return false;

  if(r.queryString.match(/login|xmlrpc/)) {
  }
  else if((r.queryString.match(/\/administrator\/index.php/))&&(r.postData.match(/passwd=/))) {
  }
  else if((r.postData.match(/user/))&&(r.postData.match(/passw/))) {
  }
  else
  	return false;

  // mc accountapi's loginlog might receive more requests
  if(r.queryString.match(/loginlog/)) {
  	return false;
  }

  // this is a protected event!
  return true;
}
olib.AccountProtectedRequest = function(r, statistics, banParameters, now){
   if(!now) {
     now = moment.nowUnixtime();
   }

   var ip = r.remoteAddr;
   if(!statistics.perIp[ip]) {
   	  statistics.perIp[ip] = [];
   }
   statistics.perIp[ip].push(now);

   if(r.webstoreId){
	   if(!statistics.perWebstore[r.webstoreId]) {
	   	 statistics.perWebstore[r.webstoreId] = [];
	   }
	   statistics.perWebstore[r.webstoreId].push(now);   	
   }

   var individualCounter = statistics.perIp[ip].length;
   if(individualCounter >= banParameters.syslog_waf_ban_individual_attempts) {
   	 return true;
   }

   if(r.webstoreId) {
	   var webstoreCounter = statistics.perWebstore[r.webstoreId].length;
	   if(
	   	  (webstoreCounter >= banParameters.syslog_waf_ban_storage_all_attempts) 
	   	  &&
	   	  (individualCounter >= banParameters.syslog_waf_ban_storage_individual_attempts)
	   	 )
	   {
	     return true;
	   }

   }

   return false;
}
