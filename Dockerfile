FROM monstercommon

ADD lib /opt/MonsterDocroot/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterDocroot/lib/bin/docroot-install.sh && /opt/MonsterDocroot/lib/bin/docroot-test.sh

ENTRYPOINT ["/opt/MonsterDocroot/lib/bin/docroot-start.sh"]
